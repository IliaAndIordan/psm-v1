import { Component, EventEmitter, OnDestroy, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
import { CompanyModel } from "src/app/@core/clients/api/company/dto";
import { CurrentUserService } from "src/app/@core/clients/current-user.service";
import { AppRoutes } from "src/app/@core/constants/app-routes.const";
import { SxSpinnerService } from "src/app/@share/sx-common/sx-spinner/sx-spinner.service";
import { URL_COMMON_IMAGE_SX, URL_NO_IMG } from "src/app/@core/constants/app-storage.const";
import { PsmUserRole } from "src/app/@core/clients/api/auth/enums";
import { PsmProduct } from "src/app/@core/clients/api/psm-product/dto";
import { PsmCompanyService } from "src/app/company/company.service";
import { PsmProductService } from "../psm-product.service";


@Component({
    selector: 'psm-product-filter-panel',
    templateUrl: './psm-product-filter-panel.component.html',
})
export class PsmProductFilterPanelComponent implements OnInit, OnDestroy {

    @Output() productEditClick: EventEmitter<PsmProduct> = new EventEmitter<PsmProduct>();

    routs = AppRoutes;
    company: CompanyModel;
    companyChanged: Subscription;
    product: PsmProduct;
    productChanged: Subscription;

    tabIdx: number;
    tabIdxChanged: Subscription;
    canEdit: boolean;

    logoUrl: string;

    noLogoUrl = URL_NO_IMG;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinnerService: SxSpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private prodService: PsmProductService) {

    }


    ngOnInit(): void {

        this.companyChanged = this.cus.companyChanged.subscribe((company: CompanyModel) => {
            this.initFields();
        });

        this.tabIdxChanged = this.prodService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.productChanged = this.prodService.productChanged.subscribe((prod: PsmProduct) => {
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.companyChanged) { this.companyChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.company = this.cus.company;
        this.product = this.prodService.product;
        this.logoUrl = this.product && this.product.logoUrl ? this.product.logoUrl : this.noLogoUrl;
        this.canEdit = ((this.cus.user.companyId === this.company.companyId) && (this.cus.user.role < PsmUserRole.User));
        this.tabIdx = this.prodService.tabIdx;
    }

    //#region Action Methods

    refreshData() {
        //console.log('refreshData -> company:', this.company);
        this.spinnerService.show();
        this.prodService.loadProduct(this.product.psmProductId)
            .then(prod => {
                console.log('refreshData -> com:', prod);
                this.spinnerService.hide();
                this.prodService.product = prod;

            });

    }

    ediProduct() {
        if (this.product && this.canEdit) {
            this.productEditClick.emit(this.product);
        }
    }

    //#endregion

}
