import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { UserModel } from "../@core/clients/api/auth/dto";
import { SxApiCompanyClient } from "../@core/clients/api/company/api-client";
import { CompanyModel, CompanyProduct } from "../@core/clients/api/company/dto";
import { ApiPsmProductClient } from "../@core/clients/api/psm-product/api-client";
import { PsmProduct } from "../@core/clients/api/psm-product/dto";
import { CurrentUserService } from "../@core/clients/current-user.service";
import { AppStore } from "../@core/constants/app-storage.const";

export const FP_IN_KEY = 'psm_product_filter_panel_in';
export const TABIDX_KEY = 'psm_product_tab_idx';

@Injectable({
    providedIn: 'root',
})
export class PsmProductService {

    spmPsmProductPanelOpen = new BehaviorSubject<PsmProduct>(undefined);
    spmPsmProduct: PsmProduct;

    constructor(
        private cus: CurrentUserService,
        private comClient: SxApiCompanyClient,
        private prodClient:ApiPsmProductClient,) {
    }

    //#region CompanyModel

    get company(): CompanyModel {
        return this.cus.company;
    }

    set company(value: CompanyModel) {
        this.cus.company = value
    }


    get user(): UserModel {
        return this.cus.user;
    }

    get sxProducts(): CompanyProduct[] {
        return this.cus.sxProducts;
    }

    productChanged = new BehaviorSubject<PsmProduct>(undefined);
    
    get product(): PsmProduct {
        const objStr = sessionStorage.getItem(AppStore.CurrentUserPsmProduct);
        let obj: PsmProduct;
        if (objStr) {
            obj = Object.assign(new PsmProduct(), JSON.parse(objStr));
        }
        // console.log('company-> obj', obj);
        return obj;
    }

    set product(value: PsmProduct) {
        if (value) {
            sessionStorage.setItem(AppStore.CurrentUserPsmProduct, JSON.stringify(value));
        } else {
            sessionStorage.removeItem(AppStore.CurrentUserPsmProduct);
        }
        this.productChanged.next(value);
    }


    //#endregion

    //#region FP and Tab Idx

    panelInChanged = new BehaviorSubject<boolean>(true);

    get panelIn(): boolean {
        let rv = true;
        const valStr = localStorage.getItem(FP_IN_KEY);
        if (valStr) {
            rv = JSON.parse(valStr) as boolean;
        }
        return rv;
    }

    set panelIn(value: boolean) {
        localStorage.setItem(FP_IN_KEY, JSON.stringify(value));
        this.panelInChanged.next(value);
    }

    tabIdxChanged = new BehaviorSubject<number>(undefined);

    get tabIdx(): number {
        let rv = 0;
        const dataStr = localStorage.getItem(TABIDX_KEY);
        //console.log('selFolderId-> dataStr', dataStr);
        if (dataStr) {
            try {
                rv = parseInt(dataStr, 10);
            }
            catch {
                localStorage.removeItem(TABIDX_KEY);
                rv = 1;
            }

        }
        // console.log('selTabIdx-> rv', rv);
        return rv;
    }

    set tabIdx(value: number) {
        // console.log('selTabIdx->', value);
        const oldValue = this.tabIdx;

        localStorage.setItem(TABIDX_KEY, JSON.stringify(value));
        if (oldValue !== value) {
            this.tabIdxChanged.next(value);
        }
    }

    //#endregion

    //#region Data

    loadProduct(id: number): Promise<PsmProduct> {

        let promise = new Promise<PsmProduct>((resolve, reject) => {
            this.prodClient.psmProductById(id)
                .subscribe((resp: PsmProduct) => {
                    //console.log('loadCompany-> resp', resp);
                    resolve(resp);
                }, msg => {
                    console.log('loadProduct -> msg', msg);
                    reject(msg);
                }
                );

        });

        return promise;
    }


    //#endregion
}
