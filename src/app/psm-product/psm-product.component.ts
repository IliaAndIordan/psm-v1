import { Component, OnInit } from '@angular/core';

@Component({
  template: '<div class="psm-page"> <router-outlet></router-outlet> </div>'
})
export class PsmProductComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
