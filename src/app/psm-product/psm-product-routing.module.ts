import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppRoutes } from '../@core/constants/app-routes.const';
import { PsmProductComponent } from './psm-product.component';
import { PsmProductHomeComponent } from './home.component';
import { AuthGuard } from '../@core/guards/auth.guard.service';
import { PsmProductTabsComponent } from './tabs/psm-product-tabs.component';

// Component



const routes: Routes = [
  {
    path: AppRoutes.Root, component: PsmProductComponent,
    
    children: [
      { path: AppRoutes.Root, component: PsmProductHomeComponent,
        
        children: [
             
          { path: AppRoutes.Root,   pathMatch: 'full', component: PsmProductTabsComponent, canActivate: [AuthGuard] },
         /*
          { path: AppRoutes.info, component: PsmCompanyDashboardComponent },
          { path: AppRoutes.users, component: PsmCompanyUserListComponent },
          { path: AppRoutes.profile, component: PmsCompanyUserComponent },
          { path: AppRoutes.products, component: PsmCompanyPmsProductsComponent },
          { path: AppRoutes.projects, component: PsmCompanyPmsProjectsComponent },
          
          { path: AppRoutes.create, component: AirlineCreateFormComponent },
          { path: AppRoutes.info, component: AirlineInfoComponent,  
            children: [
            { path: AppRoutes.Root,   pathMatch: 'full', component: AirlineInfoDashboardComponent },
            { path: AppRoutes.dashboard, component: AirlineInfoDashboardComponent },
            { path: AppRoutes.aircafts, component: AirlineInfoAircraftsComponent },
          ]},
          // { path: AppRoutes.Dashboard, component: DashboardComponent },
          */
        ]
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PsmProductRoutingModule { }

export const routedComponents = [
  PsmProductComponent, 
  PsmProductHomeComponent,
  PsmProductTabsComponent,
];
