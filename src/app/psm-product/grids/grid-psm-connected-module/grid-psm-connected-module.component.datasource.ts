import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { PsmModule, PsmModuleTableCriteria, PsmProduct, PsmProductTableCriteria, ResponsePsmModuleTable, ResponsePsmProductTable } from 'src/app/@core/clients/api/psm-product/dto';
import { ApiPsmProductClient } from 'src/app/@core/clients/api/psm-product/api-client';
import { PsmProductService } from '../../psm-product.service';

export const KEY_CRITERIA = 'psm_product_grid_psm_connected_module_criteria';
@Injectable()
export class GridPsmConnectedModuleTableDataSource extends DataSource<PsmModule> {

    seleted: PsmModule;

    private _data: Array<PsmModule>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<PsmModule[]> = new BehaviorSubject<PsmModule[]>([]);
    listSubject = new BehaviorSubject<PsmModule[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<PsmModule> {
        return this._data;
    }

    set data(value: Array<PsmModule>) {
        this._data = value;
        this.listSubject.next(this._data as PsmModule[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: ApiPsmProductClient,
        private service: PsmProductService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<PsmModuleTableCriteria>();

    public get criteria(): PsmModuleTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: PsmModuleTableCriteria;
        if (onjStr) {
            obj = Object.assign(new PsmModuleTableCriteria(), JSON.parse(onjStr));
            /*
            if(!obj.companyId || obj.companyId !== this.cus.company.companyId){
                obj.companyId = this.cus.company.companyId;
                localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
            }*/
            obj.companyId = undefined;
            if(!obj.psmProductId || obj.psmProductId !== this.service.product.psmProductId){
                obj.psmProductId = this.service.product.psmProductId;
                localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
            }
            
        } else {
            obj = new PsmModuleTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'udate';
            obj.sortDesc = true;
            obj.psmProductId = this.service.product.psmProductId;
            obj.companyId = undefined;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: PsmModuleTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<PsmModule[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<PsmModule>> {
        //console.log('loadData->');
        //console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;
        
        return new Observable<Array<PsmModule>>(subscriber => {

            this.client.psmConnectedModuleTable(this.criteria)
                .subscribe((resp: ResponsePsmModuleTable) => {
                    //const resp = Object.assign(new ResponseAmsUserTableData(), res);
                    //console.log('loadData-> resp=', resp);
                    this.data = new Array<PsmModule>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.psmModules && resp.data.psmModules.length > 0) {
                            resp.data.psmModules.forEach(iu => {
                                const obj = PsmModule.fromJSON(iu);
                                this.data.push(obj);
                            });
                        }

                        this.itemsCount = resp.data.rowsCount?resp.data.rowsCount.totalRows:0;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    //console.log('loadData-> data=', this.data);
                    //console.log('loadData-> itemsCount=', this.itemsCount);
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<PsmModule>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
