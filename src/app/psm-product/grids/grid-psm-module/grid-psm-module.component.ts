import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
// --- Services
import { SxSpinnerService } from 'src/app/@share/sx-common/sx-spinner/sx-spinner.service';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { PsmModule, PsmModuleTableCriteria, PsmProduct, PsmProductTableCriteria } from 'src/app/@core/clients/api/psm-product/dto';
import { COMMON_IMG_AVATAR, PAGE_SIZE_OPTIONS, URL_COMMON_IMAGE_AIRCRAFT } from 'src/app/@core/constants/app-storage.const';
import { CompanyModel } from 'src/app/@core/clients/api/company/dto';
import { ApiPsmProductClient } from 'src/app/@core/clients/api/psm-product/api-client';
import { PsmUserRole } from 'src/app/@core/clients/api/auth/enums';
import { AppRoutes } from 'src/app/@core/constants/app-routes.const';
import { PsmProductService } from '../../psm-product.service';
import { GridPsmModuleTableDataSource } from './grid-psm-module.datasource';
import { ThisReceiver } from '@angular/compiler';

// --- Models


@Component({
    selector: 'psm-product-grid-psm-module',
    templateUrl: './grid-psm-module.component.html',
})
export class GridPsmModuleComponent implements OnInit, OnDestroy, AfterViewInit {
    
    @Output() dataLoded: EventEmitter<number> = new EventEmitter<number>();
    @Output() addPsmModuleClicked: EventEmitter<PsmProduct> = new EventEmitter<PsmProduct>();
    @Output() editPsmModuleClicked: EventEmitter<PsmModule> = new EventEmitter<PsmModule>();
    
    // @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<PsmProduct>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    /**
     * Fields
     */
    seatsFImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_f.png';
    seatsBImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_b.png';
    seatsEImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_e.png';
    //typeOpt = RwTypeOpt;
    //selProduct: PsmProduct;
    product: PsmProduct;
    productChanged: Subscription;
    company: CompanyModel;
    companyChanged: Subscription;
    module: PsmModule;
    moduleChanged: Subscription;

    criteria: PsmModuleTableCriteria;
    criteriaChanged: Subscription;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;

    dataCount = 0;
    dataChanged: Subscription;
    //
    displayedColumns = ['action',  'name',  'moduleTypeId','notes', 'moduleId'];
    canEdit: boolean;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SxSpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private client: ApiPsmProductClient,
        private service: PsmProductService,
        public tableds: GridPsmModuleTableDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {
        this.canEdit = this.cus.user.role < PsmUserRole.User;
        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<PsmModule>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: PsmModuleTableCriteria) => {
            this.criteria = this.tableds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadPage();
        });


        this.initFields();
        this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        //this.company = this.cus.company;
        this.company = this.cus.company;
        this.product = this.service.product;
        this.canEdit = this.cus.user.role < PsmUserRole.User;
        this.criteria = this.tableds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
        this.dataLoded.emit(this.dataCount);
        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }

    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    gotoProduct(value:PsmProduct){
        console.log('gotoProduct -> value:' + value);
        if(value && this.canEdit){
            this.service.product = value;
            this.router.navigate([AppRoutes.Root, AppRoutes.product]);
        }
    }

    gotoModule(value:PsmModule){
        console.log('gotoModule -> value:' + value);
        if(value && this.canEdit){
            //this.service.product = value;
            //this.router.navigate([AppRoutes.Root, AppRoutes.product]);
        }
    }

    refreshClick() {
        this.loadPage();
    }

   
    spmPsmModuleOpen(value: PsmModule) {
        if (value) {
            //this.aliService.spmAircraftPanelOpen.next(value);
        }
    }

    addPsmModule(){
        if(this.canEdit && this.service.product){
            this.addPsmModuleClicked.emit(this.service.product);
        }
        
    }
    
    editPsmModule(data: PsmModule) {
        if (this.canEdit && data) {
            this.editPsmModuleClicked.emit(data);
        }
        else {
            this.toastr.info('Please select module first.', 'Edit Module');
        }
    }

   

    //#region Data
    /*
        public loadAirport(apId: number): Observable<AmsAirport> {
            
            this.preloader.show();
            return new Observable<AmsAirport>(subscriber => {
    
                this.aService.loadAirport(apId)
                    .subscribe((resp: AmsAirport) => {
                        //console.log('loadAirport-> resp', resp);
                        subscriber.next(resp);
                        this.preloader.hide();
                    },
                        err => {
                            this.preloader.hide();
                            throw err;
                        });
            });
            
        }
    */
    //#endregion


    /*
    gotoAirport(data: AmsAirport) {
        console.log('gotoAirport-> airport:', data);

        if (data) {
            this.wadService.airport = data;
            this.router.navigate([AppRoutes.wad, AppRoutes.airport]);
        }

    }*/

    //#endregion
}

