import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
import { PsmModule, PsmProduct } from "src/app/@core/clients/api/psm-product/dto";
import { CurrentUserService } from "src/app/@core/clients/current-user.service";
import { Animate } from "src/app/@core/constants/animation.const";
import { PageTransition } from "src/app/@core/constants/animations-triggers";
import { PsmModuleEditDialog } from "src/app/@share/sx-common/modals/psm-module/edit/psm-module-edit.dialog";
import { SxSpinnerService } from "src/app/@share/sx-common/sx-spinner/sx-spinner.service";
import { GridPsmConnectedModuleComponent } from "../grids/grid-psm-connected-module/grid-psm-connected-module.component";
import { GridPsmModuleComponent } from "../grids/grid-psm-module/grid-psm-module.component";
import { GridPsmModuleTableDataSource } from "../grids/grid-psm-module/grid-psm-module.datasource";
import { PsmProductService } from "../psm-product.service";

@Component({
  templateUrl: './psm-product-tabs.component.html',
  animations: [PageTransition]
})
export class PsmProductTabsComponent implements OnInit, OnDestroy {

  @ViewChild('moduleGrid', { static: false }) moduleGrid: GridPsmModuleComponent;
  @ViewChild('conmodGrid', { static: false }) conmodGrid: GridPsmConnectedModuleComponent; 
  
  state: string = Animate.in;
  NgbModalOptionsNoClose = { closeOnClickOutside: false, closeOnEscape: false };

  tabIdx: number;
  tabIdxChanged: Subscription;

  productChanged: Subscription;
  product: PsmProduct;

  moduleCount:number;
  connectedModuleCount:number;

  constructor(
    private toastr: ToastrService,
    private spinnerService: SxSpinnerService,
    private cus: CurrentUserService,
    private prodService: PsmProductService,
    public dialogService: MatDialog) { }


  ngOnInit(): void {
    this.state = (this.state === Animate.in ? Animate.out : Animate.in);

    this.tabIdxChanged = this.prodService.tabIdxChanged.subscribe(tabIdx => {
      this.initFields();
    });
    this.productChanged = this.prodService.productChanged.subscribe(prod => {
      this.initFields();
    });

    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }
    if (this.productChanged) { this.productChanged.unsubscribe(); }
  }

  initFields() {
    this.tabIdx = this.prodService.tabIdx;
    this.product = this.prodService.product;
    this.moduleCount = this.moduleGrid?this.moduleGrid.dataCount:0;
    this.connectedModuleCount = this.conmodGrid?this.conmodGrid.dataCount:0;
  }

  //#region  Tab Panel Actions

  selectedTabChanged(tabIdx: number) {
    const oldTab = this.tabIdx;
    this.tabIdx = tabIdx;
    this.prodService.tabIdx = this.tabIdx;
  }


  //#endregion

  //#region Action Methods
  addPsmModule() {
    let module = new PsmModule();
    module.psmProductId = this.prodService.product.psmProductId;
    this.editPsmModule(module);
  }

  editPsmModule(value: PsmModule): void {
    if (value) {
      const dialogRef = this.dialogService.open(PsmModuleEditDialog, {
        width: '721px',
        height: '420px',
        data: {
          module: value,
          product: this.prodService.product,
        }
      });

      dialogRef.afterClosed().subscribe(res => {
        console.log('editPsmModule-> res:', res);
        if (res && res.moduleId) {
          if (this.moduleGrid) {
            this.moduleGrid.refreshClick();
          }
        }
        else {
          this.initFields();
        }


      });

    }
  }


  //#endregion
}