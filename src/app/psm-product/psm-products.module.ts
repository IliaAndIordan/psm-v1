import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PsmCoreModule } from '../@core/core.module';
import { PsmShareModule } from '../@share/share.module';
import { PsmProductRoutingModule, routedComponents } from './psm-product-routing.module';
import { PsmProductService } from './psm-product.service';
import { PsmProductFilterPanelComponent } from './filter-panel/psm-product-filter-panel.component';
import { PsmProductEditDialog } from '../@share/sx-common/modals/psm-product/edit/psm-product-edit.dialog';
import { GridPsmModuleTableDataSource } from './grids/grid-psm-module/grid-psm-module.datasource';
import { GridPsmModuleComponent } from './grids/grid-psm-module/grid-psm-module.component';
import { GridPsmConnectedModuleComponent } from './grids/grid-psm-connected-module/grid-psm-connected-module.component';
import { GridPsmConnectedModuleTableDataSource } from './grids/grid-psm-connected-module/grid-psm-connected-module.component.datasource';



@NgModule({
  
  imports: [
    CommonModule,
    PsmCoreModule,
    PsmShareModule,
    //
    PsmProductRoutingModule,
  ],
  declarations: [
    routedComponents,
    PsmProductFilterPanelComponent,
    GridPsmModuleComponent,
    GridPsmConnectedModuleComponent,
  ],
  entryComponents: [
    PsmProductEditDialog,
  ],
  providers:[
    PsmProductService,
    GridPsmModuleTableDataSource,
    GridPsmConnectedModuleTableDataSource,
  ]
})
export class PmsProductsModule { }
