import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs';
import { UserModel } from '../@core/clients/api/auth/dto';
import { SxApiCompanyClient } from '../@core/clients/api/company/api-client';
import { CompanyModel, CompanyProduct } from '../@core/clients/api/company/dto';
import { PsmProduct } from '../@core/clients/api/psm-product/dto';
import { PsmUserClient } from '../@core/clients/api/user/api-client';
import { ResponseUserTable, RespUserTableData, UserTableCriteria } from '../@core/clients/api/user/dto';
import { CurrentUserService } from '../@core/clients/current-user.service';
import { AppStore } from '../@core/constants/app-storage.const';
import { PsmProductService } from '../psm-product/psm-product.service';


export const FP_IN_KEY = 'psm_company_filter_panel_in';
export const TABIDX_KEY = 'psm_company_tab_idx';

@Injectable({
    providedIn: 'root',
})
export class PsmCompanyService {

    spmCompanyPanelOpen = new BehaviorSubject<CompanyModel>(undefined);
    spmCompany: CompanyModel;

    constructor(
        private cus: CurrentUserService,
        private comClient: SxApiCompanyClient,
        private prodService: PsmProductService,
        private uClient: PsmUserClient) {
            this.productChanged = this.prodService.productChanged.subscribe(product=>{
            
            });
    }

    //#region CompanyModel

    get company(): CompanyModel {
        return this.cus.company;
    }

    set company(value: CompanyModel) {
        this.cus.company = value
    }


    get user(): UserModel {
        return this.cus.user;
    }

    get sxProducts(): CompanyProduct[] {
        return this.cus.sxProducts;
    }


    productChanged:Subscription;
    get product(): PsmProduct {
        return this.prodService.product;
    }

    set product(value: PsmProduct) {
        this.prodService.product = value;
    }

    //#endregion

    //#region FP and Tab Idx

    panelInChanged = new BehaviorSubject<boolean>(true);

    get panelIn(): boolean {
        let rv = true;
        const valStr = localStorage.getItem(FP_IN_KEY);
        if (valStr) {
            rv = JSON.parse(valStr) as boolean;
        }
        return rv;
    }

    set panelIn(value: boolean) {
        localStorage.setItem(FP_IN_KEY, JSON.stringify(value));
        this.panelInChanged.next(value);
    }

    tabIdxChanged = new BehaviorSubject<number>(undefined);

    get tabIdx(): number {
        let rv = 0;
        const dataStr = localStorage.getItem(TABIDX_KEY);
        //console.log('selFolderId-> dataStr', dataStr);
        if (dataStr) {
            try {
                rv = parseInt(dataStr, 10);
            }
            catch {
                localStorage.removeItem(TABIDX_KEY);
                rv = 1;
            }

        }
        // console.log('selTabIdx-> rv', rv);
        return rv;
    }

    set tabIdx(value: number) {
        // console.log('selTabIdx->', value);
        const oldValue = this.tabIdx;

        localStorage.setItem(TABIDX_KEY, JSON.stringify(value));
        if (oldValue !== value) {
            this.tabIdxChanged.next(value);
        }
    }

    //#endregion

    //#region User List

    usersCriteria: UserTableCriteria;
    usersCriterisChanged = new BehaviorSubject<UserTableCriteria>(undefined);

    //#endregion

    //#region Profile (User Edit)
    
    profileChanged = new BehaviorSubject<UserModel>(undefined);

    get profile(): UserModel {
        let rv:UserModel;
        const valStr = localStorage.getItem(AppStore.psm_company_profile);
        if (valStr) {
            rv = Object.assign(new UserModel, JSON.parse(valStr) );
        }
        return rv;
    }

    set profile(value: UserModel) {
        localStorage.setItem(AppStore.psm_company_profile, JSON.stringify(value));
        this.profileChanged.next(value);
    }

    

    //#endregion

    //#region Data

    loadCompany(id: number): Promise<CompanyModel> {

        let promise = new Promise<CompanyModel>((resolve, reject) => {
            this.comClient.companyById(id)
                .subscribe((resp: CompanyModel) => {
                    //console.log('loadCompany-> resp', resp);
                    resolve(resp);
                }, msg => {
                    console.log('loadCompany -> msg', msg);
                    reject(msg);
                }
                );

        });

        return promise;
    }

    userResp: RespUserTableData;
    
    loadUsers(req: UserTableCriteria): Promise<UserModel[]> {
        let rv = new Array<UserModel>();
        let promise = new Promise<UserModel[]>((resolve, reject) => {
            this.uClient.userTable(req)
                .subscribe((resp: ResponseUserTable) => {
                    //console.log('loadUsers -> resp', resp);
                    if (resp) {
                        this.userResp = resp.data;
                        if (this.userResp.users && this.userResp.users.length > 0) {
                            this.userResp.users.forEach(element => {
                                rv.push(UserModel.fromJSON(element));
                            });
                        }
                    }
                    resolve(rv);
                }, msg => {
                    console.log('loadUsers -> msg', msg);
                    reject(msg);
                });

        });

        return promise;
    }
    //#endregion


}
