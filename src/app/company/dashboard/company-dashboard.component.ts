import { Component, OnDestroy, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
import { UserModel } from "src/app/@core/clients/api/auth/dto";
import { SxApiCompanyClient } from "src/app/@core/clients/api/company/api-client";
import { CompanyModel, ResponseCompanySave } from "src/app/@core/clients/api/company/dto";
import { CurrentUserService } from "src/app/@core/clients/current-user.service";
import { COMMON_IMG_LOGO_RED, URL_NO_IMG } from "src/app/@core/constants/app-storage.const";
import { SmProductOpt, SmProductViewModel } from "src/app/@core/pipes/sx-product.pipe";
import { AppService } from "src/app/@core/services/app.service";
import { SxSpinnerComponent } from "src/app/@share/sx-common/sx-spinner/sx-spinner.component";
import { SxSpinnerService } from "src/app/@share/sx-common/sx-spinner/sx-spinner.service";
import { PsmCompanyService } from "../company.service";


@Component({
    templateUrl: './company-dashboard.component.html',
})

export class PsmCompanyDashboardComponent implements OnInit, OnDestroy {

    sxLogo = COMMON_IMG_LOGO_RED;

    company: CompanyModel;
    user: UserModel;
    userChanged: Subscription;

    productOpt: SmProductViewModel[];

    liveryUrl:string;
    noImageUrl = URL_NO_IMG;

    // public dialogService: MatDialog
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinnerService:SxSpinnerService,
        private appService: AppService,
        private cus: CurrentUserService,
        private comService: PsmCompanyService,
        private comClient:SxApiCompanyClient,
        public dialogService: MatDialog) {
    }


    ngOnInit(): void {
        //this.env = environment.abreviation;
        this.userChanged = this.cus.userChanged.subscribe((user: UserModel) => {
            this.initFields();
        });
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
    }

    initFields() {
        this.company = this.cus.company;
        this.user = this.cus.user;
        this.liveryUrl =  this.company.sxLiveryUrl;
        this.initProductOpt();
    }

    initProductOpt() {
        this.productOpt = new Array<SmProductViewModel>();
        SmProductOpt.forEach(product => {
            if (this.company.sxProducts && this.company.sxProducts.length > 0) {
                const cp = this.company.sxProducts.find(x => x.productId === product.id);
                product.isGranted = cp ? true : false;
                this.productOpt.push(product);
            } else {
                product.isGranted = false;
                this.productOpt.push(product);
            }
        });
    }

    addProduct(product: SmProductViewModel) {
        console.log('addProduct -> product:', product);
        
        if (product) {
           
            if (product.isGranted) {
                this.spinnerService.display(true);
                this.comClient.companySmProductAdd(this.company.companyId, product.id)
                    .subscribe((company: CompanyModel) => {
                        this.spinnerService.display(false);
                        if (company) {
                            this.cus.company = company;
                            this.toastr.success('Compnay', 'Operation Succesfull: Compnay Product Add');
                            this.initFields();
                        }
                    },
                    err => {
                        console.error('Observer got an error: ' + err);
                        this.spinnerService.display(false);
                        this.toastr.error('Compnay', 'Operation Failed: Compnay Product Add: ' + err);
                    },
                    () => console.log('Observer got a complete notification'));
            } else {
                console.log('product to delete -> products:', this.company.sxProducts);
                const cp = this.company.sxProducts.find(x => x.productId === product.id);
                console.log('product to delete -> cp:', cp);
                if (cp) {
                    this.spinnerService.display(true);
                    this.comClient.companySmProductDelete(this.company.companyId, cp.cpId)
                        .subscribe((company: CompanyModel) => {
                            this.spinnerService.display(false);
                            if (company) {
                                this.cus.company = company;
                                this.toastr.success('Compnay', 'Operation Succesfull: Compnay Product Delete');
                            }
                        },
                        err => {
                            console.error('Observer got an error: ' + err);
                            this.spinnerService.display(false);
                            this.toastr.error('Compnay', 'Operation Failed: Compnay Sm product delete: ' + err);
                        },
                        () => console.log('Observer got a complete notification'));
                }

            }



        }


    }

    

   

    generateLiveryClick(){
        if(this.company){
            
        }
        
    }
}