import { Component, OnInit } from '@angular/core';

@Component({
  template: '<div class="psm-page"> <router-outlet></router-outlet> </div>'
})
export class PmsCompanyComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
