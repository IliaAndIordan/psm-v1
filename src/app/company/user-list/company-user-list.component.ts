import { Component, OnDestroy, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
import { UserModel } from "src/app/@core/clients/api/auth/dto";
import { CompanyModel } from "src/app/@core/clients/api/company/dto";
import { UserTableCriteria } from "src/app/@core/clients/api/user/dto";
import { CurrentUserService } from "src/app/@core/clients/current-user.service";
import { AppRoutes } from "src/app/@core/constants/app-routes.const";
import { AppStore, COMMON_IMG_LOGO_RED, URL_NO_IMG } from "src/app/@core/constants/app-storage.const";
import { AppService } from "src/app/@core/services/app.service";
import { PsmUserEditDialog } from "src/app/@share/sx-common/modals/user/edit/psm-user-edit.dialog";
import { SxSpinnerService } from "src/app/@share/sx-common/sx-spinner/sx-spinner.service";
import { PsmCompanyService } from "../company.service";


@Component({
    templateUrl: './company-user-list.component.html',
})

export class PsmCompanyUserListComponent implements OnInit, OnDestroy {

    sxLogo = COMMON_IMG_LOGO_RED;

    company: CompanyModel;
    user: UserModel;
    users: UserModel[];
    usersCount: number;
    userChanged: Subscription;

    liveryUrl: string;
    noImageUrl = URL_NO_IMG;

    // public dialogService: MatDialog
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinnerService: SxSpinnerService,
        private appService: AppService,
        private cus: CurrentUserService,
        private comService: PsmCompanyService,
        public dialogService: MatDialog) {
    }


    ngOnInit(): void {
        //this.env = environment.abreviation;
        this.userChanged = this.cus.userChanged.subscribe((user: UserModel) => {
            this.initFields();
        });
        this.initFields();
        this.loadUsers();
    }

    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
    }

    initFields() {
        this.company = this.cus.company;
        this.user = this.cus.user;
        this.liveryUrl = this.company.sxLiveryUrl;
    }

    getCriteria(): UserTableCriteria {
        let rv = new UserTableCriteria();
        rv.companyId = this.company.companyId;
        rv.offset = 0;
        rv.limit = 1000;
        return rv;
    }

    gotoUserProfile(user: UserModel) {
        if (user) {
            localStorage.setItem(AppStore.psm_company_profile, JSON.stringify(user));
            this.router.navigate([AppRoutes.Root, AppRoutes.company, AppRoutes.profile]);
        }
    }

    generateLiveryClick() {
        if (this.company) {

        }

    }

    loadUsers() {
        this.spinnerService.show();
        this.comService.loadUsers(this.getCriteria())
            .then((users: UserModel[]) => {
                //console.log('loadUsers -> users:', users);
                this.spinnerService.hide();
                this.users = users;
                this.usersCount = this.comService.userResp.rowsCount.totalRows;
            });

    }

    editUser(data: UserModel): void {
        //console.log('editUser-> data:', data);

        if (data) {
            const dialogRef = this.dialogService.open(PsmUserEditDialog, {
                width: '721px',
                height: '420px',
                data: {
                    user: data,
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                //console.log('editUser-> res:', res);
                if (res) {
                    this.loadUsers();
                }
                else {
                    this.initFields();
                }
            });
        }
    }
}