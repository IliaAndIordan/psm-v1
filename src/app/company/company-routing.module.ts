import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppRoutes } from '../@core/constants/app-routes.const';
import { PmsCompanyComponent } from './company.component';
import { PsmCompanyHomeComponent } from './home.component';
import { AuthGuard } from '../@core/guards/auth.guard.service';
import { PsmCompanyDashboardComponent } from './dashboard/company-dashboard.component';
import { PsmCompanyUserListComponent } from './user-list/company-user-list.component';
import { PmsCompanyUserComponent } from './user/user.component';
import { PsmCompanyPmsProductsComponent } from './pms-products/pms-products.component';
import { PsmCompanyPmsProjectsComponent } from './pms-projects/pms-projects.component';
// Component



const routes: Routes = [
  {
    path: AppRoutes.Root, component: PmsCompanyComponent,
    
    children: [
      { path: AppRoutes.Root, component: PsmCompanyHomeComponent,
        
        children: [
          { path: AppRoutes.Root,   pathMatch: 'full', component: PsmCompanyDashboardComponent, canActivate: [AuthGuard] },
          { path: AppRoutes.info, component: PsmCompanyDashboardComponent },
          { path: AppRoutes.users, component: PsmCompanyUserListComponent },
          { path: AppRoutes.profile, component: PmsCompanyUserComponent },
          { path: AppRoutes.products, component: PsmCompanyPmsProductsComponent },
          { path: AppRoutes.projects, component: PsmCompanyPmsProjectsComponent },
          /*
          { path: AppRoutes.create, component: AirlineCreateFormComponent },
          { path: AppRoutes.info, component: AirlineInfoComponent,  
            children: [
            { path: AppRoutes.Root,   pathMatch: 'full', component: AirlineInfoDashboardComponent },
            { path: AppRoutes.dashboard, component: AirlineInfoDashboardComponent },
            { path: AppRoutes.aircafts, component: AirlineInfoAircraftsComponent },
          ]},
          // { path: AppRoutes.Dashboard, component: DashboardComponent },
          */
        ]
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PsmCompanyRoutingModule { }

export const routedComponents = [
  PmsCompanyComponent, 
  PsmCompanyHomeComponent,
  PsmCompanyDashboardComponent,
  PsmCompanyUserListComponent,
  PmsCompanyUserComponent,
  PsmCompanyPmsProductsComponent,
  PsmCompanyPmsProjectsComponent,
];
