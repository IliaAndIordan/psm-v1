import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PsmCompanyPmsProductsComponent } from './pms-products.component';

describe('PsmCompanyPmsProductsComponent', () => {
  let component: PsmCompanyPmsProductsComponent;
  let fixture: ComponentFixture<PsmCompanyPmsProductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PsmCompanyPmsProductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PsmCompanyPmsProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
