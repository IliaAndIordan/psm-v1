import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { PsmProduct, PsmProductTableCriteria, ResponsePsmProductTable } from 'src/app/@core/clients/api/psm-product/dto';
import { ApiPsmProductClient } from 'src/app/@core/clients/api/psm-product/api-client';
import { PsmCompanyService } from '../../company.service';

export const KEY_CRITERIA = 'psm_grid_company_psm_products_criteria';
@Injectable()
export class PsmGridCompanyPsmProductTableDataSource extends DataSource<PsmProduct> {

    seleted: PsmProduct;

    private _data: Array<PsmProduct>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<PsmProduct[]> = new BehaviorSubject<PsmProduct[]>([]);
    listSubject = new BehaviorSubject<PsmProduct[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<PsmProduct> {
        return this._data;
    }

    set data(value: Array<PsmProduct>) {
        this._data = value;
        this.listSubject.next(this._data as PsmProduct[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: ApiPsmProductClient,
        private service: PsmCompanyService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<PsmProductTableCriteria>();

    public get criteria(): PsmProductTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: PsmProductTableCriteria;
        if (onjStr) {
            obj = Object.assign(new PsmProductTableCriteria(), JSON.parse(onjStr));
            if(!obj.companyId || obj.companyId !== this.cus.company.companyId){
                obj.companyId = this.cus.company.companyId;
                localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
            }
            
        } else {
            obj = new PsmProductTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'udate';
            obj.sortDesc = true;
            obj.actorId = undefined;
            obj.companyId = this.cus.company.companyId;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: PsmProductTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<PsmProduct[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<PsmProduct>> {
        //console.log('loadData->');
        //console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;
        
        return new Observable<Array<PsmProduct>>(subscriber => {

            this.client.psmProductTable(this.criteria)
                .subscribe((resp: ResponsePsmProductTable) => {
                    //const resp = Object.assign(new ResponseAmsUserTableData(), res);
                    //console.log('loadData-> resp=', resp);
                    this.data = new Array<PsmProduct>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.psmProducts && resp.data.psmProducts.length > 0) {
                            resp.data.psmProducts.forEach(iu => {
                                const obj = PsmProduct.fromJSON(iu);
                                this.data.push(obj);
                            });
                        }

                        this.itemsCount = resp.data.rowsCount?resp.data.rowsCount.totalRows:0;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    //console.log('loadData-> data=', this.data);
                    //console.log('loadData-> itemsCount=', this.itemsCount);
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<PsmProduct>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
