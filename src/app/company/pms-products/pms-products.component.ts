import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { CompanyModel } from 'src/app/@core/clients/api/company/dto';
import { PsmProduct } from 'src/app/@core/clients/api/psm-product/dto';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { PsmProductEditDialog } from 'src/app/@share/sx-common/modals/psm-product/edit/psm-product-edit.dialog';
import { SxSpinnerService } from 'src/app/@share/sx-common/sx-spinner/sx-spinner.service';
import { PsmGridCompanyPsmProductComponent } from './grid/grid-pms-product.component';

@Component({
  selector: 'pms-company-pms-products',
  templateUrl: './pms-products.component.html',
})
export class PsmCompanyPmsProductsComponent implements OnInit {

  @ViewChild(PsmGridCompanyPsmProductComponent, { static: false }) productTable: PsmGridCompanyPsmProductComponent;

  constructor(
    private cus: CurrentUserService,
    private toastr: ToastrService,
    private spinnerService: SxSpinnerService,
    public dialogService: MatDialog,) { }

  ngOnInit(): void {
  }

  initFields() {

  }


  //#region  Action Methods
  addPsmProduct(company: CompanyModel): void {
    let product = new PsmProduct();
    product.companyId = company?company.companyId: this.cus.company.companyId;
    this.editPsmProduct(product);
  }

  editPsmProduct(data: PsmProduct): void {
    //console.log('editPsmProduct-> data:', data);
    if (data) {
      const dialogRef = this.dialogService.open(PsmProductEditDialog, {
        width: '721px',
        height: '420px',
        data: {
          product: data,
        }
      });

      dialogRef.afterClosed().subscribe(res => {
        console.log('editPsmProduct-> res:', res);
        if (res) {
          if (this.productTable) { this.productTable.refreshClick(); }
          this.initFields();
        }
        else {
          this.initFields();
        }


      });

    }


  }

}
