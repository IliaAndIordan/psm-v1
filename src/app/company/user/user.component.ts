import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserModel } from 'src/app/@core/clients/api/auth/dto';
import { CompanyModel } from 'src/app/@core/clients/api/company/dto';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { PsmCompanyService } from '../company.service';

@Component({
  selector: 'pms-company-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class PmsCompanyUserComponent implements OnInit {

  company: CompanyModel;
  user: UserModel;
  userChanged: Subscription;

  profile: UserModel;
  profileChanged: Subscription;

  canEdit:boolean;

  constructor(
    private cus: CurrentUserService,
    private comService: PsmCompanyService) {

  }

  ngOnInit(): void {
    //this.env = environment.abreviation;
    this.userChanged = this.cus.userChanged.subscribe((user: UserModel) => {
      this.initFields();
    });
    this.profileChanged = this.comService.profileChanged.subscribe((profile: UserModel) => {
      this.initFields();
    });
    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.userChanged) { this.userChanged.unsubscribe(); }
  }

  initFields() {
    this.company = this.cus.company;
    this.user = this.cus.user;
    this.profile = this.comService.profile;
    this.canEdit = this.profile && this.user && 
      (this.user.userId === this.profile.userId || 
        this.cus.isAdmin || 
        (this.cus.isCompanyAdmin && this.user.companyId === this.profile.companyId));
  }


}
