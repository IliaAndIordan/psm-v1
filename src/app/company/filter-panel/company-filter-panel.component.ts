import { Component, EventEmitter, OnDestroy, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
import { CompanyModel } from "src/app/@core/clients/api/company/dto";
import { CurrentUserService } from "src/app/@core/clients/current-user.service";
import { AppRoutes } from "src/app/@core/constants/app-routes.const";
import { SxSpinnerService } from "src/app/@share/sx-common/sx-spinner/sx-spinner.service";
import { PsmCompanyService } from "../company.service";
import { URL_COMMON_IMAGE_SX, URL_NO_IMG } from "src/app/@core/constants/app-storage.const";
import { PsmUserRole } from "src/app/@core/clients/api/auth/enums";


@Component({
    selector: 'psm-company-filter-panel',
    templateUrl: './company-filter-panel.component.html',
})
export class PsmCompanyFilterPanelComponent implements OnInit, OnDestroy {

    @Output() companyEditClick: EventEmitter<CompanyModel> = new EventEmitter<CompanyModel>();

    routs = AppRoutes;
    company: CompanyModel;
    companyChanged: Subscription;

    tabIdx: number;
    tabIdxChanged: Subscription;
    canEdit: boolean;

    logoUrl:string;
    liveryUrl:string;
    noLogoUrl = URL_NO_IMG;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinnerService: SxSpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private comService: PsmCompanyService) {

    }


    ngOnInit(): void {

        this.companyChanged = this.cus.companyChanged.subscribe((company: CompanyModel) => {
            this.initFields();
        });

        this.tabIdxChanged = this.comService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.companyChanged) { this.companyChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.company = this.cus.company;
        this.logoUrl = this.company && this.company.sxLogoUrl ? this.company.sxLogoUrl : this.noLogoUrl;
        this.liveryUrl = this.company && this.company.sxLiveryUrl ? this.company.sxLiveryUrl : this.noLogoUrl;
        this.canEdit = ((this.cus.user.companyId === this.company.companyId) &&  (this.cus.user.role < PsmUserRole.User));
        this.tabIdx = this.comService.tabIdx;
    }

    //#region Action Methods

    refreshCompany(){
        //console.log('refreshCompany -> company:', this.company);
        this.spinnerService.show();
        this.comService.loadCompany(this.company.companyId)
        .then(com=>{
            //console.log('refreshCompany -> com:', com);
            this.spinnerService.hide();
            this.cus.company = com;

        });

    }

    ediCompany(){
        if(this.company && this.canEdit){
            this.companyEditClick.emit(this.company);
        }
    }

    //#endregion

}
