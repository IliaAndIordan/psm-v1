import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PsmShareModule } from '../@share/share.module';
import { PsmCompanyService } from './company.service';
import { PsmCompanyRoutingModule, routedComponents } from './company-routing.module';
import { PsmCompanyFilterPanelComponent } from './filter-panel/company-filter-panel.component';
import { PsmCoreModule } from '../@core/core.module';
import { PsmGridCompanyPsmProductTableDataSource } from './pms-products/grid/grid-pms-product.datasource';
import { PsmGridCompanyPsmProductComponent } from './pms-products/grid/grid-pms-product.component';
import { PsmProductService } from '../psm-product/psm-product.service';
import { PsmGridCompanyPsmProjectTableDataSource } from './pms-projects/grid/grid-pms-project.datasource';
import { PsmGridCompanyPsmProjectComponent } from './pms-projects/grid/grid-pms-project.component';





@NgModule({
  imports: [
    CommonModule,
    PsmCoreModule,
    PsmShareModule,
    //
    PsmCompanyRoutingModule,
  ],
  declarations: [
    routedComponents,
    PsmCompanyFilterPanelComponent,
    PsmGridCompanyPsmProductComponent,
    PsmGridCompanyPsmProjectComponent,
  ],
  exports: [
    PsmCoreModule
  ],
  entryComponents: [
],
providers:[
  PsmProductService,
  PsmCompanyService,
  PsmGridCompanyPsmProductTableDataSource,
  PsmGridCompanyPsmProjectTableDataSource,
]
})
export class PsmCompanyModule { }
