import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PsmCompanyPmsProjectsComponent } from './pms-projects.component';

describe('PmsProjectsComponent', () => {
  let component: PsmCompanyPmsProjectsComponent;
  let fixture: ComponentFixture<PsmCompanyPmsProjectsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PsmCompanyPmsProjectsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PsmCompanyPmsProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
