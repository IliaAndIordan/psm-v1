import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { PsmProduct } from 'src/app/@core/clients/api/psm-product/dto';
import { PsmCompanyService } from '../../company.service';
import { PsmProject, PsmProjectTableCriteria, ResponsePsmProjectTable } from 'src/app/@core/clients/api/project/dto';
import { ApiPsmProjectClient } from 'src/app/@core/clients/api/project/api-client';

export const KEY_CRITERIA = 'psm_grid_company_psm_project_criteria';
@Injectable()
export class PsmGridCompanyPsmProjectTableDataSource extends DataSource<PsmProject> {

    seleted: PsmProject;

    private _data: Array<PsmProject>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<PsmProject[]> = new BehaviorSubject<PsmProject[]>([]);
    listSubject = new BehaviorSubject<PsmProject[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<PsmProject> {
        return this._data;
    }

    set data(value: Array<PsmProject>) {
        this._data = value;
        this.listSubject.next(this._data as PsmProject[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: ApiPsmProjectClient,
        private service: PsmCompanyService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<PsmProjectTableCriteria>();

    public get criteria(): PsmProjectTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: PsmProjectTableCriteria;
        if (onjStr) {
            obj = Object.assign(new PsmProjectTableCriteria(), JSON.parse(onjStr));
            if(!obj.companyId || obj.companyId !== this.cus.company.companyId){
                obj.companyId = this.cus.company.companyId;
                localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
            }
            
        } else {
            obj = new PsmProjectTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'udate';
            obj.sortDesc = true;
            obj.productId = undefined;
            obj.statusId = undefined;
            obj.companyId = this.cus.company.companyId;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: PsmProjectTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<PsmProject[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<PsmProject>> {
        //console.log('loadData->');
        //console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;
        
        return new Observable<Array<PsmProject>>(subscriber => {

            this.client.psmProjectTable(this.criteria)
                .subscribe((resp: ResponsePsmProjectTable) => {
                    //const resp = Object.assign(new ResponseAmsUserTableData(), res);
                    //console.log('loadData-> resp=', resp);
                    this.data = new Array<PsmProject>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.projects && resp.data.projects.length > 0) {
                            resp.data.projects.forEach(iu => {
                                const obj = PsmProject.fromJSON(iu);
                                this.data.push(obj);
                            });
                        }

                        this.itemsCount = resp.data.rowsCount?resp.data.rowsCount.totalRows:0;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    //console.log('loadData-> data=', this.data);
                    //console.log('loadData-> itemsCount=', this.itemsCount);
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<PsmProject>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
