import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CompanyModel } from 'src/app/@core/clients/api/company/dto';
import { PsmProject } from 'src/app/@core/clients/api/project/dto';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { PsmProjectEditDialog } from 'src/app/@share/sx-common/modals/pms-project/edit/psm-project-edit.dialog';
import { PsmGridCompanyPsmProjectComponent } from './grid/grid-pms-project.component';

@Component({
  selector: 'pma-company-pms-projects',
  templateUrl: './pms-projects.component.html',
})
export class PsmCompanyPmsProjectsComponent implements OnInit {

  @ViewChild(PsmGridCompanyPsmProjectComponent, { static: false }) projectTable: PsmGridCompanyPsmProjectComponent;
  constructor(
    private cus:CurrentUserService,
    public dialogService: MatDialog,) { }

  ngOnInit(): void {
  }

  initFields(){

  }
  //#region  Action Methods
  addPsmProject(company: CompanyModel): void {
    let dto = new PsmProject();
    dto.companyId = company?company.companyId: this.cus.company.companyId;
    this.editPsmProject(dto);
  }

  editPsmProject(data: PsmProject): void {
    //console.log('editPsmProduct-> data:', data);
    if (data) {
      
      const dialogRef = this.dialogService.open(PsmProjectEditDialog, {
        width: '721px',
        height: '420px',
        data: {
          project: data,
          company: this.cus.company
        }
      });

      dialogRef.afterClosed().subscribe(res => {
        console.log('editPsmProject-> res:', res);
        if (res) {
          if (this.projectTable) { this.projectTable.refreshClick(); }
          this.initFields();
        }
        else {
          this.initFields();
        }


      });
      
    }


  }

}
