import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ExpandTab, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, SpinExpandIconTrigger } from '../@core/constants/animations-triggers';
import { PsmCompanyService } from './company.service';
import { CompanyModel } from '../@core/clients/api/company/dto';
import { COMMON_IMG_LOGO_RED } from '../@core/constants/app-storage.const';
import { Animate } from '../@core/constants/animation.const';
import { IInfoMessage, InfoMessageDialog } from '../@share/sx-common/modals/info-message/info-message.dialog';
import { MatDrawerMode, MatSidenav } from '@angular/material/sidenav';
import { AppService } from '../@core/services/app.service';
import { CurrentUserService } from '../@core/clients/current-user.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { UserModel } from '../@core/clients/api/auth/dto';
import { PsmCompanyEditDialog } from '../@share/sx-common/modals/company-edit/company-edit.dialog';


@Component({
  templateUrl: './home.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab]
})

export class PsmCompanyHomeComponent implements OnInit, OnDestroy {

  @ViewChild('snav') snav: MatSidenav;
  @ViewChild('spmCompanyPanel') spmCompanyPanel: any; //AmsSidePanelModalComponent;

  company: CompanyModel;
  spmCompany: CompanyModel;
  spmCompanyPanelOpen: Subscription;


  sxLogo = COMMON_IMG_LOGO_RED;
  sidebarClass = 'sidebar-closed';
  // --- Side Tab
  expandTabVar: string = Animate.in;
  showTabContentsVar: string = Animate.hide;

  snavmode: MatDrawerMode;
  sideNavState: string = Animate.open;
  mobileQuery: MediaQueryList;

  panelInVar = Animate.in;
  panelInChanged: Subscription;
  private _mobileQueryListener: () => void;

  user:UserModel;
  userChanged: Subscription;

  // public dialogService: MatDialog
  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    public dialog: MatDialog,
    private appService: AppService,
    private cus: CurrentUserService,
    private comService: PsmCompanyService,
    public dialogService: MatDialog) {
      this.mobileQuery = media.matchMedia('(max-width: 600px)');
      this._mobileQueryListener = () => changeDetectorRef.detectChanges();
      this.mobileQuery.addListener(this._mobileQueryListener);
     }


  ngOnInit(): void {
    //this.env = environment.abreviation;
    const tmp = this.panelIn;
    this.panelInChanged = this.comService.panelInChanged.subscribe((panelIn: boolean) => {
      const tmp = this.panelIn;
    });
    this.spmCompanyPanelOpen = this.comService.spmCompanyPanelOpen.subscribe((com: CompanyModel) => {
      this.spmCompanyOpen(com);
    });
    this.spmCompanyPanelOpen = this.comService.spmCompanyPanelOpen.subscribe((com: CompanyModel) => {
      this.spmCompanyOpen(com);
    });
    this.userChanged = this.cus.userChanged.subscribe((user: UserModel) => {
      this.initFields();
    });
    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
    if (this.spmCompanyPanelOpen) { this.spmCompanyPanelOpen.unsubscribe(); }
  }

  initFields() {
    
    this.snavmode = this.cus.localSettings.snavLeftWidth > 0 ? 'side' : 'over';
    this.company = this.cus.company;
    this.user = this.cus.user;
    if(this.snav){
      this.snav.opened = this.panelIn;
    }
  }

 

  //#region  Action Methods

  companyEdit(value:CompanyModel): void {
    
    const dialogRef = this.dialogService.open(PsmCompanyEditDialog, {
      width: '750px',
      height: '520px',
      data: { company: value}
    });

    dialogRef.afterClosed().subscribe(company => {
      console.log('The dialog was closed - company:', company);
      if(company && company.compabyId){
        this.company = company;
        this.initFields();
      }
      
    });
    
  }

  exapandTabClick() {
    this.expandTabVar = this.expandTabVar === Animate.in ? Animate.out : Animate.in;
    this.showTabContentsVar = this.showTabContentsVar === Animate.show ? Animate.hide : Animate.show;
  }
  //#endregion

  //#region snav

  snavToggleClick() {
    if (this.snav) {
      this.snav.toggle();
      this.panelIn = this.snav.opened;
    }
  }

  //#endregion

  //#region SPM

  showMessage(data: IInfoMessage) {
    console.log('showMessage-> data:', data);

    const dialogRef = this.dialog.open(InfoMessageDialog, {
      width: '721px',
      height: '320px',
      data: data
    });

    dialogRef.afterClosed().subscribe(res => {
      console.log('showMessage-> res:', res);
      if (res) {

      }
    });

  }

  sidebarToggle() {
    this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
  }

  spmCompanyClose(): void {
    if (this.spmCompanyPanel) {
      this.spmCompanyPanel.closePanel();
    }
  }

  spmCompanyOpen(com: CompanyModel) {
    this.spmCompany = com;
    if (this.spmCompanyPanel && this.spmCompany) {
      this.spmCompanyPanel.expandPanel();
    }
    else {
      this.spmCompanyClose();
    }

  }


  //#endregion

  //#region Filter Panel

  public closeFilterPanel(): void {
    this.panelIn = false;
  }

  public expandFilterPanel(): void {
    // this.treeComponent.openPanelClick();
    this.panelIn = true;
  }

  get panelIn(): boolean {
    let rv = this.comService.panelIn;
    this.panelInVar = rv ? Animate.out : Animate.in;
    // console.log('panelIn-> hps:', this.hps);
    return this.comService.panelIn;
  }

  set panelIn(value: boolean) {
    this.comService.panelIn = value;
    this.panelInVar = value ? Animate.out : Animate.in;
  }
  //#endregion


}
