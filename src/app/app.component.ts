import { Component, OnDestroy, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { default as projectInfo } from 'package.json';
import { ToastrService } from 'ngx-toastr';
import { SxSpinnerService } from './@share/sx-common/sx-spinner/sx-spinner.service';
import { Subscription, timer } from 'rxjs';
import { COMMON_IMG_LOGO_RED } from './@core/constants/app-storage.const';
import { AppService } from './@core/services/app.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  logoImgUrl = COMMON_IMG_LOGO_RED;
  title = projectInfo.name;
  appVersion = projectInfo.version;
  env  = environment.production?'PD':'DV';
  

  constructor(
    private appService: AppService,
    private toastr: ToastrService,
    private spinnerService: SxSpinnerService) {
  }


  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  toggleSpinner(){
    this.spinnerService.display(true);
    setTimeout(() => {
      this.spinnerService.setMessage('1000');
    }, 1000);
    setTimeout(() => {
      this.spinnerService.display(false);
    }, 2000);
    
  }

  showToastr() {
    this.toastr.success('Hello world!', 'Toastr fun!');
  }
}
