import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PsmCommonModule } from './sx-common/psm-common.module';
import { PsmCoreModule } from '../@core/core.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PsmCoreModule,
    PsmCommonModule,
  ],
  exports:[
    PsmCoreModule,
    PsmCommonModule,
  ]
})
export class PsmShareModule { }
