import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { AppStore, COMMON_IMG_LOGO_RED } from 'src/app/@core/constants/app-storage.const';
import { ToastrService } from 'ngx-toastr';
import { SxSpinnerService } from '../../sx-spinner/sx-spinner.service';
import { AppService } from 'src/app/@core/services/app.service';
import { MatDialog } from '@angular/material/dialog';
import { LoginModal } from '../../modals/login/login.dialog';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AppRoutes } from 'src/app/@core/constants/app-routes.const';
import { UserModel } from 'src/app/@core/clients/api/auth/dto';
import { CompanyModel } from 'src/app/@core/clients/api/company/dto';
import { AnimateTimings } from '@angular/animations';
import { Animate } from 'src/app/@core/constants/animation.const';
import { DelayFadeInTrigger, ExpandTab, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, SpinExpandIconTrigger } from 'src/app/@core/constants/animations-triggers';

@Component({
  selector: 'sx-toolbar-main',
  templateUrl: './sx-toolbar-main.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    DelayFadeInTrigger,
    ExpandTab],
})
export class SxToolbarMainComponent implements OnInit, OnDestroy, OnChanges {

  @Input() panelIn: boolean;
  @Input() paneBtnHide: boolean = false;
  @Output() login: EventEmitter<void> = new EventEmitter<void>();
  @Output() snavToggle: EventEmitter<void> = new EventEmitter<void>();

  logoImgUrl = COMMON_IMG_LOGO_RED;
  title: string;
  appVersion: string;
  env: string;
  paneBtnVar: string = Animate.hide;
  paneBtnVarOp: string = Animate.show;

  rootes = AppRoutes;

  user: UserModel;
  userChanged: Subscription;

  company: CompanyModel;
  companyChanged: Subscription;
  isLoggedIn: boolean;


  constructor(
    private router: Router,
    private appService: AppService,
    private toastr: ToastrService,
    private spinnerService: SxSpinnerService,
    public dialogService: MatDialog,
    private cus: CurrentUserService) { }


  ngOnInit(): void {
    this.title = this.appService.title;
    this.appVersion = this.appService.version;
    this.env = this.appService.env;

    this.userChanged = this.cus.userChanged.subscribe((user => {
      this.initFields();
    }));
    this.companyChanged = this.cus.companyChanged.subscribe((user => {
      this.initFields();
    }));
    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.userChanged) { this.userChanged.unsubscribe(); }
    if (this.companyChanged) { this.companyChanged.unsubscribe(); }
  }


  ngOnChanges(changes: SimpleChanges): void {
    if (changes['panelIn']) {
      this.panelIn = changes['panelIn'].currentValue;
    }
    if (changes['paneBtnHide']) {
      this.paneBtnHide = changes['paneBtnHide'].currentValue;
      this.initFields();
    }
  }

  initFields() {
    this.isLoggedIn = this.cus.isLogged;
    this.user = this.cus.user;
    //console.log('initFields -> user:', this.user);
    this.company = this.cus.company;
    this.paneBtnVar = this.paneBtnHide ? Animate.hide : Animate.show;
    this.paneBtnVarOp = this.paneBtnHide ? Animate.show : Animate.hide;
  }

  snavToggleClick() {
    if (!this.paneBtnHide) {
      this.snavToggle.emit();
    }

  }

  toggleSpinner() {
    this.spinnerService.display(true);
    setTimeout(() => {
      this.spinnerService.setMessage('1000');
    }, 1000);
    setTimeout(() => {
      this.spinnerService.display(false);
    }, 2000);

  }

  showToastr() {
    this.toastr.success('Hello world!', 'Toastr fun!');
  }

  //#region Action Methods

  gotoUserProfile(user: UserModel) {
    if (user) {
      localStorage.setItem(AppStore.psm_company_profile, JSON.stringify(user));
      this.router.navigate([AppRoutes.Root, AppRoutes.company, AppRoutes.profile]);
    }
  }

  loginDialogShow(): void {

    const dialogRef = this.dialogService.open(LoginModal, {
      width: '500px',
      height: '340px',
      data: { email: null, password: null }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
      // this.password = result.password;
    });

  }

  logout() {
    this.cus.logout().subscribe(res => {
      //console.log('logout -> res', res);
      // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      // this.router.onSameUrlNavigation = 'reload';
      this.router.navigate([AppRoutes.Root]);
      //this.router.navigate[AppRoutes.Root, AppRoutes.public];
    });

  }

  //#endregion
  
}
