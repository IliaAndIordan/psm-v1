import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { SxApiAuthClient } from 'src/app/@core/clients/api/auth/api.client';
import { TokenService } from 'src/app/@core/clients/token.service';
import { LoginModal } from '../../modals/login/login.dialog';
import { SxSpinnerService } from '../../sx-spinner/sx-spinner.service';
import { AppService } from 'src/app/@core/services/app.service';
import { SxToolbarMainComponent } from './sx-toolbar-main.component';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { SxProjectButtonComponent } from '../../buttons/sx-project-button.component';
import { SxSpinnerComponent } from '../../sx-spinner/sx-spinner.component';
import { SxCommonModalsModule } from '../../modals/modals.module';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonModule } from '@angular/common';



describe('SxToolbarMainComponent', () => {
  let component: SxToolbarMainComponent;
  let fixture: ComponentFixture<SxToolbarMainComponent>;
  let spinnerService: SxSpinnerService;
  let toastr: ToastrService;
  let fb: FormBuilder;
  let login: LoginModal;
  let loginFixture: ComponentFixture<LoginModal>;
  let httpClientSpy: { get: jasmine.Spy };
  let http: HttpClient;
  let authService: SxApiAuthClient;
 

  beforeEach(async () => {
    return await TestBed.configureTestingModule({
      imports: [
        CommonModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        JwtModule.forRoot({
          config: {
            tokenGetter: () => {
              return '';
            }
          }
        }),
        MatDialogModule,
        SxCommonModalsModule,
      ],
      declarations: [
        SxToolbarMainComponent,
        SxProjectButtonComponent,
        SxSpinnerComponent,
      ],
      providers: [
        //FormBuilder,
        TokenService,
        { provide: HttpClient, useValue: {} },
        CurrentUserService,
        SxApiAuthClient,
        AppService,
        SxSpinnerService,
        /*{ provide: Router, useValue: {} },
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        
        */
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    toastr = TestBed.inject(ToastrService);
    spinnerService = TestBed.inject(SxSpinnerService);
    
    fb = TestBed.inject(FormBuilder);
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    authService = TestBed.inject(SxApiAuthClient);
    //http = TestBed.inject(HttpClient);

    //loginFixture = TestBed.createComponent(LoginModal);
    //login = loginFixture.componentInstance;

    fixture = TestBed.createComponent(SxToolbarMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
   
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should call toggleSpinner', () => {
    component.showToastr();
    component.toggleSpinner();
    spinnerService.status.subscribe(res => {
      expect(res).toBe(true);
    });
  });
  
  it('Should open the LoginModal in a MatDialog' , () => {
    spyOn(component.dialogService,'open').and.callThrough();
    component.loginDialogShow();
    expect(component.dialogService.open).toHaveBeenCalledWith(LoginModal, {
      width: '500px',
      height: '340px',
      data: { email: null, password: null }
    });
  });

  it('LoginModal', () => {
    const matDialog = fixture.debugElement.injector.get(MatDialog);
    spyOn(matDialog, 'open').and.callThrough();
    component.loginDialogShow();
    expect(matDialog.open).toHaveBeenCalledWith(LoginModal, {
      width: '500px',
      height: '340px',
      data: { email: null, password: null }
    });
  });

});
