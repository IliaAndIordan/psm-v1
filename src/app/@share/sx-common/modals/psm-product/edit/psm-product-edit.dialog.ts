import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// Services
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { ApiPsmProductClient } from 'src/app/@core/clients/api/psm-product/api-client';
// Models and Components
import { CompanyModel } from 'src/app/@core/clients/api/company/dto';
import { CountryModel } from 'src/app/@core/clients/api/country/dto';
import { CompanyTypeOpt } from 'src/app/@core/clients/api/company/enums';
import { URL_NO_IMG_SQ } from 'src/app/@core/constants/app-storage.const';
import { PsmProduct } from 'src/app/@core/clients/api/psm-product/dto';
import { UserModel } from 'src/app/@core/clients/api/auth/dto';



export interface IPsmProductEdit {
    product: PsmProduct;
}

@Component({
    templateUrl: './psm-product-edit.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class PsmProductEditDialog implements OnInit {

    noImgUrl = URL_NO_IMG_SQ;

    formGrp: FormGroup;

    product: PsmProduct;
    company: CompanyModel;
    user:UserModel;
    selCountry: CountryModel;
    countryList: Array<CountryModel>;
    countryOpt: Observable<CountryModel[]>;
    companyTypeOpt = CompanyTypeOpt;

    hasSpinner = false;
    errorMessage: string;
    flagUrl: string = URL_NO_IMG_SQ;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private cus: CurrentUserService,
        private client: ApiPsmProductClient,
        public dialogRef: MatDialogRef<IPsmProductEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IPsmProductEdit) {
       
        this.product = data.product;
    }

    get psmProductKey() { return this.formGrp.get('psmProductKey'); }
    get name() { return this.formGrp.get('name'); }
    
    get logoUrl() { return this.formGrp.get('logoUrl'); }
    get webUrl() { return this.formGrp.get('webUrl'); }
    get notes() { return this.formGrp.get('notes'); }
    

    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.user = this.cus.user;
        this.company =  this.cus.company;
        this.formGrp = this.fb.group({
            psmProductKey: new FormControl(this.product ? this.product.psmProductKey : '', [Validators.required, Validators.maxLength(50)]),
            name: new FormControl(this.product ? this.product.name : '', [Validators.required, Validators.maxLength(256)]),
            logoUrl: new FormControl(this.product ? this.product.logoUrl : '', [Validators.maxLength(1024)]),
            webUrl: new FormControl(this.product ? this.product.webUrl : '', [Validators.maxLength(1024)]),
            notes: new FormControl(this.product ? this.product.notes : '', [Validators.maxLength(2000)]),
        });
    }


    onCansel(): void {
        this.dialogRef.close(false);
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.product) {
                this.product = new PsmProduct();
            }
            const message = this.product.psmProductId > 0 ? 'Update' : 'Create';
            
            this.product.name = this.name.value;
            this.product.psmProductKey = this.psmProductKey.value;

            this.product.companyId = this.company? this.company.companyId:this.cus.company.companyId;
            this.product.logoUrl = this.logoUrl.value;
            this.product.webUrl = this.webUrl.value;
            this.product.notes = this.notes.value;

            this.product.actorId = this.cus.user.userId;

            this.client.psmProductSave(this.product)
                .subscribe((res: PsmProduct) => {
                    //console.log('companySave -> res:', res);
                    if (res && res.psmProductId) {
                        this.product = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Operation Succesfull: Product ' + message, 'Product');
                        this.dialogRef.close(this.company);
                    }
                },
                    err => {
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Product ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(false);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

    

}
