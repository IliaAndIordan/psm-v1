import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { PsmCoreModule } from "src/app/@core/core.module";
import { SxMaterialModule } from "../../material.module";
import { SxSharePipeModule } from "../../pipes/pipes.module";
import { InfoMessageDialog } from "./info-message/info-message.dialog";
import { LoginModal } from "./login/login.dialog";
import { UserRegisterModal } from "./user/register/user-register.dialog";
import { PsmCompanyEditDialog } from "./company-edit/company-edit.dialog";
import { PsmProductEditDialog } from "./psm-product/edit/psm-product-edit.dialog";
import { PsmUserEditDialog } from "./user/edit/psm-user-edit.dialog";
import { PsmModuleEditDialog } from "./psm-module/edit/psm-module-edit.dialog";
import { PsmSidePanelModalModule } from "../side-panel-modal/psm-side-panel-modal.module";
import { PsmCommonButtonsModule } from "../buttons/sx-buttons.module";
import { PsmCommonCardsModule } from "../cards/psm-cards.module";
import { PsmProjectEditDialog } from "./pms-project/edit/psm-project-edit.dialog";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,

    PsmCoreModule,
    SxMaterialModule,
    PsmSidePanelModalModule,
    SxSharePipeModule,
    
    PsmCommonButtonsModule,
    PsmCommonCardsModule,
    
  ],
  declarations: [
    LoginModal,
    UserRegisterModal,
    PsmUserEditDialog,
    InfoMessageDialog,

    PsmCompanyEditDialog,
    // --- PSM Products
    PsmProductEditDialog,
    PsmModuleEditDialog,
    PsmProjectEditDialog,
  ],
  exports: [
    LoginModal,
    UserRegisterModal,
    PsmUserEditDialog,
    InfoMessageDialog,

    PsmCompanyEditDialog,
    // --- PSM Products
    PsmProductEditDialog,
    PsmModuleEditDialog,
    PsmProjectEditDialog,
  ],
  providers: [
  ],
  entryComponents: [
    LoginModal,
    UserRegisterModal,
    PsmUserEditDialog,
    InfoMessageDialog,

    PsmCompanyEditDialog,
    // --- PSM Products
    PsmProductEditDialog,
    PsmModuleEditDialog,
  ]
})
export class PsmCommonModalsModule { }