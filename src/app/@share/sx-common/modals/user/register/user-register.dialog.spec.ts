import { HttpClient } from '@angular/common/http';
import { async, ComponentFixture, fakeAsync, flush, inject, TestBed, tick } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { SxApiAuthClient } from 'src/app/@core/clients/api/auth/api.client';
import { TokenService } from 'src/app/@core/clients/token.service';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, DebugElement, Directive, NgModule, ViewChild, ViewContainerRef } from '@angular/core';
import { SxMaterialModule } from 'src/app/@share/material.module';
import { By } from '@angular/platform-browser';
import { Subject } from 'rxjs';
import { ResponseAuthenticate, UserModel } from 'src/app/@core/clients/api/auth/dto';
import { UserRegisterModal } from './user-register.dialog';
import { PsmCommonModalsModule } from '../../modals.module';
import { SxSpinnerComponent } from '../../../sx-spinner/sx-spinner.component';


describe('UserRegisterModal', () => {

  const userStr = {
  company_id: 25,
  eMail: "iziordanov@gmail.com",
  e_mail: "iziordanov@gmail.com",
  ipAddress: "95.43.220.58",
  isReceiveEMails: 1,
  lastLogged: {date: "2021-08-31 23:38:55.000000", timezone_type: 3, timezone: "Europe/Helsinki"},
  password: "password",
  role: 1,
  updated: 0,
  userId: 1,
  userName: "Iordan G Mail",
};
  

  const config = {
    width: '500px',
    height: '340px',
    data: { email: 'i_z_iordanov@gmail.com', password: 'password' }
  };
  let user = Object.assign(new UserModel(), userStr);
  let dialog: MatDialog;
  let overlayContainer: OverlayContainer;
  let overlayContainerElement: HTMLElement;


  let component: UserRegisterModal;
  let fixture: ComponentFixture<UserRegisterModal>;
  let componentRef: ViewContainerRef;
  let dref:MatDialogRef<UserRegisterModal>;
  let testViewContainerRef: ViewContainerRef;
  let viewContainerFixture: ComponentFixture<ComponentWithChildViewContainer>;


  let fb: FormBuilder;
  let router: Router;
  let authClient: SxApiAuthClient;
  let toastr: ToastrService;
  let cus: CurrentUserService;
  let http: HttpClient;
  let matDialog: MatDialog;

  beforeEach(async () => {
    return await TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        JwtModule.forRoot({
          config: {
            tokenGetter: () => {
              return '';
            }
          }
        }),
        PsmCommonModalsModule,
        MatDialogModule,
        DialogTestModule
      ],
      declarations: [
        UserRegisterModal, 
        SxSpinnerComponent,
      ],
      providers: [
        TokenService,
        { provide: HttpClient, useValue: {} },
        { provide: MatDialogRef, useValue: {} },
        {provide: MAT_DIALOG_DATA, useValue: config},
        {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: config},
        CurrentUserService,
        SxApiAuthClient,
        /*
        {
          provide: OverlayContainer, useFactory: () => {
            overlayContainerElement = document.createElement('div');
            return { getContainerElement: () => overlayContainerElement };
          }
        },*/
      ]
    })
      .compileComponents();
  });

  beforeEach(inject([MatDialog, OverlayContainer],
    (d: MatDialog, oc: OverlayContainer) => {
      dialog = d;
      overlayContainer = oc;
      overlayContainerElement = oc.getContainerElement();
    }));

  afterEach(() => {
    overlayContainer.ngOnDestroy();
  });

  beforeEach(() => {
    fb = TestBed.inject(FormBuilder);
    toastr = TestBed.inject(ToastrService);
    authClient = TestBed.inject(SxApiAuthClient);
    router = TestBed.inject(Router);
    cus = TestBed.inject(CurrentUserService);
    cus.user = user;
    http = TestBed.inject(HttpClient);
    //http = jasmine.createSpyObj('HttpClient', ['get']);
    viewContainerFixture = TestBed.createComponent(ComponentWithChildViewContainer);

    viewContainerFixture.detectChanges();
    testViewContainerRef = viewContainerFixture.componentInstance.childViewContainer;
    
    //fixture = TestBed.createComponent(LoginModal);
    dref = dialog.open(UserRegisterModal, config);
    fixture = TestBed.createComponent(UserRegisterModal);
    component = dref.componentInstance;

  });

  it('should create', () => {
    //onsole.log('component -> ', component);
    expect(component).toBeTruthy();
  });

  it('should call auth login', fakeAsync(() => {
    let loginElement: DebugElement;
    fixture.detectChanges();
    //const loginSpy = spyOn(authClient , 'autenticate').and.callThrough();
    const loginSpy = spyOn(authClient , 'autenticate').and.returnValue( new Subject<ResponseAuthenticate>().asObservable());
    const curSpy = spyOnProperty(cus, 'user', 'get').and.returnValue(user);
    const postSpy = spyOn(HttpClient.prototype, 'post').and.callThrough();
    //const postSpy = jasmine.createSpyObj(HttpClient.prototype, ['post']);
    loginElement = fixture.debugElement.query(By.css('form'));
    // to set values
    expect(component.formGrp.valid).toBe(true);
    component.sx_email.setValue('user');
    expect(component.formGrp.valid).toBe(false);
    component.sx_email.setValue('user@emaol.com');
    component.sx_password.setValue('123');
    expect(component.formGrp.valid).toBe(false);
    component.sx_email.setValue('user@emaol.com');
    component.sx_password.setValue('123456');
    expect(component.formGrp.valid).toBe(true);
    component.onLoginClick();
    //loginElement.triggerEventHandler('LoginClick', null);
   
    expect(loginSpy).toHaveBeenCalledTimes(1); 
    //expect(curSpy).toHaveBeenCalledTimes(1);
   }));

   it('should call fError', fakeAsync(() => {
    let loginElement: DebugElement;
    fixture.detectChanges();
    
    loginElement = fixture.debugElement.query(By.css('form'));
    expect(component.formGrp.valid).toBe(true);
    component.sx_email.setValue('');
    expect(component.formGrp.valid).toBe(false);
   }));
   
   it('should call close', fakeAsync(() => {
    let loginElement: DebugElement;
    fixture.detectChanges();

    let afterCloseCallback = jasmine.createSpy('afterClose callback');
    let dialogRef = component.dialogRef;
    const postSpy = spyOn(dialogRef, 'close').and.callThrough();
    loginElement = fixture.debugElement.query(By.css('form'));
    expect(component.formGrp.valid).toBe(true);
    
    dialogRef.afterClosed().subscribe(afterCloseCallback);
    //loginElement.triggerEventHandler('ngClose', null);
    const na = overlayContainerElement.querySelector('mat-dialog-container');
    
    component.close();
    fixture.detectChanges();
    flush();
    expect(postSpy).toHaveBeenCalled();
    //expect(overlayContainerElement.querySelector('mat-dialog-container')).toBeNull();
   }));
  /*
  it('should close a dialog and get back a result', fakeAsync(() => {
    let dialogRef = dialog.open(LoginModal, { viewContainerRef: testViewContainerRef });
    let afterCloseCallback = jasmine.createSpy('afterClose callback');

    dialogRef.afterClosed().subscribe(afterCloseCallback);
    dialogRef.close();
    viewContainerFixture.detectChanges();
    flush();

    expect(afterCloseCallback).toHaveBeenCalled();
    expect(overlayContainerElement.querySelector('mat-dialog-container')).toBeNull();
  }));

  it('should close a dialog and get back a result', fakeAsync(() => {
    let dialogRef = dialog.open(LoginModal, config);
    let afterCloseCallback = jasmine.createSpy('afterClose callback');

    dialogRef.afterClosed().subscribe(afterCloseCallback);
    component.onLoginClick();
    fixture.detectChanges();
    flush();

    expect(afterCloseCallback).toHaveBeenCalled();
    expect(overlayContainerElement.querySelector('mat-dialog-container')).toBeNull();
  }));
  */
/*
  beforeEach(() => {
    fb = TestBed.inject(FormBuilder);
    toastr = TestBed.inject(ToastrService);
    authClient = TestBed.inject(SxApiAuthClient);
    router = TestBed.inject(Router);
    cus = TestBed.inject(CurrentUserService);
    http = jasmine.createSpyObj('HttpClient', ['get']);

    //http = TestBed.inject(HttpClient);

    //loginFixture = TestBed.createComponent(LoginModal);
    //login = loginFixture.componentInstance;
   
    //overlayContainerElement = fixture.debugElement.injector.get(OverlayContainer);
    
    noopFixture = TestBed.createComponent(NoopComponent);
    noop = noopFixture.componentInstance;
    fixture = TestBed.createComponent(LoginModal);
    component = fixture.componentInstance;
    matDialog = TestBed.inject(MatDialog); 
    matDialog = TestBed.get(MatDialog);
    //matDialog = fixture.debugElement.injector.get(MatDialog);
    matDialog.open(LoginModal , config);
    //overlayContainerElement = fixture.debugElement.injector.get(OverlayContainer) as HTMLElement;
    //matDialog = fixture.debugElement.injector.get(MatDialog);
    //fixture.detectChanges();

  });
*/
/*

*/
/*
it('should open a dialog with a component', () => {
  let dialogRef = dialog.open(LoginModal, config);

  viewContainerFixture.detectChanges();
  console.log('overlayContainerElement -> ', overlayContainerElement);
  expect(overlayContainerElement.textContent).toContain('Login');
  expect(dialogRef.componentInstance instanceof LoginModal).toBe(true);
  expect(dialogRef.componentInstance.dialogRef).toBe(dialogRef);

  viewContainerFixture.detectChanges();
  let dialogContainerElement = overlayContainerElement.querySelector('mat-dialog-container')!;
  expect(dialogContainerElement.getAttribute('role')).toBe('dialog');
});
*/
  
  
});

@Directive({selector: 'dir-with-view-container'})
class DirectiveWithViewContainer {
  constructor(public viewContainerRef: ViewContainerRef) { }
}

@Component({
  selector: 'arbitrary-component',
  template: `<dir-with-view-container></dir-with-view-container>`,
})
class ComponentWithChildViewContainer {
  @ViewChild(DirectiveWithViewContainer) childWithViewContainer: DirectiveWithViewContainer;

  get childViewContainer() {
    return this.childWithViewContainer.viewContainerRef;
  }
}


const TEST_DIRECTIVES = [
  ComponentWithChildViewContainer,
  DirectiveWithViewContainer, 
  SxSpinnerComponent,
];

@NgModule({
  imports: [MatDialogModule, NoopAnimationsModule],
  exports: TEST_DIRECTIVES,
  declarations: TEST_DIRECTIVES,
  entryComponents: [
    UserRegisterModal, 
    ComponentWithChildViewContainer
  ],
})
class DialogTestModule { }