import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// Services
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { CompanyModel, RespCompanyListToJoin } from 'src/app/@core/clients/api/company/dto';
import { CountryModel } from 'src/app/@core/clients/api/country/dto';
import { CompanyTypeOpt } from 'src/app/@core/clients/api/company/enums';
import { SxApiCompanyClient } from 'src/app/@core/clients/api/company/api-client';
import { SxApiCountryClient } from 'src/app/@core/clients/api/country/api-client';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { URL_NO_IMG_SQ } from 'src/app/@core/constants/app-storage.const';
import { ResponseGetUser, UserModel } from 'src/app/@core/clients/api/auth/dto';
import { PsmUserRoleOpt } from 'src/app/@core/clients/api/auth/enums';
import { SxSpinnerService } from '../../../sx-spinner/sx-spinner.service';
import { PsmUserClient } from 'src/app/@core/clients/api/user/api-client';
// Models and Components



export interface IUserEdit {
    user: UserModel;
}

@Component({
    templateUrl: './psm-user-edit.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class PsmUserEditDialog implements OnInit {

    noImgUrl = URL_NO_IMG_SQ;

    formGrp: FormGroup;

    user: UserModel;

    selCompany: CompanyModel;
    companyList: Array<CompanyModel>;
    companyCount: number;
    companyOpt: Observable<CompanyModel[]>;
    roleOpt = PsmUserRoleOpt;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinnerService: SxSpinnerService,
        private cus: CurrentUserService,
        private comClient: SxApiCompanyClient,
        private userCliente: PsmUserClient,
        public dialogRef: MatDialogRef<IUserEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IUserEdit) {
        this.user = data.user;
    }

    get name() { return this.formGrp.get('name'); }
    get email() { return this.formGrp.get('email'); }
    get password() { return this.formGrp.get('password'); }
    get role() { return this.formGrp.get('role'); }
    get company() { return this.formGrp.get('company'); }

    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;

        this.formGrp = this.fb.group({
            name: new FormControl(this.user ? this.user.name : '', [Validators.required, Validators.maxLength(512)]),
            email: new FormControl(this.user ? this.user.email : '', [Validators.required, Validators.maxLength(256)]),
            password: new FormControl(this.user ? this.user.password : '', [Validators.required, Validators.minLength(5)]),
            role: new FormControl(this.user ? this.user.role : '', [Validators.required]),
            company: new FormControl( '', [Validators.required]),
        });

        this.loadCompanyList();
    }


    onCansel(): void {
        this.dialogRef.close(false);
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.company) {
                this.user = new UserModel();
            }
            const message = this.user.userId > 0 ? 'Update' : 'Create';
            const com: CompanyModel = this.company.value as CompanyModel;
            this.user.name = this.name.value;
            this.user.email = this.email.value;
            if (this.user.userId === this.cus.user.userId) {
                this.user.password = this.password.value;
            }

            this.user.companyId = com ? com.companyId : this.user.companyId;
            this.user.role = this.role.value;

            this.userCliente.userSave(this.user)
                .subscribe((res: ResponseGetUser) => {
                    console.log('userSave -> res:', res);
                    if (res && res.data && res.data.user) {
                        this.user = UserModel.fromJSON(res.data.user);
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('PSM User', 'Operation Succesfull: User ' + message);
                        this.dialogRef.close(this.company);
                    }
                },
                    err => {
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'User ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(false);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

    //#region  Country Filter

    companyValueChange(event: MatAutocompleteSelectedEvent) {
        if (this.company.valid) {
            this.selCompany = this.company.value;
        }
    }

    displayCompany(cm: CompanyModel): string {
        let rv: string = '';

        if (cm) {
            this.selCompany = cm;
            //rv = '<img class="flag-16 mr-2" [src]="' + cm.flagUrl + '" alt="flag">';
            rv = cm.name;
            rv += ' ' + cm.branch;
        }
        return rv;
    }



    filterContry(val: any): CompanyModel[] {
        // console.log('filterDpc -> val', val);
        if (val && val.id) {
            this.selCompany = val as CompanyModel;
        }

        // console.log('filterBranches -> this.selDpc', this.selDpc);
        const value = val.name || val; // val can be companyName or string
        const filterValue = value ? value.toLowerCase() : '';
        // console.log('_filter -> filterValue', filterValue);
        let rv = new Array<CompanyModel>()
        if (this.companyList && this.companyList.length) {
            rv = this.companyList.filter(x => (
                x.name.toLowerCase().indexOf(filterValue) > -1 ||
                x.branch.toLowerCase().indexOf(filterValue) > -1));
        }
        // console.log('_filter -> rv', rv);
        return rv;
    }

    initCompanyOpt() {
        this.companyOpt = of(this.companyList);
        this.companyOpt = this.company.valueChanges
            .pipe(
                startWith(null),
                map(state => state ? this.filterContry(state) :
                    (this.companyList ? this.companyList.slice() : new Array<CompanyModel>()))
            );

        if(this.user.companyId && this.companyList && this.companyList.length>0){
            this.selCompany = this.companyList.find(x=>x.companyId === this.user.companyId);
        }
        this.formGrp.patchValue({ 'company': this.selCompany });
        this.formGrp.updateValueAndValidity();
    }

    loadCompanyList() {
        this.spinnerService.show();
        this.comClient.ListToJoin()
            .subscribe((resp: RespCompanyListToJoin) => {
                
                this.companyList = new Array<CompanyModel>();
                this.spinnerService.hide();
                if (resp) {
                    this.companyCount = resp.data.countTotal;
                    if (resp.data &&
                        resp.data.companies &&
                        resp.data.companies.length > 0) {

                        resp.data.companies.forEach(el => {
                            const obj = CompanyModel.fromJSON(el);
                            this.companyList.push(obj);
                        });
                        //console.log('ListToJoin -> companies=', this.companyList);
                    }


                }

                if(this.user.companyId && this.companyList && this.companyList.length>0){
                    this.selCompany = this.companyList.find(x=>x.companyId === this.user.companyId);
                }

                this.initCompanyOpt();
            });
    }

    //#endregion

}
