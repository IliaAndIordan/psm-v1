import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// -Services
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { SxSpinnerService } from '../../sx-spinner/sx-spinner.service';
// -Models


export interface IInfoMessage {
    title: string;
    message: string;
}

@Component({
    templateUrl: './info-message.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class InfoMessageDialog implements OnInit {

    formGrp: FormGroup;

    title: string;
    message: string;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: FormBuilder,
        private toastr: ToastrService,
        private spinerService: SxSpinnerService,
        private cus: CurrentUserService,
        public dialogRef: MatDialogRef<IInfoMessage>,
        @Inject(MAT_DIALOG_DATA) public data: IInfoMessage) {
        this.title = data.title;
        this.message = data.message;
    }

    ngOnInit(): void {
        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.formGrp = this.fb.group({
           
        });
    }


    onCansel(): void {
        this.dialogRef.close(false);
    }

    onSubmit() {
        this.dialogRef.close(true);
    }

}
