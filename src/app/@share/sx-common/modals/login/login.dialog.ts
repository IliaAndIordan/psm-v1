import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SxApiAuthClient } from 'src/app/@core/clients/api/auth/api.client';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { ResponseAuthenticate } from 'src/app/@core/clients/api/auth/dto';
import { AppRoutes } from 'src/app/@core/constants/app-routes.const';


// Constants

export interface ILoginModal {
  email: string;
  password: string;
}

@Component({
  templateUrl: './login.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class LoginModal implements OnInit {

  formGrp: FormGroup;

  email: string;
  password: string;

  hasSpinner = false;
  errorMessage: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authClient: SxApiAuthClient,
    private toastr: ToastrService,
    private cus: CurrentUserService,
    public dialogRef: MatDialogRef<LoginModal>,
    @Inject(MAT_DIALOG_DATA) public data: ILoginModal) {
    this.email = data.email;
    this.password = data.password;
  }

  get sx_email() { return this.formGrp.get('sx_email') as FormControl; }
  get sx_password() { return this.formGrp.get('sx_password') as FormControl; }


  fError(fname: string): string {
      const field = this.formGrp.get(fname);
      return field.hasError('required') ? 'Field ' + fname + '  is required.' :
          field.hasError('minlength') ? 'Field ' + fname + ' to short must be et least 5 characters lon.' :
              field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
  }


  ngOnInit(): void {

    this.errorMessage = undefined;
    this.hasSpinner = false;

    this.formGrp = this.fb.group({
      sx_email: new FormControl(this.email, [Validators.required, Validators.email]),
      sx_password: new FormControl(this.password, [Validators.required, Validators.minLength(5)]),
    });
  }


  close(): void {
    this.dialogRef.close();
  }

  onLoginClick() {

    if (this.formGrp.valid) {
      this.errorMessage = undefined;
      this.hasSpinner = true;

      this.email = this.sx_email.value;
      this.password = this.sx_password.value;
     
      this.authClient.autenticate(this.email, this.password)
        .subscribe((resp: ResponseAuthenticate) => {
          const user = this.cus.user;
           //console.log('autenticate-> resp user', user);
          if (user) {
            // this.spinerService.display(false);
            this.hasSpinner = false;
            this.toastr.success('Welcome ' + this.cus.userName + '!', 'Login success');
            if(user.companyId){
              const redirectTo = this.cus.redirectTo;
              //console.log('redirectTo -> ', redirectTo);
              if (!redirectTo)
              {
                  //var redirectUrl = redirectTo + "&token=" + this.tokenService.bearerToken;
                  //this.authService.logout(redirectUrl);
                  this.router.navigate([AppRoutes.Root, AppRoutes.dashboard]);
              }
              else
              {
                  this.router.navigate([redirectTo], { replaceUrl: true });
              }
              
            }
            else{
              this.router.navigate([AppRoutes.Root, AppRoutes.public, AppRoutes.companyset]);
            }
            this.dialogRef.close(user);
            /*
            setTimeout((router: Router) => {
              this.router.navigate([AppRoutes.Root, AppRoutes.dashboard]);
              this.dialogRef.close(user);
            }, 1000);
            */
          } else {
            this.hasSpinner = false;
            this.toastr.error('Login Failed', 'Login Failed');
          }
        },
          err => {
            console.error('autenticate-> got an error: ' + err);
            this.errorMessage = 'Please enter valid values for fields';
            this.hasSpinner = false;
          },
          () => {
            //console.log('autenticate-> got a complete notification');
          });
    } else {
      this.errorMessage = 'Please enter valid values for fields';
      this.hasSpinner = false;
    }
  }


}
