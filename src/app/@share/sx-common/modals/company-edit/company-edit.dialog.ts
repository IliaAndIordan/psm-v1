import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// Services
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { CompanyModel } from 'src/app/@core/clients/api/company/dto';
import { CountryModel } from 'src/app/@core/clients/api/country/dto';
import { CompanyTypeOpt } from 'src/app/@core/clients/api/company/enums';
import { SxApiCompanyClient } from 'src/app/@core/clients/api/company/api-client';
import { SxApiCountryClient } from 'src/app/@core/clients/api/country/api-client';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { URL_NO_IMG_SQ } from 'src/app/@core/constants/app-storage.const';
// Models and Components



export interface ICompanyEdit {
    company: CompanyModel;
}

@Component({
    templateUrl: './company-edit.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class PsmCompanyEditDialog implements OnInit {

    noImgUrl = URL_NO_IMG_SQ;

    formGrp: FormGroup;

    company: CompanyModel;
    selCountry: CountryModel;
    countryList: Array<CountryModel>;
    countryOpt: Observable<CountryModel[]>;
    companyTypeOpt = CompanyTypeOpt;

    hasSpinner = false;
    errorMessage: string;
    flagUrl: string = URL_NO_IMG_SQ;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private cus: CurrentUserService,
        private comClient: SxApiCompanyClient,
        private cCliente: SxApiCountryClient,
        public dialogRef: MatDialogRef<ICompanyEdit>,
        @Inject(MAT_DIALOG_DATA) public data: ICompanyEdit) {
        this.company = data.company;
    }

    get company_name() { return this.formGrp.get('company_name'); }
    get branch_code() { return this.formGrp.get('branch_code'); }
    get company_type_id() { return this.formGrp.get('company_type_id'); }
    get logo_url() { return this.formGrp.get('logo_url'); }
    get web_url() { return this.formGrp.get('web_url'); }
    get notes() { return this.formGrp.get('notes'); }
    get country() { return this.formGrp.get('country'); }

    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;

        this.formGrp = this.fb.group({
            company_name: new FormControl(this.company ? this.company.name : '', [Validators.required, Validators.maxLength(512)]),
            branch_code: new FormControl(this.company ? this.company.branch : '', [Validators.maxLength(256)]),
            company_type_id: new FormControl(this.company ? this.company.typeId : '', [Validators.required]),
            country: new FormControl('', [Validators.required]),
            logo_url: new FormControl(this.company ? this.company.logoUrl : '', []),
            web_url: new FormControl(this.company ? this.company.webUrl : '', []),
            notes: new FormControl(this.company ? this.company.notes : '', []),

        });

        this.loadCountryList();
    }


    onCansel(): void {
        this.dialogRef.close(false);
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.company) {
                this.company = new CompanyModel();
            }
            const message = this.company.companyId > 0 ? 'Update' : 'Create';
            const country: CountryModel = this.country.value as CountryModel;
            this.company.name = this.company_name.value;
            this.company.branch = this.branch_code.value;

            this.company.typeId = this.company_type_id.value;
            this.company.countryId = country.id;
            this.company.logoUrl = this.logo_url.value;
            this.company.webUrl = this.web_url.value;
            this.company.notes = this.notes.value;

            this.company.actorId = this.cus.user.userId;

            this.comClient.companySave(this.company)
                .subscribe((res: CompanyModel) => {
                    //console.log('companySave -> res:', res);
                    if (res && res.companyId) {
                        this.company = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Compnay', 'Operation Succesfull: Compnay ' + message);
                        this.dialogRef.close(this.company);
                    }
                },
                    err => {
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Compnay ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(false);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

    //#region  Country Filter

    countryValueChange(event: MatAutocompleteSelectedEvent) {
        if (this.country.valid) {
            this.selCountry = this.country.value;
            this.flagUrl = this.selCountry ? this.selCountry.flagUrl : this.noImgUrl;
        }
    }

    displayCountry(cm: CountryModel): string {
        let rv: string = '';

        if (cm) {
            this.selCountry = cm;
            //rv = '<img class="flag-16 mr-2" [src]="' + cm.flagUrl + '" alt="flag">';
            rv = cm.name;
            rv += ' ' + cm.iso2 + ' - ' + cm.region;
        }
        this.flagUrl = this.selCountry ? this.selCountry.flagUrl : this.noImgUrl;
        return rv;
    }

    initContryOpt() {
        this.countryOpt = of(this.countryList);
        this.countryOpt = this.country.valueChanges
            .pipe(
                startWith(null),
                map(state => state ? this.filterContry(state) :
                    (this.countryList ? this.countryList.slice() : new Array<CountryModel>()))
            );
        this.formGrp.patchValue({ 'country': this.selCountry });
        this.flagUrl = this.selCountry ? this.selCountry.flagUrl : this.noImgUrl;
        this.formGrp.updateValueAndValidity();
    }

    filterContry(val: any): CountryModel[] {
        // console.log('filterDpc -> val', val);
        if (val && val.id) {
            this.selCountry = val as CountryModel;
        }

        // console.log('filterBranches -> this.selDpc', this.selDpc);
        const value = val.name || val; // val can be companyName or string
        const filterValue = value ? value.toLowerCase() : '';
        // console.log('_filter -> filterValue', filterValue);
        let rv = new Array<CountryModel>()
        if (this.countryList && this.countryList.length) {
            rv = this.countryList.filter(x => (
                x.name.toLowerCase().indexOf(filterValue) > -1 ||
                x.iso2.toLowerCase().indexOf(filterValue) > -1 ||
                x.region.toLowerCase().indexOf(filterValue) > -1));
        }
        // console.log('_filter -> rv', rv);
        return rv;
    }


    loadCountryList() {
        this.cCliente.countryList
            .subscribe((resp: Array<CountryModel>) => {

                this.countryList = resp;
                if (this.countryList && this.countryList.length > 0) {
                    this.selCountry = this.company ? this.countryList.find(x => x.id === this.company.countryId) : undefined;
                }
                this.flagUrl = this.selCountry ? this.selCountry.flagUrl : this.noImgUrl;
                this.initContryOpt();
            });
    }

    //#endregion

}
