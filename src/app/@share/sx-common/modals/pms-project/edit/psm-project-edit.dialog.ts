import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// Services
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { ApiPsmProductClient } from 'src/app/@core/clients/api/psm-product/api-client';
// Models and Components
import { CompanyModel } from 'src/app/@core/clients/api/company/dto';
import { CountryModel } from 'src/app/@core/clients/api/country/dto';
import { CompanyTypeOpt } from 'src/app/@core/clients/api/company/enums';
import { URL_NO_IMG_SQ } from 'src/app/@core/constants/app-storage.const';
import { PsmModule, PsmProduct, PsmProductTableCriteria, ResponsePsmProductTable } from 'src/app/@core/clients/api/psm-product/dto';
import { UserModel } from 'src/app/@core/clients/api/auth/dto';
import { PsmModuleTypeOpt } from 'src/app/@core/clients/api/psm-product/enums';
import { PsmProject } from 'src/app/@core/clients/api/project/dto';
import { SxProjectStatus, SxProjectStatusOpt } from 'src/app/@core/clients/api/project/enums';
import { ApiPsmProjectClient } from 'src/app/@core/clients/api/project/api-client';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { map, startWith } from 'rxjs/operators';




export interface IPsmProjectEdit {
    project: PsmProject;
    company: CompanyModel;
}

@Component({
    templateUrl: './psm-project-edit.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class PsmProjectEditDialog implements OnInit {

    noImgUrl = URL_NO_IMG_SQ;
    formGrp: FormGroup;

    project: PsmProject;
    company: CompanyModel;

    user: UserModel;

    selProduct: PsmProduct;
    productList: Array<PsmProduct>;
    productOpt: Observable<PsmProduct[]>;

    
    
    userList: Array<UserModel>;

    selManager: UserModel;
    managerOpt: Observable<UserModel[]>;
    selEstimator: UserModel;
    estimatorOpt: Observable<UserModel[]>;

    statusOpt = SxProjectStatusOpt;

    hasSpinner = false;
    errorMessage: string;
    logoUrl: string = URL_NO_IMG_SQ;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private cus: CurrentUserService,
        private client: ApiPsmProjectClient,
        private prodClient: ApiPsmProductClient,
        public dialogRef: MatDialogRef<IPsmProjectEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IPsmProjectEdit) {

        this.project = data.project;
        this.company = data.company;
    }

    get name() { return this.formGrp.get('name'); }
    get status() { return this.formGrp.get('status'); }
    get product() { return this.formGrp.get('product'); }
    get key() { return this.formGrp.get('key'); }
    get manager() { return this.formGrp.get('manager'); }
    get estimator() { return this.formGrp.get('estimator'); }
    get notes() { return this.formGrp.get('notes'); }
    get dayStart() { return this.formGrp.get('dayStart'); }
    get dayEnd() { return this.formGrp.get('dayEnd'); }

    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.user = this.cus.user;
        this.company = this.cus.company;
        this.formGrp = this.fb.group({
            name: new FormControl(this.project ? this.project.name : '', [Validators.required, Validators.maxLength(512)]),
            status: new FormControl(this.project ? this.project.statusId : SxProjectStatus.Backlog, [Validators.required]),
            product: new FormControl('', [Validators.required]),
            key: new FormControl(this.project ? this.project.accountKey : '', [Validators.required, Validators.maxLength(256)]),
            manager: new FormControl('', []),
            estimator: new FormControl('', []),
            notes: new FormControl(this.project ? this.project.notes : '', [Validators.maxLength(2000)]),
            dayStart: new FormControl(this.project ? this.project.dayStart : '', []),
            dayEnd: new FormControl(this.project ? this.project.dayEnd : '', []),
        });

        this.loadProductList();
    }


    onCansel(): void {
        this.dialogRef.close(false);
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.project) {
                this.project = new PsmProject();
            }
            const message = this.project.projectId > 0 ? 'Update' : 'Create';

            this.project.name = this.name.value;
            this.project.accountKey = this.key.value;
            this.project.notes = this.notes.value;

            const product: PsmProduct = this.product.value as PsmProduct;
            this.project.productId = product ? product.psmProductId : undefined;
            const selManager: UserModel = this.manager.value as UserModel;
            this.project.managerId = selManager ? selManager.userId : undefined;
            const selEstimator: UserModel = this.estimator.value as UserModel;
            this.project.estimatorId = selEstimator ? selEstimator.userId : undefined;


            this.client.psmProjectSave(this.project)
                .subscribe((res: PsmProject) => {
                    //console.log('companySave -> res:', res);
                    if (res && res.projectId) {
                        this.project = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Operation Succesfull: Project ' + message, '.');
                        this.dialogRef.close(this.project);
                    }
                },
                    err => {
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Project ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(false);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }


    //#region  Product Filter

    productValueChange(event: MatAutocompleteSelectedEvent) {
        if (this.product.valid) {
            this.selProduct = this.product.value as PsmProduct;
            this.logoUrl = this.selProduct ? this.selProduct.logoUrl : this.noImgUrl;
        }
    }

    displayProduct(val: PsmProduct): string {
        let rv: string = '';

        if (val && val.psmProductId) {
            this.selProduct = val;
            //rv = '<img class="flag-16 mr-2" [src]="' + cm.flagUrl + '" alt="flag">';
            rv = val.name;
            rv += ' - ' + val.psmProductKey;
        }
        this.logoUrl = this.selProduct ? this.selProduct.logoUrl : this.noImgUrl;
        return rv;
    }

    initProductOpt() {
        this.productOpt = of(this.productList);
        this.productOpt = this.product.valueChanges
            .pipe(
                startWith(null),
                map(state => state ? this.filterProduct(state) :
                    (this.productList ? this.productList.slice() : new Array<PsmProduct>()))
            );
        this.formGrp.patchValue({ 'product': this.selProduct });
        this.logoUrl = this.selProduct ? this.selProduct.logoUrl : this.noImgUrl;
        this.formGrp.updateValueAndValidity();
    }

    filterProduct(val: any): PsmProduct[] {
        // console.log('filterDpc -> val', val);
        if (val && val.psmProductId) {
            this.selProduct = val as PsmProduct;
        }

        // console.log('filterBranches -> this.selDpc', this.selDpc);
        const value = val.name || val; // val can be companyName or string
        const filterValue = value ? value.toLowerCase() : '';
        // console.log('_filter -> filterValue', filterValue);
        let rv = new Array<PsmProduct>()
        if (this.productList && this.productList.length) {
            rv = this.productList.filter(x => (
                x.name.toLowerCase().indexOf(filterValue) > -1 ||
                x.psmProductKey.toLowerCase().indexOf(filterValue) > -1 ||
                x.name.toLowerCase().indexOf(filterValue) > -1));
        }
        // console.log('_filter -> rv', rv);
        return rv;
    }


    loadProductList() {
        const criteris = new PsmProductTableCriteria();
        criteris.companyId = this.company.companyId;
        criteris.pageIndex = 0;
        criteris.limit = 10000;
        this.prodClient.psmProductTable(criteris)
            .subscribe((resp: ResponsePsmProductTable) => {
                //const resp = Object.assign(new ResponseAmsUserTableData(), res);
                //console.log('loadData-> resp=', resp);
                this.productList = new Array<PsmProduct>();

                if (resp && resp.success) {
                    if (resp && resp.data &&
                        resp.data.psmProducts && resp.data.psmProducts.length > 0) {
                        resp.data.psmProducts.forEach(iu => {
                            const obj = PsmProduct.fromJSON(iu);
                            this.productList.push(obj);
                        });
                    }

                    if (this.productList && this.productList.length > 0) {
                        this.selProduct = this.project && this.project.productId ? this.productList.find(x => x.psmProductId === this.project.productId) : undefined;
                    }
                    this.logoUrl = this.selProduct ? this.selProduct.logoUrl : this.noImgUrl;
                    this.initProductOpt();
                }

            });

    }

    //#endregion

}
