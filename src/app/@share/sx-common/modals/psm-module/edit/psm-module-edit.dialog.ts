import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// Services
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { ApiPsmProductClient } from 'src/app/@core/clients/api/psm-product/api-client';
// Models and Components
import { CompanyModel } from 'src/app/@core/clients/api/company/dto';
import { CountryModel } from 'src/app/@core/clients/api/country/dto';
import { CompanyTypeOpt } from 'src/app/@core/clients/api/company/enums';
import { URL_NO_IMG_SQ } from 'src/app/@core/constants/app-storage.const';
import { PsmModule, PsmProduct } from 'src/app/@core/clients/api/psm-product/dto';
import { UserModel } from 'src/app/@core/clients/api/auth/dto';
import { PsmModuleTypeOpt } from 'src/app/@core/clients/api/psm-product/enums';




export interface IPsmModuleEdit {
    module: PsmModule;
    product: PsmProduct;
}

@Component({
    templateUrl: './psm-module-edit.dialog.html',
})

// tslint:disable-next-line:component-class-suffix
export class PsmModuleEditDialog implements OnInit {

    noImgUrl = URL_NO_IMG_SQ;

    formGrp: FormGroup;

    product: PsmProduct;
    company: CompanyModel;
    module: PsmModule;
    user: UserModel;
    selCountry: CountryModel;
    countryList: Array<CountryModel>;
    countryOpt: Observable<CountryModel[]>;
    moduleTypeOpt = PsmModuleTypeOpt;

    hasSpinner = false;
    errorMessage: string;
    flagUrl: string = URL_NO_IMG_SQ;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private cus: CurrentUserService,
        private client: ApiPsmProductClient,
        public dialogRef: MatDialogRef<IPsmModuleEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IPsmModuleEdit) {

        this.product = data.product;
        this.module = data.module;
    }

    get name() { return this.formGrp.get('name'); }
    get moduleTypeId() { return this.formGrp.get('moduleTypeId'); }
    get notes() { return this.formGrp.get('notes'); }
    //get productC() { return this.formGrp.get('productc'); }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.user = this.cus.user;
        this.company = this.cus.company;
        this.formGrp = this.fb.group({
            name: new FormControl(this.module ? this.module.name : '', [Validators.required, Validators.maxLength(256)]),
            moduleTypeId: new FormControl(this.module ? this.module.moduleTypeId : '', [Validators.required]),
            notes: new FormControl(this.module ? this.module.notes : '', [Validators.maxLength(512)]),
           // productC: new FormControl('', [Validators.required]),

        });
    }


    onCansel(): void {
        this.dialogRef.close(false);
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.module) {
                this.module = new PsmModule();
            }
            const message = this.module.moduleId > 0 ? 'Update' : 'Create';

            this.module.name = this.name.value;
            this.module.moduleTypeId = this.moduleTypeId.value;

            this.module.psmProductId = this.product.psmProductId;
            this.module.notes = this.notes.value;

            this.client.psmModuleSave(this.module)
                .subscribe((res: PsmModule) => {
                    //console.log('companySave -> res:', res);
                    if (res && res.moduleId) {
                        this.module = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Operation Succesfull: Module ' + message, 'Module');
                        this.dialogRef.close(this.module);
                    }
                },
                    err => {
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Module ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(false);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }



}
