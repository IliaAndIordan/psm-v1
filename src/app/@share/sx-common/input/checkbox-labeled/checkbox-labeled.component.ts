import { Component, OnInit, EventEmitter, Input, Output, ViewChild } from '@angular/core';

@Component({
    selector: 'sx-checkbox-labeled',
    templateUrl: 'checkbox-labeled.component.html'
})

export class SxCheckboxLabeled implements OnInit {

    constructor() { }

    /**
     * BINDINGS
     */
    @Input() label: string;
    @Input() cssId: string;
    @Input() checked: boolean;
    @Input() disabled: boolean = false;
    @Output() onChecked: EventEmitter<boolean> = new EventEmitter<boolean>();

    /**
     * FIELDS
     */
    @ViewChild('input') input: { nativeElement: { checked: boolean; }; };

    ngOnInit() { }

    handleCheck(event: any) {
        //what we are sending to the event handler that is being passed to parent function.
        //const checked = event.target.checked;
        this.checked = event.target.checked;
        this.onChecked.emit(this.checked);
    }

    public uncheckBox() {
        this.checked = false;
        this.input.nativeElement.checked = false;
    }

    public checkBox() {
        this.checked = true;
        this.input.nativeElement.checked = true;
    }

    public setCheckbox(value:boolean):void{
        console.log('setCheckbox');
        this.checked = value;
    }


}
