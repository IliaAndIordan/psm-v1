import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxSpinnerComponent } from './sx-spinner/sx-spinner.component';
import { SxMaterialModule } from '../material.module';
import { SxToolbarMainComponent } from './toolbars/sx-toolbar-main/sx-toolbar-main.component';
import { PsmCoreModule } from 'src/app/@core/core.module';
import { PsmCommonModalsModule } from './modals/modals.module';
import { SxSharePipeModule } from '../pipes/pipes.module';
import { SxFilterPanelComponent } from './filter-panel/sx-filter-panel.component';
import { PsmCompanyLiveryComponent } from './images/livery/company-livery.component';
import { SxMenuComponent } from './menu/menu.component';
import { RouterModule } from '@angular/router';
import { SxCheckboxLabeled } from './input/checkbox-labeled/checkbox-labeled.component';
import { SxProductLineComponent } from './cards/sx-product/line/product-line.component';
import { PsmSidePanelModalModule } from './side-panel-modal/psm-side-panel-modal.module';
import { PsmCommonCardsModule } from './cards/psm-cards.module';
import { PsmCommonButtonsModule } from './buttons/sx-buttons.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SxMaterialModule,
    PsmCoreModule,

    PsmSidePanelModalModule,
    PsmCommonButtonsModule,
    PsmCommonCardsModule,
    PsmCommonModalsModule,
  ],
  declarations: [
    // Toolbars
    SxSpinnerComponent,
    SxMenuComponent,
    SxCheckboxLabeled,
    SxToolbarMainComponent,
    SxFilterPanelComponent,
    // Images
    PsmCompanyLiveryComponent,
    // ---SxProducts
    SxProductLineComponent,
  ],
  exports:[
    SxMaterialModule,
    PsmCoreModule,
    SxSharePipeModule,

    PsmCommonButtonsModule,
    PsmCommonCardsModule,
    PsmSidePanelModalModule,
    
    PsmCommonModalsModule,
    
    SxSpinnerComponent,
    SxMenuComponent,
    SxCheckboxLabeled,

    
    SxFilterPanelComponent,
    
    // Toolbars
    SxToolbarMainComponent,
    // Images
    PsmCompanyLiveryComponent,
    // ---SxProducts
    SxProductLineComponent,
  ],
 
  providers:[
  ]
})
export class PsmCommonModule { }
