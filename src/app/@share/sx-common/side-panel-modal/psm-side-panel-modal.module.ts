import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { PsmCoreModule } from "src/app/@core/core.module";
import { SxMaterialModule } from "../../material.module";
import { PsmCommonButtonsModule } from "../buttons/sx-buttons.module";
import { SxSidenavComponent } from "../snav/sx-sidenav/sx-sidenav.component";
import { SidePanelRowValueComponent } from "./row/side-panel-row-value.component";
import { AmsSidePanelModalComponent } from "./side-panel-modal";

@NgModule({
    imports: [
      CommonModule,
      RouterModule,
      SxMaterialModule,
      PsmCoreModule,
      PsmCommonButtonsModule,
    ],
    declarations: [
      //Side Panels
      SxSidenavComponent,
      AmsSidePanelModalComponent,
      SidePanelRowValueComponent,
      
    ],
    exports:[
      //Side Panels
      SxSidenavComponent,
      AmsSidePanelModalComponent,
      SidePanelRowValueComponent,
    ],
   
    providers:[
    ]
  })
  export class PsmSidePanelModalModule { }
  