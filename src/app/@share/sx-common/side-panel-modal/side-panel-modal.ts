import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Animate } from 'src/app/@core/constants/animation.const';

// Animations 
import { ExpandSidePanelTrigger, ShowHideTriggerBlock} from './animation';


@Component({
  selector: 'psm-side-panel-modal',
  templateUrl: './side-panel-modal.html',
  styles: [],
  animations: [ExpandSidePanelTrigger,
    ShowHideTriggerBlock]
})
export class AmsSidePanelModalComponent implements OnInit {

  constructor() { }


  /**
   * BINDINGS
   */
  @Output() closeSmpClick: EventEmitter<any> = new EventEmitter<any>();



  /**
   * FIELDS
   */
  expandPanelVar: string = Animate.hide;

  ngOnInit() { }

  public expandPanel() {
    this.expandPanelVar = Animate.show;
  }

  public closePanel() {
    this.expandPanelVar = Animate.hide;
  }

  handleClosePanelClick() {
    this.closeSmpClick.emit();
  }

}