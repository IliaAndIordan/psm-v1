import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { SxApiCountryClient } from 'src/app/@core/clients/api/country/api-client';
import { CountryModel } from 'src/app/@core/clients/api/country/dto';

@Component({
  selector: 'psm-side-panel-row-value',
  templateUrl: 'side-panel-row-value.component.html',
})
export class SidePanelRowValueComponent implements OnInit, OnChanges {



  @Input() label: string;
  @Input() infoMessage: string;
  @Input() value: any;
  @Input() isPrice: boolean = false;
  @Input() isDate: boolean = false;
  @Input() isContryId: boolean = false;
  @Input() showTime: boolean = false;
  @Input() size: string = 'large'; // accepts 'small', 'med' or large'
  @Input() valueCap: number = 30;

  isNumber: boolean;
  valueD: number;
  country: CountryModel;

  constructor(
    private countryCliente: SxApiCountryClient,) { }

  ngOnInit(): void {
    if (this.size === 'med') {
      this.valueCap = 70;
    }
    this.initFields();
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['value']) {
      this.value = changes['value'].currentValue;
      this.initFields();
    }
    if (changes['label']) {
      this.label = changes['label'].currentValue;
      this.initFields();
    }
  }

  initFields(): void {
    this.isNumber = this.checkNumber(this.value);
    this.valueD = undefined;

    if (this.isPrice) {
      this.valueD = parseFloat(this.value);
    }
    if (this.isContryId) {
      this.loadCountry();
    }

  }

  checkNumber(val: any): boolean { return typeof val === 'number'; }

  loadCountry() {
    this.countryCliente.countryList
      .subscribe((resp: Array<CountryModel>) => {
        if (resp && resp.length > 0) {

          this.country =resp.find(x => x.id == this.value);
        }
      });
  }

}
