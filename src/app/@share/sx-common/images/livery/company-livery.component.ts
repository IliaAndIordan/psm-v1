import { Component, OnInit, OnDestroy, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { URL_COMMON_IMAGES_COMPANY, URL_NO_IMG } from 'src/app/@core/constants/app-storage.const';


@Component({
    selector: 'psm-com-livery',
    templateUrl: './company-livery.component.html',
})

export class PsmCompanyLiveryComponent implements OnInit, OnDestroy, OnChanges {


    @Input() id: number;
    @Input() tooltip: string;
    @Input() imgclass: string = 'livery-table';
    @Output() liveryClick: EventEmitter<number> = new EventEmitter<number>();

    liveryUrl: string;
    noLogoUrl: string = URL_NO_IMG;
    constructor(
        private router: Router,
        private cus: CurrentUserService) { }


    ngOnInit(): void {
        this.initFields();
    }

    ngOnDestroy(): void {

    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['id']) {
            this.id = changes['id'].currentValue;
            this.initFields();
        }
        if (changes['tooltip']) {
            this.tooltip = changes['tooltip'].currentValue;
            //this.initFields();
        }
    }

    initFields() {
        if(this.id){
            this.liveryUrl = URL_COMMON_IMAGES_COMPANY + 'livery_' + this.id.toString() + '.png?' + new Date().getTime();
        } else{
            this.liveryUrl = undefined;
        }
        //console.log('initFields-> liveryUrl:', this.liveryUrl);
    }

    liveryClicked() {
        this.liveryClick.emit(this.id);
    }

    onImageLoad() {
        //console.log('onImageLoad -> liveryUrl:', this.liveryUrl);
        this.getImageClass();
    }

    onImageError() {
        //console.log('onImageError -> liveryUrl:', this.liveryUrl);

        this.liveryUrl = undefined;
    }

    getImageClass() {
        const image = new Image();
        image.src = this.liveryUrl;
        if (image.width > image.height + 20 || image.width === image.height) {
            // return wide image class
            //return Wide;
        } else {
            //return Tall;
        }
    }

}

