import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TooltipPosition } from '@angular/material/tooltip';

@Component({
  selector: 'sx-button-round',
  templateUrl: './button.component.html'
})
// tslint:disable-next-line: component-class-suffix
export class SxButtonRound {

  constructor() { }

  /**
   * BINDINGS
   */
  @Input() icon: string;
  @Input() message: string;
  @Input() color: string;
  @Input() position: TooltipPosition = 'above' as TooltipPosition;
  @Input() disabled: boolean;



  /**
   * FIELDS
   */

}
