import { Component, EventEmitter, Input, Output, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'sx-back-button',
  templateUrl: './back-button.component.html'
})
export class SxBackButtonComponent implements OnDestroy {

  navigationSubscription;

  constructor(
    private location: Location,
    private router: Router) {
    this.navigationSubscription = this.router.events.subscribe(e => {
      if (e instanceof NavigationStart) {
        /*
        if ((this.location.path().includes(SubmittalManagerRoutes.Root + '/' + SubmittalManagerRoutes.Submittal + '/')) &&
          (e.url.includes(SubmittalManagerRoutes.Root + '/' + SubmittalManagerRoutes.Submittal + '/' + SubmittalManagerRoutes.Add + '/'))) {
          this.goBack();
        }*/
      }
    });
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  /**
   * BINDINGS
   */


  /**
   * FIELDS
   */
  goBack(): void {
    this.location.back();
  }

};