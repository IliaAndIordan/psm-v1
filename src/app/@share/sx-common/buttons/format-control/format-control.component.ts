import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { TooltipPosition } from '@angular/material/tooltip';

@Component({
  selector: 'sx-format-control',
  templateUrl: './format-control.component.html'
})
export class SxFormatControl implements OnInit {

  constructor() { }

  /**
   * BINDINGS
   */
  @Input() icon: string;
  @Input() label: string;
  @Input() color: string;
  @Input() large: boolean;
  @Input() disabled: boolean;
  @Input() tooltip: string = '';
  @Input() position: TooltipPosition = 'above' as TooltipPosition;

  /**
   * FIELDS
   */
  labeled: boolean;

  ngOnInit() {

    if (this.label) {
      this.labeled = true;
    } else {
      this.labeled = false;
    }

  }

};