import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'psm-button-info',
  templateUrl: './button.component.html'
})
export class ButtonInfo implements OnInit {

  constructor() { }

  /**
   * BINDINGS
   */
  @Output() onHover: EventEmitter<any> = new EventEmitter<any>();
  @Output() offHover: EventEmitter<any> = new EventEmitter<any>();
  @Input() message: string;
  @Input() toolTipPlacement: string = 'top';

  /**
   * FIELDS
   */
  
  ngOnInit() {

  }



};