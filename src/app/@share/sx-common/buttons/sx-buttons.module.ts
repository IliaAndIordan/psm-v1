import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SxMaterialModule } from '../../material.module';
import { PsmCoreModule } from 'src/app/@core/core.module';
import { SxProjectButtonComponent } from './sx-project-button.component';
import { ButtonInfo } from './info/button.component';
import { SxButtonRound } from './round/button.component';
import { SxFormatControl } from './format-control/format-control.component';
import { SxButtonFlat } from './flat/button.component';
import { SxBackButtonComponent } from './back/back-button.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SxMaterialModule,
    PsmCoreModule,
  ],
  declarations: [
    SxProjectButtonComponent,
    ButtonInfo,
    SxButtonRound,
    SxFormatControl,
    SxButtonFlat,
    SxBackButtonComponent,
  ],
  exports:[
    SxProjectButtonComponent,
    ButtonInfo,
    SxButtonRound,
    SxFormatControl,
    SxButtonFlat,
    SxBackButtonComponent,
  ],
 
  providers:[
  ]
})
export class PsmCommonButtonsModule { }
