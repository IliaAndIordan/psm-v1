import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SxSidenavComponent } from './sx-sidenav.component';

describe('SxSidenavComponent', () => {
  let component: SxSidenavComponent;
  let fixture: ComponentFixture<SxSidenavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SxSidenavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SxSidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
