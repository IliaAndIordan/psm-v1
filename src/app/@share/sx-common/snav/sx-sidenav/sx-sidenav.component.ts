import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDrawerMode, MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserModel } from 'src/app/@core/clients/api/auth/dto';
import { CompanyModel } from 'src/app/@core/clients/api/company/dto';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { Animate } from 'src/app/@core/constants/animation.const';
import { ExpandTab, ExpandTrigger, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, ShowlinksTrigger, ShowOverlayTrigger, SpinExpandIconTrigger } from 'src/app/@core/constants/animations-triggers';
import { AppRoutes } from 'src/app/@core/constants/app-routes.const';

@Component({
  selector: 'psm-sx-sidenav',
  templateUrl: './sx-sidenav.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, ExpandTrigger, ShowlinksTrigger, ShowOverlayTrigger]
})
export class SxSidenavComponent implements OnInit, OnDestroy {

  showText: string = 'hide';
  snavWidth: string = 'small';
  showOverlay: string = 'hideOverLay';

  rootes = AppRoutes;

  user: UserModel;
  userChanged: Subscription;
  
  company: CompanyModel;
  companyChanged:Subscription;

  constructor(
    private router: Router,
    private cus: CurrentUserService) {
  }

  ngOnInit(): void {

    this.userChanged = this.cus.userChanged.subscribe((user => {
      this.initFields();
    }));
    this.companyChanged = this.cus.companyChanged.subscribe((user => {
      this.initFields();
    }));
    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.userChanged) { this.userChanged.unsubscribe(); }
    if (this.companyChanged) { this.companyChanged.unsubscribe(); }
  }
  
  initFields() {
    this.showText = Animate.hide;
    this.user = this.cus.user;
    this.company = this.cus.company;
  }

  //#region Action Metods

  snavToggleClick() {
   
  }

  // Expand Menu
  expandMenu(): void {
    this.snavWidth = Animate.large;
    this.showOverlay = 'showOverLay';
    this.showText = Animate.show;
   
  }

  // Close Menu
  closeMenu(): void {
    this.snavWidth = Animate.small;
    this.showOverlay = 'hideOverLay';
    this.showText = Animate.hide;
  }

  //#endregion
}
