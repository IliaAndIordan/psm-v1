import 'jasmine';
import { fakeAsync, flush, TestBed, waitForAsync } from '@angular/core/testing';
import { BehaviorSubject, Subject } from 'rxjs';

import { SxSpinnerService } from './sx-spinner.service';

describe('SxSpinnerService', () => {
  let service: SxSpinnerService;
  let subjectMock = new BehaviorSubject<boolean>(undefined);
  
  beforeEach( () => {
    TestBed.configureTestingModule({ providers: [SxSpinnerService] });
    service = TestBed.inject(SxSpinnerService);
  });


  it('should call show', () => {
    const showSpy = spyOn(service, 'show').and.callThrough();
    service.show();
    expect(showSpy).toHaveBeenCalled();
    
    service.status.subscribe(res => {
      expect(res).toBe(true);
    });
    
  });
/*
  it('should call #setMessage', fakeAsync(() => {
    const stubValue: string = 'stub value';
    service.setMessage(stubValue);
    service.message.subscribe(res => {
      expect(res).toBe(stubValue);
    });
  }));

  it('should call #hide', fakeAsync(() => {
    const stubValue: string = 'stub value';
    service.hide();
    service.status.subscribe(res => {
      expect(res).toBe(false);
    });
  }));

*/

});
