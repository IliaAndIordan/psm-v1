import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SxSpinnerService {
  
  public showLoaderSuject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public loaderMessageSubscr: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  showLoader:boolean;
  private msg:string;

  get message(): string {
    return this.msg;
  }

  set message(value: string) {
    this.msg = value;
    this.loaderMessageSubscr.next(this.message);
  }

  constructor() { }

  display(value: boolean): void {
      // clear any message
      this.initLoader(value).then(res =>{
          if(!res){
              this.message = undefined;
          }
          this.showLoaderSuject.next(this.showLoader);
      });
  }

  setMessage(value: string): void {
    this.message = value;
  }

  show(): void {
      this.display(true);
  }

  hide(): void {
      this.display(false);
  }

  initLoader(val: boolean): Promise<boolean> {
    let promise = new Promise<boolean>((resolve, reject) => {
      if (!this.message) { this.message = 'LOADING'; };
      this.showLoader = val;
      resolve(this.showLoader);
    });
    return promise;
  }
  
}
