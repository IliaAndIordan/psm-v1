import { ComponentFixture, fakeAsync, flush, TestBed, waitForAsync } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SxSpinnerComponent } from './sx-spinner.component';
import { SxSpinnerService } from './sx-spinner.service';

describe('SxSpinnerComponent', () => {
  let component: SxSpinnerComponent;
  let fixture: ComponentFixture<SxSpinnerComponent>;
  let service: SxSpinnerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports:[BrowserAnimationsModule],
      declarations: [ SxSpinnerComponent ],
      providers: [SxSpinnerService]
    })
    .compileComponents();
    service = TestBed.inject(SxSpinnerService);
  });

  beforeEach(waitForAsync(() => {
    fixture = TestBed.createComponent(SxSpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should call show', fakeAsync(() => {
    const showSpy = spyOn(service, 'show').and.callThrough();
    service.show();
    fixture.detectChanges();
    flush();
    expect(showSpy).toHaveBeenCalled();
    service.status.subscribe(res => {
      expect(res).toBe(true);
      expect(component.isOpen).toBe(true);
    });

  }));
/*
  it('should show and hide', () => {
    service.show();
    expect(component.isOpen).toBe(true);
    service.hide();
    expect(component.isOpen).toBe(false);
  });

  it('should call toggle', () => {
    component.toggle();
    expect(component.isOpen).toBe(true);
    component.toggle();
    expect(component.isOpen).toBe(false);
  });
  */
});
