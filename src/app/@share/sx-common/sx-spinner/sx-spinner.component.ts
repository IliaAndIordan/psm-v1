import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterViewChecked, ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SxSpinnerService } from './sx-spinner.service';

@Component({
  selector: 'sx-spinner',
  templateUrl: './sx-spinner.component.html',
  styleUrls: ['./sx-spinner.component.scss'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        opacity: 0.8,
      })),
      state('closed', style({
        height: '0px', opacity: '0', display: 'none'
      })),
      transition('* => closed', [
        animate('500ms',
          style({ opacity: '*' }),
        ),
      ]),
      transition('* => open', [
        animate('0.8s')
      ]),
    ]),
  ],
})
export class SxSpinnerComponent implements OnInit, OnDestroy, AfterViewChecked {

  private _isOpen: boolean;
  private selector = 'globalLoader';

  showLoaderSuject: Subscription;
  loaderMessageSubscr: Subscription;
  loaderMessage: string;
  showLoaderVar: string;
  private _showLoader: boolean;

  get showLoader(): boolean {
    return this._showLoader;
  }

  set showLoader(value: boolean) {
    this._showLoader = value;
    this.showLoaderVar = this._showLoader ? 'open' : 'closed';
    /*
    setTimeout((value:boolean) => {
      this._showLoader = value;
      this.showLoaderVar = this._showLoader ? 'open' : 'closed';
    }, 60);*/
  }

  constructor(
    private spinnerService: SxSpinnerService,
    private cdRef: ChangeDetectorRef) { }


  ngOnInit(): void {
    this.showLoader = false;

    this.showLoaderSuject = this.spinnerService.showLoaderSuject.subscribe((val: boolean) => {
      this.initLoader(val).then(show => {
        if (!this.loaderMessage && show) { this.loaderMessage = 'LOADING'; };
      });
    });

    this.loaderMessageSubscr = this.spinnerService.loaderMessageSubscr.subscribe((val: string) => {
      this.loaderMessage = val;
    });
  }

  ngOnDestroy(): void {
    if (this.showLoaderSuject) { this.showLoaderSuject.unsubscribe(); }
    if (this.loaderMessageSubscr) { this.loaderMessageSubscr.unsubscribe(); }
  }

  ngAfterViewChecked(): void {
    let show = this.showLoaderVar === 'open';

    if (show != this.showLoader) { // check if it change, tell CD update view
      //console.log('ngAfterViewChecked -> show:', show);
      //console.log('ngAfterViewChecked -> showLoader:', this.showLoader);
      this.showLoader = show;
      this.cdRef.detectChanges();
    }
  }



  //#region Action Methods

  private getElement(): any {
    return document.getElementById(this.selector);
  }

  initLoader(val: boolean): Promise<boolean> {
    let promise = new Promise<boolean>((resolve, reject) => {
      if (!this.loaderMessage) { this.loaderMessage = 'LOADING'; };
      this.showLoader = val;
      resolve(this.showLoader);
    });
    return promise;
  }

  hide(): void {
    this.showLoader = false;
  }

  show(): void {
    if (!this.loaderMessage) { this.loaderMessage = 'LOADING'; }
    this.showLoader = true;
  }

  toggle(): void {
    if (!this.loaderMessage) { this.loaderMessage = 'LOADING'; }
    //this.isOpen = !this.isOpen;
  }
  //#endregion

}
