import { Component, OnInit, Input, ViewEncapsulation, HostBinding, Output, EventEmitter } from '@angular/core';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { COMMON_IMG_LOGO_RED, URL_NO_IMG_SQ } from 'src/app/@core/constants/app-storage.const';
import { SmProduct, SmProductViewModel } from 'src/app/@core/pipes/sx-product.pipe';
const Wide = 'listcard-image-wide';
const Tall = 'listcard-image-tall';
@Component({
    selector: 'sx-product-line',
    templateUrl: './product-line.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class SxProductLineComponent implements OnInit {
    @Input() product: SmProductViewModel;
    @Input() isGranted: boolean;
    @Output() grandProduct: EventEmitter<SmProductViewModel> = new EventEmitter<SmProductViewModel>();
    @Output() revokeProduct: EventEmitter<SmProductViewModel> = new EventEmitter<SmProductViewModel>();

    imgUrl = URL_NO_IMG_SQ;
    imgClass: string;
    enableProductAcitavion: boolean;


    constructor(private cus: CurrentUserService) { }

    ngOnInit() {
        this.enableProductAcitavion = this.cus.isAdmin;
        if (this.product) {
            switch (this.product.id) {
                case SmProduct.SupplierExchange:
                    this.imgUrl = COMMON_IMG_LOGO_RED;
                    break;
                default:
                    this.imgUrl = URL_NO_IMG_SQ;
                    break;
            }
        }
        this.imgClass = this.getImageClass();
    }

    getImageClass() {
        const image = new Image();
        image.src = this.imgUrl;

        if (image.width > image.height + 10 || image.width === image.height) {
            //return wide image class 
            return Wide;//Tall;
        } else {
            return Wide;
        }
    }

    activeChecked(event: boolean): void {
        console.log('activeChecked -> event:', event);
        if (event) {
            this.product.isGranted = true;
            this.grandProduct.emit(this.product);
        } else {
            this.product.isGranted = false;
            this.revokeProduct.emit(this.product);
        }
    }
}
