import { Component, OnInit, OnDestroy, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { URL_NO_IMG } from 'src/app/@core/constants/app-storage.const';


@Component({
    selector: 'psm-image-logo-td',
    templateUrl: './psm-image-logo-td.component.html',
})

export class PsmImageLogoTdComponent implements OnInit, OnDestroy, OnChanges {


    @Input() id: number;
    @Input() imgUrl: string;
    @Input() tooltip: string;
    @Input() imgclass: string = 'livery-td';
    @Output() imageClicked: EventEmitter<number> = new EventEmitter<number>();

    noImageUrl: string = URL_NO_IMG;

    constructor(
        private router: Router,
        private cus: CurrentUserService) { }


    ngOnInit(): void {
        this.initFields();
    }

    ngOnDestroy(): void {

    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['id']) {
            this.id = changes['id'].currentValue;
            this.initFields();
        }
        if (changes['imgUrl']) {
            this.imgUrl = changes['imgUrl'].currentValue;
            this.initFields();
        }
        if (changes['tooltip']) {
            this.tooltip = changes['tooltip'].currentValue;
            //this.initFields();
        }
    }

    initFields() {
        if (this.imgUrl) {
            //this.imgUrl = URL_COMMON_IMAGE_AIRLINE + 'livery_' + this.alId.toString() + '.png?' + new Date().getTime();
        } else {
            //this.imgUrl = undefined;
        }
        //console.log('initFields-> liveryUrl:', this.liveryUrl);
    }

    imageClick() {
        this.imageClicked.emit(this.id);
    }

    onImageLoad() {
        //console.log('onImageLoad -> liveryUrl:', this.liveryUrl);
        this.getImageClass();
    }

    onImageError() {
        console.log('onImageError -> imgUrl:', this.imgUrl);
        this.imgUrl = undefined;
    }

    getImageClass() {
        if (this.imgUrl) {
            const image = new Image();
            image.src = this.imgUrl;
            if (image.width > image.height + 20 || image.width === image.height) {
                // return wide image class
                //return Wide;
            } else {
                //return Tall;
            }
        }

    }

}

