import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange, ViewEncapsulation } from '@angular/core';
import { UserModel } from 'src/app/@core/clients/api/auth/dto';
import { PsmUserRole, PsmUserRoleOpt } from 'src/app/@core/clients/api/auth/enums';
// ---Services
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { AppStore, URL_NO_IMG_SQ } from 'src/app/@core/constants/app-storage.const';
// ---Models


const Wide = 'listcard-image-wide';
const Tall = 'listcard-image-tall';
@Component({
    selector: 'psm-user-card',
    templateUrl: './user-card.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class PsmUserCardComponent implements OnInit, OnChanges {

    /** Bindings */
    @Input() user: UserModel;
    @Output() editUser: EventEmitter<UserModel> = new EventEmitter<UserModel>();
    @Output() sendEmail: EventEmitter<UserModel> = new EventEmitter<UserModel>();
    @Output() viewProfile: EventEmitter<UserModel> = new EventEmitter<UserModel>();

    imgUrl = URL_NO_IMG_SQ;
    imgClass: string;
    roleOpt = PsmUserRole;

    isAdmin: boolean;

    constructor(private cus: CurrentUserService) { }

    ngOnInit() {
        this.isAdmin = this.cus.isAdmin || this.cus.isCompanyAdmin;
        if (this.user) {
            this.imgUrl = this.cus.getAvatarUrl(this.user.email);
        }
        this.imgClass = this.getImageClass();
    }

    ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
        if (changes['user']) {
            this.user = changes['user'].currentValue;
            this.ngOnInit();
        }

    }

    onImageLoad(): void {
        this.imgClass = this.getImageClass();
    }


    getImageClass() {
        const image = new Image();
        image.src = this.imgUrl;

        if (image.width > image.height + 10 || image.width === image.height) {
            return Tall;
        } else {
            return Wide;
        }
    }

    sendEmailClick() {
        this.sendEmail.emit(this.user);
    }

    editUserClick() {
        this.editUser.emit(this.user);
    }

    gotoUserProfile() {
        this.viewProfile.emit(this.user);
    }

}
