import { Component, OnInit, OnDestroy, Input, OnChanges, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { UserModel } from 'src/app/@core/clients/api/auth/dto';
import { PsmUserRole, PsmUserRoleOpt } from 'src/app/@core/clients/api/auth/enums';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { URL_NO_IMG_SQ } from 'src/app/@core/constants/app-storage.const';

const Wide = 'listcard-image-wide';
const Tall = 'listcard-image-tall';

@Component({
    selector: 'pms-user-avatar-480-card',
    templateUrl: './user-avatar-480-card.component.html'
})

export class PsmUserAvatar480CardComponent implements OnInit, OnDestroy, OnChanges {


    @Input() user: UserModel;

    @Output() editUser: EventEmitter<UserModel> = new EventEmitter<UserModel>();
    @Output() sendEmail: EventEmitter<UserModel> = new EventEmitter<UserModel>();

    imgUrl = URL_NO_IMG_SQ;
    noImgUrl = URL_NO_IMG_SQ;
    imgClass: string;
    roleOpt = PsmUserRole;

    isAdmin: boolean;

    constructor(
        private router: Router,
        private cus: CurrentUserService) { }

   
    ngOnInit(): void {
        this.initFields();
    }

    ngOnDestroy(): void {

    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['user']) {
            this.user = changes['user'].currentValue;
            this.initFields();
        }
    }

    initFields(){
        this.isAdmin = this.cus.isAdmin || this.cus.isCompanyAdmin;
        if (this.user) {
            this.imgUrl = this.cus.getAvatarUrl(this.user.email);
        }
        this.imgClass = this.getImageClass();
    }

    onImageLoad(): void {
        this.imgClass = this.getImageClass();
    }


    getImageClass() {
        const image = new Image();
        image.src = this.imgUrl;

        if (image.width > image.height + 10 || image.width === image.height) {
            return Tall;
        } else {
            return Wide;
        }
    }

    sendEmailClick() {
        this.sendEmail.emit(this.user);
    }

    editUserClick() {
        this.editUser.emit(this.user);
    }

}
