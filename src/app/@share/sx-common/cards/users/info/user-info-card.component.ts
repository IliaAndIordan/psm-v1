import { Component, OnInit, OnDestroy, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UserModel } from 'src/app/@core/clients/api/auth/dto';
import { PsmUserRole } from 'src/app/@core/clients/api/auth/enums';
import { CompanyModel } from 'src/app/@core/clients/api/company/dto';
import { CompanyType } from 'src/app/@core/clients/api/company/enums';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { URL_NO_IMG } from 'src/app/@core/constants/app-storage.const';


@Component({
    selector: 'psm-user-info-card',
    templateUrl: './user-info-card.component.html',
})

export class PsmUserInfoCardComponent implements OnInit, OnDestroy, OnChanges {

    @Output() generateLivery: EventEmitter<CompanyModel> = new EventEmitter<CompanyModel>();
    @Output() editClicked: EventEmitter<UserModel> = new EventEmitter<UserModel>();
    @Input() user: UserModel;
    @Input() company: CompanyModel;

    admin = PsmUserRole.Admin;
    comAdmin = PsmUserRole.CompanyAdmin;
    

    constructor(
        private router: Router,
        private cus: CurrentUserService) { }

   
    ngOnInit(): void {
        this.initFields();
    }

    ngOnDestroy(): void {

    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['user']) {
            this.user = changes['user'].currentValue;
            this.initFields();
        }
        if (changes['company']) {
            this.company = changes['company'].currentValue;
            this.initFields();
        }
    }

    initFields(){
        if(this.user && this.company && this.user.companyId === this.company.companyId){
            this.user.companyName = this.company.name;
            this.user.branchCode = this.company.branch;
            this.user.countryId = this.company.countryId;
        }
    }

}

