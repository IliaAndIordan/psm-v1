import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SxMaterialModule } from '../../material.module';
import { PsmCoreModule } from 'src/app/@core/core.module';
import { PsmCommonButtonsModule } from '../buttons/sx-buttons.module';
import { CompanyInfoCardComponent } from './company/info/company-info-card.component';
import { SxCompanyLiveryNameCardComponent } from './company/sx-company-livery-name-card/sx-company-livery-name-card.component';
import { SxCompanyLiveryComponent } from './company/livery/sx-company-livery.component';
import { CompanyLogo480CardComponent } from './company/logo/company-logo-480-card.component';
import { PsmUserCardComponent } from './users/user-card.component';
import { PsmUserAvatar480CardComponent } from './users/logo/user-avatar-480-card.component';
import { PsmUserInfoCardComponent } from './users/info/user-info-card.component';
import { PsmImageLogoTdComponent } from './image/logo-td/psm-image-logo-td.component';
import { PsmSidePanelModalModule } from '../side-panel-modal/psm-side-panel-modal.module';



@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SxMaterialModule,
    PsmCoreModule,

    PsmCommonButtonsModule,
    PsmSidePanelModalModule,
  ],
  declarations: [
    //--- Company
    CompanyInfoCardComponent,
    SxCompanyLiveryComponent,
    CompanyLogo480CardComponent,
    SxCompanyLiveryNameCardComponent,
    //--- User
    PsmUserCardComponent,
    PsmUserAvatar480CardComponent,
    PsmUserInfoCardComponent,
    //--- Image
    PsmImageLogoTdComponent,

  ],
  exports: [
     //--- Company
     SxCompanyLiveryNameCardComponent,
     CompanyInfoCardComponent,
     SxCompanyLiveryComponent,
     CompanyLogo480CardComponent,
    //--- User
    PsmUserCardComponent,
    PsmUserAvatar480CardComponent,
    PsmUserInfoCardComponent,
    //--- Image
    PsmImageLogoTdComponent,
  ],

  providers: [
  ]
})
export class PsmCommonCardsModule { }
