import { Component, OnInit, OnDestroy, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { PsmUserRole, PsmUserRoleOpt } from 'src/app/@core/clients/api/auth/enums';
import { CompanyModel } from 'src/app/@core/clients/api/company/dto';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { URL_NO_IMG } from 'src/app/@core/constants/app-storage.const';


@Component({
    selector: 'sx-company-logo-480-card',
    templateUrl: './company-logo-480-card.component.html',
})

export class CompanyLogo480CardComponent implements OnInit, OnDestroy, OnChanges {


    @Input() company: CompanyModel;
    @Output() generateLogo: EventEmitter<CompanyModel> = new EventEmitter<CompanyModel>();

    logoUrl:string;
    liveryUrl:string;
    noLogoUrl = URL_NO_IMG;

    canGenerate:boolean = false;

    constructor(
        private router: Router,
        private cus: CurrentUserService) { }

   
    ngOnInit(): void {
        this.initFields();
    }

    ngOnDestroy(): void {

    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['company']) {
            this.company = changes['company'].currentValue;
            this.initFields();
        }
    }

    initFields(){
        this.logoUrl = this.company && this.company.sxLogoUrl ? this.company.sxLogoUrl : this.noLogoUrl;
        this.liveryUrl = this.company && this.company.sxLiveryUrl ? this.company.sxLiveryUrl : this.noLogoUrl;
        this.canGenerate = ((this.cus.user.companyId === this.company.companyId) &&  (this.cus.user.role < PsmUserRole.User));
    }

    generateLogoClick(){
        if(this.company){
            this.generateLogo.emit(this.company);
        }
        
    }

    gotoWebSiteUrl(){
        if(this.company.webUrl && this.company.webUrl.length>8)
        window.open(this.company.webUrl, "_blank");
    }

}

