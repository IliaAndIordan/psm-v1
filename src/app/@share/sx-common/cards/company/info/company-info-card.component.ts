import { Component, OnInit, OnDestroy, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { CompanyModel } from 'src/app/@core/clients/api/company/dto';
import { CompanyType } from 'src/app/@core/clients/api/company/enums';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { URL_NO_IMG } from 'src/app/@core/constants/app-storage.const';


@Component({
    selector: 'psm-company-info-card',
    templateUrl: './company-info-card.component.html',
})

export class CompanyInfoCardComponent implements OnInit, OnDestroy, OnChanges {

    @Output() generateLivery: EventEmitter<CompanyModel> = new EventEmitter<CompanyModel>();
    @Input() company: CompanyModel;

    contractor = CompanyType.Contractor;
    distr = CompanyType.Distributor;
    mfr = CompanyType.Manufacturer;

    noImageUrl = URL_NO_IMG;
    imageUrl: string;
    
    logoUrl:string;
    liveryUrl:string;
    constructor(
        private router: Router,
        private cus: CurrentUserService) { }

   
    ngOnInit(): void {
        this.initFields();
    }

    ngOnDestroy(): void {

    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['company']) {
            this.company = changes['company'].currentValue;
            this.initFields();
        }
    }

    initFields(){
        this.logoUrl = this.company.logoUrl?this.company.logoUrl: this.company.logoUrl;
        this.liveryUrl =  this.company.sxLiveryUrl;
    }

    generateLiveryClick(){
        if(this.company){
            this.generateLivery.emit(this.company);
        }
        
    }

}

