import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { CompanyModel } from 'src/app/@core/clients/api/company/dto';
import { ImgTall, ImgWide,URL_NO_IMG } from 'src/app/@core/constants/app-storage.const';

@Component({
  selector: 'sx-company-livery',
  templateUrl: './sx-company-livery.component.html',
})
export class SxCompanyLiveryComponent implements OnInit, OnChanges {

  @Input() company: CompanyModel;
  @Input() tooltip: string;
  @Input() imageClass: string = 'livery-table';
  @Output() liveryClicked: EventEmitter<CompanyModel> = new EventEmitter<CompanyModel>();


  noImageUrl = URL_NO_IMG;
  imageUrl: string;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['company']) {
        this.company = changes['company'].currentValue;
        this.initFields();
    }
    if (changes['imageClass']) {
      this.imageClass = changes['imageClass'].currentValue;
      this.initFields();
  }
}

  initFields(): void {
    this.imageUrl = this.company ?this.company.sxLiveryUrl:this.noImageUrl;
  }

  onImageError() {
    this.imageUrl = this.noImageUrl;
    console.log('onImageError-> imageUrl:', this.imageUrl);
    this.onImageLoad();
  }

  onImageLoad() {
    let image = new Image();
    image.src = this.imageUrl;
    /*
    if (image.width > image.height + 20 || image.width === image.height) {
      this.imageClass = ImgWide;
    } else {
      this.imageClass = ImgTall;
    }*/
  }

  liveryClick(){
    if(this.company){
        this.liveryClicked.emit(this.company);
    }
    
}

}
