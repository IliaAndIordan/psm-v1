import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { CompanyModel } from 'src/app/@core/clients/api/company/dto';
import { ImgTall, ImgWide,URL_NO_IMG } from 'src/app/@core/constants/app-storage.const';

@Component({
  selector: 'sx-company-livery-name-card',
  templateUrl: './sx-company-livery-name-card.component.html',
})
export class SxCompanyLiveryNameCardComponent implements OnInit, OnChanges {

  @Input() company: CompanyModel;

  noImageUrl = URL_NO_IMG;
  imageUrl: string;
  imageClass: string;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['company']) {
        this.company = changes['company'].currentValue;
        this.initFields();
    }
}

  initFields(): void {
    this.imageClass = ImgWide;
    this.imageUrl = this.company ?this.company.sxLiveryUrl:this.noImageUrl;
  }

  onImageError() {
    this.imageUrl = this.noImageUrl;
    console.log('onImageError-> imageUrl:', this.imageUrl);
    this.onImageLoad();
  }

  onImageLoad() {
    let image = new Image();
    image.src = this.imageUrl;
    if (image.width > image.height + 20 || image.width === image.height) {
      this.imageClass = ImgWide;
    } else {
      this.imageClass = ImgTall;
    }
  }

}
