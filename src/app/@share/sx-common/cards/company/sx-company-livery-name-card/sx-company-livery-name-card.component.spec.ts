import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SxCompanyLiveryNameCardComponent } from './sx-company-livery-name-card.component';

describe('SxCompanyLiveryNameCardComponent', () => {
  let component: SxCompanyLiveryNameCardComponent;
  let fixture: ComponentFixture<SxCompanyLiveryNameCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SxCompanyLiveryNameCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SxCompanyLiveryNameCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
