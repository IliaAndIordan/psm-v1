import { Component, OnDestroy, OnInit, EventEmitter, Input, Output, ViewChild, OnChanges, SimpleChanges, } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
// Animations
import { TogglePanelActionTrigger, TogglePanelTrigger } from './animation';
import { Animate } from 'src/app/@core/constants/animation.const';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
// Enums

@Component({
  selector: 'sx-filter-panel',
  templateUrl: './sx-filter-panel.component.html',
  animations: [TogglePanelTrigger, TogglePanelActionTrigger]
})
export class SxFilterPanelComponent implements OnInit, OnDestroy, OnChanges {

  @Output() closePanel: EventEmitter<any> = new EventEmitter<any>();
  @Output() openPanel: EventEmitter<any> = new EventEmitter<any>();
  @Output() subtitleClick: EventEmitter<any> = new EventEmitter<any>();
  @Input() panelVar: string;
  @Input() fptitle: string;
  @Input() subtitle: string;
  @Input() icon: string;
  @Input() logoUrl: string;
  @Input() id: number;


  panelOut:string;
  constructor(
    private cus: CurrentUserService,) {
  }

  ngOnInit() {
    this.panelOut = Animate.out;//this.panelVar === Animate.out ? Animate.in : Animate.out;
  }

  ngOnDestroy(): void {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['panelVar']) {
      this.panelVar = changes['panelVar'].currentValue;
      this.panelOut =  Animate.out;//this.panelVar === Animate.out ? Animate.in : Animate.out;
  }
  }




  closePanelClick(): void {
    // this.panelVar = Animate.in;
    this.closePanel.emit();
  }
  openPanelClick(): void {
    // this.panelVar = Animate.in;
    this.openPanel.emit();
  }

  subtitleClickEv(): void {
    // this.panelVar = Animate.in;
    this.subtitleClick.emit();
  }

}//end class

