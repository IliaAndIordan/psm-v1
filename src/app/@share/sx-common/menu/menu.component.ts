import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserModel } from 'src/app/@core/clients/api/auth/dto';
import { CompanyModel } from 'src/app/@core/clients/api/company/dto';
import { CompanyType } from 'src/app/@core/clients/api/company/enums';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { TokenService } from 'src/app/@core/clients/token.service';
import { Animate } from 'src/app/@core/constants/animation.const';
import { AppRoutes } from 'src/app/@core/constants/app-routes.const';
import { AppStore } from 'src/app/@core/constants/app-storage.const';
import { environment } from 'src/environments/environment';
import { SxSpinnerService } from '../sx-spinner/sx-spinner.service';

// Animations 
import { ExpandTrigger, ShowlinksTrigger, ShowOverlayTrigger } from './animation';
// Services


@Component({
  selector: 'sx-main-menu',
  templateUrl: './menu.component.html',
  styles: [],
  animations: [ExpandTrigger, ShowlinksTrigger, ShowOverlayTrigger]
})
export class SxMenuComponent implements OnInit, OnDestroy {

  @Input() isVisible: boolean = false;

  rootes = AppRoutes;

  isLoggedIn: boolean = false;
  isNavContracted: boolean = true;
  navStateVar: string = 'small';
  linksVar: string = 'hide';
  overlayVar: string = 'hideOverLay';
  // Entitlements
  isManufacturer = false;
  isContractor = false;
  isDistributor = false;

  isAdmin: boolean;
  isProduction = environment.production;

  company: CompanyModel;

  user: UserModel;
  userChanged: Subscription;

  constructor(
    private router: Router,
    private cus: CurrentUserService,
    private spinnerService: SxSpinnerService,
    private tokenService: TokenService) {
    this.isManufacturer = false;
    this.isContractor = false;
    this.isDistributor = false;
  }

  ngOnInit() {
    this.userChanged = this.cus.userChanged
      .subscribe(user => {
        this.initFields();
      });

  } // end oninit

  ngOnDestroy(): void {
    if (this.userChanged) { this.userChanged.unsubscribe(); }
  }


  initFields() {
    this.isAdmin = this.cus.isAdmin;
    this.isLoggedIn = this.cus.isLogged;
    this.company = this.cus.company;
    this.user = this.cus.user;
    this.getUserEntitlements();
  }

  getUserEntitlements(): void {

    this.isContractor = this.company ? this.company.typeId === CompanyType.Contractor : false;
    this.isDistributor = this.company ? this.company.typeId === CompanyType.Distributor : false;
    this.isManufacturer = this.company ? this.company.typeId === CompanyType.Manufacturer : false;
  }


//#region Action Methods

gotoUserProfile(user: UserModel) {
  if (user) {
      localStorage.setItem(AppStore.psm_company_profile, JSON.stringify(user));
      this.router.navigate([AppRoutes.Root, AppRoutes.company, AppRoutes.profile]);
  }
}

//#endregion

//#region ANIMATIONS

  expandMenu(): void {
    this.navStateVar = Animate.large;
    this.overlayVar = 'showOverLay';
    this.linksVar = Animate.show;
    this.isNavContracted = false;

  }

  closeMenu(): void {
    this.navStateVar = Animate.small;
    this.overlayVar = 'hideOverLay';
    this.linksVar = Animate.hide;
    this.isNavContracted = true;
  }

  toggleMenu(): void {
    this.getUserEntitlements();
    if (this.isNavContracted) {
      this.expandMenu();
    } else {
      this.closeMenu();
    }

  }

  //#endregion

}