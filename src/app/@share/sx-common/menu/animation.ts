import { trigger, state, style, transition, animate } from '@angular/animations';
import { Animate } from 'src/app/@core/constants/animation.const';


export const ExpandTrigger = 
 trigger('expandTrigger',[
      state('small', style({
        width: '50px'
      })),
      state('large', style({
        width: '240px'
      })),
      transition(Animate.small + ' => ' + Animate.large, animate('200ms')),
      transition(Animate.large + ' => ' + Animate.small,animate('200ms')),
    ])

export const ShowlinksTrigger = 
trigger('showlinksTrigger',[
      state(Animate.hide, style({
        display: 'none',
        color: 'rgba(0,0,0,0.0)',
      })),
      state(Animate.show, style({
        display: 'block',

      })),
      transition(Animate.show + ' => ' + Animate.hide, animate('200ms')),
      transition(Animate.hide + ' => ' + Animate.show, animate('400ms'))
    ])

export const ShowOverlayTrigger = 
trigger('showOverlayTrigger',[
      state('hideOverLay', style({
        display: 'none',
        backgroundColor: 'rgba(0,0,0,0.0)'
      })),
      state('showOverLay', style({
        display: 'block',
        backgroundColor: 'rgba(0,0,0,0.5)'
      })),
      transition('hideOverLay => showOverLay', animate('300ms')),
      transition('showOverLay => hideOverLay', animate('100ms'))
    ])
