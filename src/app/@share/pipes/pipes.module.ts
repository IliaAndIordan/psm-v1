import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SxPipesModule } from "src/app/@core/pipes/pipes.module";
import { TruncatePipe } from "./truncate.pipe";


@NgModule({
    imports: [
      CommonModule,
      SxPipesModule,
    ],
    declarations: [
        TruncatePipe,
    ],
    exports:[
        SxPipesModule,
        TruncatePipe,
    ]
  })
  export class SxSharePipeModule { }
  