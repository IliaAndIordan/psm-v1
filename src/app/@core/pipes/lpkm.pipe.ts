import { Pipe, PipeTransform, LOCALE_ID, Inject } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { AppStore } from '../constants/app-storage.const';
import { LocalUserSettings } from '../clients/models/local-user-settings';


@Pipe({name: 'lpkm'})
export class LpkmPipe implements PipeTransform {


    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: number): string {

        if (isNaN(value)) { return undefined; }
        if (value === null) { return undefined; }
        if (value === 0) { return undefined; }

        const argStr = sessionStorage.getItem(AppStore.UserSettings);
        let lpkmToMpg = 1;
        let label = 'l/100 km';
        if (argStr) {
            const settings = Object.assign(new LocalUserSettings(), JSON.parse(argStr));
            if (settings) {
                lpkmToMpg = settings.displayWeightLb ? (2.3521/100) : 1;
                label = settings.displayWeightLb ? 'mpg' : 'l/100 km';
            }
        }
        const num = value * lpkmToMpg;

        const rv = new DecimalPipe(this.locale).transform(num, '1.2-2') + ' ' + label;
        return rv;

    }
}
