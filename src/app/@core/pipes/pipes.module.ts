import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyStatusDisplayPipe, CompanyTypeDisplayPipe } from "../clients/api/company/enums";
import { SmProductNamePipe } from './sx-product.pipe';
import { TsDatePipe } from './ts-date.pipe';
import { LatLonPipe } from './latlon.pipe';
import { DistanseNmiPipe, LengthPipe, SpeedPipe } from './length.pipe';
import { MtowPipe } from './mtow.pipe';
import { UtcPipe } from './utc.pipe';
import { AmsStatusDisplayPipe, WadStatusDisplayPipe } from './ams-status.enums';
import { AmsAcIfeTypeDescrDisplayPipe, AmsAcIfeTypeDisplayPipe } from './ac-ife-type.pipe';
import { TruncatePipe } from './truncate.pipe';
import { AcStatusDisplayPipe, AircraftActionDisplayPipe } from './aircraft-statis.pipe';
import { PeriodMinPipe } from './interval.pipe';
import { LpkmPipe } from './lpkm.pipe';
import { PsmUserRoleDisplayPipe } from '../clients/api/auth/enums';
import { PsmModuleTypeTypeDisplayPipe } from '../clients/api/psm-product/enums';
import { SxProjectStatusDisplayPipe } from '../clients/api/project/enums';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        CompanyTypeDisplayPipe,
        CompanyStatusDisplayPipe,
        SmProductNamePipe,
        PsmUserRoleDisplayPipe,
        TsDatePipe,
        LatLonPipe,
        LengthPipe,
        DistanseNmiPipe,
        MtowPipe,
        UtcPipe,
        WadStatusDisplayPipe,
        AmsStatusDisplayPipe,
        AmsAcIfeTypeDisplayPipe,
        AmsAcIfeTypeDescrDisplayPipe,
        TruncatePipe,
        AcStatusDisplayPipe,
        SpeedPipe,
        PeriodMinPipe,
        LpkmPipe,
        AircraftActionDisplayPipe,
        PsmModuleTypeTypeDisplayPipe,
        SxProjectStatusDisplayPipe,
    ],
    exports:[
        CompanyTypeDisplayPipe,
        CompanyStatusDisplayPipe,
        SmProductNamePipe,
        PsmUserRoleDisplayPipe,
        TsDatePipe,
        LatLonPipe,
        LengthPipe,
        DistanseNmiPipe,
        MtowPipe,
        UtcPipe,
        WadStatusDisplayPipe,
        AmsStatusDisplayPipe,
        AmsAcIfeTypeDisplayPipe,
        AmsAcIfeTypeDescrDisplayPipe,
        TruncatePipe,
        AcStatusDisplayPipe,
        SpeedPipe,
        PeriodMinPipe,
        LpkmPipe,
        AircraftActionDisplayPipe,
        PsmModuleTypeTypeDisplayPipe,
        SxProjectStatusDisplayPipe,
    ],
    providers: [
        /*
        CompanyTypeDisplayPipe,
        CompanyStatusDisplayPipe,
        SmProductNamePipe,
        UserRoleDisplayPipe,
        TsDatePipe,
        LatLonPipe,
        LengthPipe,
        DistanseNmiPipe,
        MtowPipe,
        UtcPipe,
        WadStatusDisplayPipe,
        AmsStatusDisplayPipe,
        AmsAcIfeTypeDisplayPipe,
        AmsAcIfeTypeDescrDisplayPipe,
        TruncatePipe,
        AcStatusDisplayPipe,
        SpeedPipe,
        PeriodMinPipe,
        LpkmPipe,
        AircraftActionDisplayPipe,
        PsmModuleTypeTypeDisplayPipe*/
    ]
})
export class SxPipesModule { }
