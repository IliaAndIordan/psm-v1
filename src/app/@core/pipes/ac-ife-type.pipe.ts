import { Pipe, PipeTransform } from "@angular/core";

//#region AcIfeType
export class AmsAcIfeTypeO {
    public ifeId: number;
    public order: number;
    public iname: string;
    public description: string;
    public stars: number;
    public priceInstalation: number;
    public pricePerPax: number;
    public points: number;
}

export enum AcIfeType  {
    Base = 1,
    BaseWiFi = 2,
    BaseTV = 3,
    BaseHQTV = 4,
    QualityTV = 5,
    QualityTVPersonalAudio = 6,
    QualityTVPersonalAudioAndVideo = 7,
    MovingMap = 8,
    GameSystem = 9,
    SatillitePhone = 10,
    WiFi = 11,
    MobilePhone = 12,
}

export const AcIfeTypeOpt: AmsAcIfeTypeO[] = [
    { ifeId:1,
        order:1,
        iname:"Base",
        description:"Public Inflight Music",
        stars:1,
        priceInstalation:40,
        pricePerPax:1,
        points:1
    },
    { ifeId:2,
        order:2,
        iname:"Base + Wi-Fi",
        description:"Public Inflight Music + In-flight internet service.",
        stars:3,
        priceInstalation:250,
        pricePerPax:12,
        points:15
    },
    { ifeId:3,
        order:3,
        iname:"Base + TV",
        description:"Good quality TVs hanging from the top of a seat or in the middle of the aisle. 1 TV per 100 seat.",
        stars:1,
        priceInstalation:75,
        pricePerPax:3,
        points:6
    },
    { ifeId:4,
        order:4,
        iname:"Base + HQ TV 2",
        description:"High quality TVs hanging from the top of a seat or in the middle of the aisle. 2 TV per 100 seats:",
        stars:1,
        priceInstalation:85,
        pricePerPax:4,
        points:8
    },
    { ifeId:5,
        order:5,
        iname:"Quality TV",
        description:"Excellent quality TVs hanging from the top of a seat or in the middle of the aisle. 2 TV per 100 seats. One Vireo and One real-time flight information channels.",
        stars:1,
        priceInstalation:85,
        pricePerPax:4,
        points:8
    },
    { ifeId:6,
        order:6,
        iname:"Quality TV Personal Audio",
        description:"TVs hanging from the top of a seat or in the middle of the aisle. 2 TV per 100 seats. Best quality of the TVs.<br/>A personal on demand music player for each seat. 15 on demand music chanels",
        stars:2,
        priceInstalation:266,
        pricePerPax:5,
        points:10
    },
    { ifeId:7,
        order:7,
        iname:"Quality TV 3 Personal Audio and Video",
        description:"Full Cabin Movie Projectors (2 TV screens per 100 seats) Audio on Demand and Video on Demand systems.<br/>A personal on demand music player for each seat. 15 music and 15 vireo chanels",
        stars:3,
        priceInstalation:465,
        pricePerPax:11,
        points:15
    },
    { ifeId:8,
        order:8,
        iname:"Moving-map",
        description:"Full Cabin Movie Projectors (2 TV screens per 100 seats) Audio on Demand and Video on Demand systems.<br/>A personal on demand music player for each seat. 15 music and 15 vireo chanels. Real-time flight information video channel.",
        stars:3,
        priceInstalation:822,
        pricePerPax:18,
        points:30
    },
    { ifeId:9,
        order:9,
        iname:"Game system",
        description:"Full Cabin Movie Projectors (2 TV screens per 100 seats) Audio on Demand and Video on Demand systems.<br/>A personal on demand music player for each seat. 15 music and 15 vireo chanels. Real-time flight information video channel.Personal Video Game",
        stars:4,
        priceInstalation:1069,
        pricePerPax:22,
        points:40
    },
    { ifeId:10,
        order:10,
        iname:"Satillite Phone",
        description:"Game system + Satellite telephones integrated into system. These are either found at strategic locations in the aircraft or integrated into the passenger remote control used for the individual in-flight entertainment. Passengers can use their credit card to make phone calls anywhere on the ground.",
        stars:4,
        priceInstalation:1200,
        pricePerPax:30,
        points:50
    },
    { ifeId:11,
        order:11,
        iname:"Wi-Fi",
        description:"Satillite Phone + In-flight internet service. It is provided either through a satellite network or an air-to-ground network. Allows passengers to connect to live Internet from the individual IFE units or their laptops via the in-flight Wi-Fi access",
        stars:5,
        priceInstalation:1600,
        pricePerPax:38,
        points:60
    },
    { ifeId:12,
        order:12,
        iname:"In flight mobile phone coverage.",
        description:"Wi-Fi + inflight mobile connectivity. The GSM network connects to the ground infrastructure via an Inmarsat SwiftBroadband satellite which provides consistent global coverage.",
        stars:5,
        priceInstalation:1800,
        pricePerPax:50,
        points:80
    },
  ];


  @Pipe({ name: 'acifetype' })
  export class AmsAcIfeTypeDisplayPipe implements PipeTransform {
      transform(value: number, args?: string[]): string {
          let rv: string = AcIfeType[value];
          const data: AmsAcIfeTypeO = AcIfeTypeOpt.find(x => x.ifeId === value);
          if (data) {
              rv = data.iname;
          }
          return rv;
      }
  }
  
  @Pipe({ name: 'acifedescr' })
  export class AmsAcIfeTypeDescrDisplayPipe implements PipeTransform {
      transform(value: number, args?: string[]): string {
          //let rv: string = AcSeatType[value];
          //let rv = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/seat_icon_3.png';
          let rv: string = AcIfeType[value];
          const data: AmsAcIfeTypeO = AcIfeTypeOpt.find(x => x.ifeId === value);
          if (data) {
              rv = data.description;
          }
          return rv;
      }
  }
  
