import { Pipe, PipeTransform, LOCALE_ID, Inject } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { AppStore } from '../constants/app-storage.const';
import { LocalUserSettings } from '../clients/models/local-user-settings';



@Pipe({ name: 'len'})
export class LengthPipe implements PipeTransform {

    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: number): string {

        if (isNaN(value)) { return null; }
        if (value === null) { return null; }
        if (value === 0) { return null; }

        const argStr = sessionStorage.getItem(AppStore.UserSettings);
        let mToft = 1;
        let label = 'm';
        if (argStr) {
            const settings = Object.assign(new LocalUserSettings(), JSON.parse(argStr));
            if (settings) {
                mToft = settings.displayDistanceMl ? 3.28084 : 1;
                label = settings.displayDistanceMl ? 'ft' : 'm';
            }
        }
        const num = value * mToft;
        
        const rv = new DecimalPipe(this.locale).transform(num, '1.0-0') + ' ' + label;
        return rv;
    }

}

@Pipe({ name: 'nmi'})
export class DistanseNmiPipe implements PipeTransform {

    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: number): string {

        if (isNaN(value)) { return null; }
        if (value === null) { return null; }
        if (value === 0) { return null; }

        const argStr = sessionStorage.getItem(AppStore.UserSettings);
        let mToNmi = 0.539957;
        const label = 'nmi';
        
        const num = value * mToNmi;
        let abs = Math.abs(num);
        const rounder = Math.pow(10, 1);
        const isNegative = num < 0; // will also work for Negetive numbers
        let key = '';


        const powers = [
            { key: 'Q', num: Math.pow(10, 15) },
            { key: 'T', num: Math.pow(10, 12) },
            { key: 'B', num: Math.pow(10, 9) },
            { key: 'M', num: Math.pow(10, 6) },
            { key: 'k', num: 1000 }
        ];


        for (let i = 0; i < powers.length; i++) {
            let reduced = abs / powers[i].num;
            reduced = Math.round(reduced * rounder) / rounder;
            if (reduced >= 1) {
                abs = reduced;
                key = powers[i].key;
                break;
            }
        }
        const rv = (isNegative ? '-' : '') + new DecimalPipe(this.locale).transform(abs, '1.0-0') + ' ' + key + ' ' + label;
        
        //const rv = new DecimalPipe(this.locale).transform(num, '1.0-0') + ' ' + label;
        return rv;
    }
}


@Pipe({ name: 'speed'})
export class SpeedPipe implements PipeTransform {

    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: number): string {

        if (isNaN(value)) { return null; }
        if (value === null) { return null; }
        if (value === 0) { return null; }

        const argStr = sessionStorage.getItem(AppStore.UserSettings);
        let kmphTomiph = 1;
        let label = 'Km/h';
        if (argStr) {
            const settings = Object.assign(new LocalUserSettings(), JSON.parse(argStr));
            if (settings) {
                kmphTomiph = settings.displayDistanceMl ? 0.621371 : 1;
                label = settings.displayDistanceMl ? 'mph' : 'Km/h';
            }
        }
        const num = value * kmphTomiph;
        
        const rv = new DecimalPipe(this.locale).transform(num, '1.0-0') + ' ' + label;
        return rv;
    }

}
