import { Pipe, PipeTransform, LOCALE_ID, Inject } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Pipe({ name: 'periodmin' })
export class PeriodMinPipe implements PipeTransform {


    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: number): string {
        if (isNaN(value)) { return undefined; }
        if (value === null) { return undefined; }
        if (value === 0) { return undefined; }
        
        const sec_num = value * 60;
        const hours   = Math.floor(sec_num / 3600);
        const minutes = Math.floor(sec_num / 60) % 60;
        const seconds = sec_num % 60;
        /*
        const rv = [hours,minutes,seconds]
            .map(v => v < 10 ? "0" + v : v)
            .filter((v,i) => v !== "00" || i > 0)
            .join(":");
            */
        const rv = [0, hours,minutes]
            .map(v => v < 10 ? "0" + v : v)
            .filter((v,i) => v !== "00" || i > 0)
            .join(":");
        
        return rv;
    }
}
