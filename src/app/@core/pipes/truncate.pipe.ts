import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'truncate' })
export class TruncatePipe implements PipeTransform {
    transform(value: string, args: number): string {
        let rv = value;
        if (args === 0) {
            rv = value;
        }
        // Set limit to 10 if none declared
        const limit = Number(args) > 3 ? (args - 3) : 10;
        // Trail string
        const trail = '...';

        if (value) {
            if (value.length > limit) {
                rv = value.substring(0, limit) + trail;
            } else {
                rv = value;
            }
        }
        return rv;
    }
}
