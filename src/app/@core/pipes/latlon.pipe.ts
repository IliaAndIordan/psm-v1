import { Pipe, PipeTransform, LOCALE_ID, Inject } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { AppStore } from '../constants/app-storage.const';
import { LocalUserSettings } from '../clients/models/local-user-settings';



@Pipe({
    name: 'latlon'
})
export class LatLonPipe implements PipeTransform {

    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: number): string {

        if (isNaN(value)) { return null; }
        if (value === null) { return null; }
        if (value === 0) { return null; }

        const argStr = sessionStorage.getItem(AppStore.UserSettings);
        let mToft = 1;
        let label = ' ';
        if (argStr) {
            const settings = Object.assign(new LocalUserSettings(), JSON.parse(argStr));
            if (settings) {
                mToft = settings.displayDistanceMl ? 3.28084 : 1;
                label = settings.displayDistanceMl ? ' ' : ' ';
            }
        }
        const num = value;
        const rv = new DecimalPipe(this.locale).transform(num, '2.4-4');
        return rv;
    }
}
