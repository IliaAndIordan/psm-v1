import { Pipe, PipeTransform } from '@angular/core';

export enum SmProduct {
    SubmittalManager = 1,
    SupplierExchange = 2,
    ProjectStoryManager = 3,
}

export class SmProductViewModel {
    public id: SmProduct;
    public name: string;
    public abreviation: string;
    public isGranted: boolean;
}


export const SmProductOpt: SmProductViewModel[] = [
    {
        id: SmProduct.SubmittalManager,
        name: 'Submittal Manager',
        abreviation: 'SM',
        isGranted: false,
    },
    {
        id: SmProduct.SupplierExchange,
        name: 'Supplier Exchange',
        abreviation: 'SX',
        isGranted: false,
    },
    {
        id: SmProduct.ProjectStoryManager,
        name: 'Project Story Manager',
        abreviation: 'PSM',
        isGranted: false,
    },
];


@Pipe({ name: 'smproductname' })
export class SmProductNamePipe implements PipeTransform {
    transform(value: number, args?: string[]): string {
        let rv: string = SmProduct[value];
        const data: SmProductViewModel = SmProductOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}



