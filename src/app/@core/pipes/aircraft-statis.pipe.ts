import { Pipe, PipeTransform } from "@angular/core";
import { EnumViewModel } from "./enum-view.model";

export enum AircraftStatus {
    New = 1,
    Operational = 2,
    Maintenance = 3,
    ForLeaseOrSale = 4,
    Retired = 5
}



export const AircraftStatusOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'New'
    },
    {
        id: 2,
        name: 'Operational'
    },
    {
        id: 3,
        name: 'Maintenance'
    },
    {
        id: 4,
        name: 'For Lease Or Sale'
    },
    {
        id: 5,
        name: 'Retired'
    },
];


@Pipe({ name: 'acstatus' })
export class AcStatusDisplayPipe implements PipeTransform {
    transform(value: number, args?: string[]): string {
        let rv: string = AircraftStatus[value];
        const data: EnumViewModel = AircraftStatusOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}


export enum AircraftActionType {
    AirworthinessCertificate = 1,
    AircraftPurchase = 2,
    CabinConfigurationChange = 3,
    TransferFlight = 4,
    Flight = 5,
    PlaneStand = 6,
    TaxHangarRentWeek = 10,
    TaxAirportWeek = 11,
    InsuranceWeek = 12,
    AircratLeasing = 20,
    Meintenance = 30,
    MaintenanceCheck = 31
}


export const AircraftActionTypeOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Airworthiness Certificate'
    },
    {
        id: 2,
        name: 'Aircraft Purchase'
    },
    {
        id: 3,
        name: 'Cabin Configuration Change'
    },
    {
        id: 4,
        name: 'Transfer Flight'
    },
    {
        id: 5,
        name: 'Flight'
    },
    {
        id: 6,
        name: 'Plane Stand'
    },
    {
        id: 10,
        name: 'Tax Hangar Rent Week'
    },
    {
        id: 11,
        name: 'Tax Airpor Week'
    },
    {
        id: 12,
        name: 'Insurance Week'
    },
    {
        id: 20,
        name: 'Aircrat Leasing'
    },
    {
        id: 30,
        name: 'Meintenance'
    },
    {
        id: 31,
        name: 'Maintenance Check'
    },
];

@Pipe({ name: 'acat' })
export class AircraftActionDisplayPipe implements PipeTransform {
    transform(value: number, args?: string[]): string {
        let rv: string = AircraftActionType[value];
        const data: EnumViewModel = AircraftActionTypeOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}
