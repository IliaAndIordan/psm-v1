import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';
import { SxServicesModule } from './services/sx-services.module';
import { SxSharePipeModule } from '../@share/pipes/pipes.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ToastrModule.forRoot(),

    SxServicesModule,
    SxSharePipeModule,
  ],
  exports:[
    SxServicesModule,
    SxSharePipeModule,
  ]
})
export class PsmCoreModule { }
