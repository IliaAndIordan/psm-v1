
export const PageViewType = {
    Card: 'card',
    List: 'list',
    Table: 'table'
};

export const AppRoutes = {
    Root: '',
    dashboard: 'dashboard',
    //
    public: 'public',
    login: 'login',
    companyset: 'company-set',
    // Company
    company: 'company',
    // PSM Product
    product:'product',
    //Project
    project: 'project',
    //----------
    //SUb Routes
    info: 'info',
    users: 'users',
    profile: 'profile',
    products: 'products',
    projects: 'projects',
    NotAuthorized: 'not-authorized',
    NoImageAvailable: '//images.tradeservice.com/ProductImages/DIR_TSL/TSL_PRODUCT_NOT_AVAILABLE.png',
    Maintenance: 'under-maintenance',
    SessionSuspended: 'session-suspended'
};
