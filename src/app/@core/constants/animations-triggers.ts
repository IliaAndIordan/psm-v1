import { trigger, state, style, transition, animate } from '@angular/animations';
import { Animate } from './animation.const';

export const SideNavChange = 
trigger('onSideNavChange', [
    state('close',
      style({
        'margin-left': '60px'
      })
    ),
    state('open',
      style({
        'margin-left': '250px'
      })
    ),
    transition('close => open', animate('250ms ease-in')),
    transition('open => close', animate('250ms ease-in')),
  ]);
  
export const PageTransition =
    trigger('transition', [

        state('in', style({ transform: 'translateY(0)', opacity: '0' })),

        transition('void => *', [
            style({ transform: 'translateY(-10px)', opacity: '.5' }),
            animate('400ms')
        ])
    ]);


export const ShowHideTriggerBlock =
    trigger('showHideTriggerBlock', [
        state(Animate.show, style({
            opacity: '1',
            display: 'block'
        })),
        state(Animate.hide, style({
            opacity: '0',
            display: 'none'
        })),
        transition(Animate.show + ' <=> ' + Animate.hide, animate('300ms'))
    ])

export const ShowHideTriggerFlex =
    trigger('showHideTriggerFlex', [
        state(Animate.show, style({
            opacity: '1',
            display: 'flex'
        })),
        state(Animate.hide, style({
            opacity: '0',
            display: 'none'
        })),
        transition(Animate.show + ' <=> ' + Animate.hide, animate('300ms'))
    ])

export const SpinExpandIconTrigger =
    trigger('spinIconTrigger', [
        state(Animate.normal, style({
            transform: "rotate(0deg)"
        })),
        state(Animate.turned, style({
            transform: "rotate(90deg)"
        })),
        transition('normal <=> turned', animate('400ms'))
    ]);

export const ShowHideDelayFlexTrigger =
    trigger('showHideDelayFlexTrigger', [
        state(Animate.show, style({
            opacity: '1',
            display: 'flex'
        })),
        state(Animate.hide, style({
            opacity: '0',
            display: 'none'
        })),
        transition(Animate.show + ' => ' + Animate.hide, animate('0ms')),
        transition(Animate.hide + ' => ' + Animate.show, animate('400ms 100ms'))
    ])

export const ShowHideDelayBlockTrigger =
    trigger('showHideDelayBlockTrigger', [
        state(Animate.show, style({
            opacity: '1',
            display: 'block'
        })),
        state(Animate.hide, style({
            opacity: '0',
            display: 'none'
        })),
        transition(Animate.show + ' => ' + Animate.hide, animate('0ms')),
        transition(Animate.hide + ' => ' + Animate.show, animate('400ms 100ms'))
    ])


export const ShowHideListTrigger =
    trigger('showHideListTrigger', [
        state(Animate.show, style({
            opacity: '1',
            position: 'static'
        })),
        state(Animate.hide, style({
            opacity: '0',
            position: 'fixed'
        })),
        transition(Animate.show + ' => ' + Animate.hide, animate('0ms')),
        transition(Animate.hide + ' => ' + Animate.show, animate('400ms 100ms'))
    ])



export const ShowHideCardTrigger =
    trigger('showHideCardTrigger', [
        state(Animate.show, style({
            opacity: '1',
            display: 'flex'
        })),
        state(Animate.hide, style({
            opacity: '0',
            display: 'none'
        })),
        transition(Animate.show + ' => ' + Animate.hide, animate('0ms')),
        transition(Animate.hide + ' => ' + Animate.show, animate('400ms 100ms'))
    ])

//Show Hide Similar Items Cards
export const ShowHideCard =
    trigger('showHideCard', [
        state('show', style({
            opacity: '1'
        })),
        state('hide', style({
            opacity: '0'
        })),
        transition('hide => show', animate('500ms')),
        transition('show => hide', animate('500ms'))
    ])

export const ShowPanelTrigger =
    trigger('ShowPanelTrigger', [
        state('show', style({
            opacity: '1',
            display: 'block'
        })),
        state('hide', style({
            opacity: '0',
            display: 'none'
        })),
        transition('hide => show', animate('300ms')),
        transition('show => hide', animate('300ms'))
    ])



export const ViewMoreCardTrigger =
    trigger('viewMoreCardTrigger', [
        state(Animate.hide, style({
            display: 'none',
            bottom: '-45px',
            opacity: '0'
        })),
        state(Animate.show, style({
            display: 'block',
            bottom: '0px',
            opacity: '1'
        })),
        transition(Animate.hide + '<=>' + Animate.show, animate('200ms')),
        transition(Animate.show + '<=>' + Animate.hide, animate('200ms')),
    ])

export const DelayFadeInTrigger =
    trigger('delayFadeInTrigger', [
        state(Animate.hide, style({
            opacity: '0'
        })),
        state(Animate.show, style({
            opacity: '1'
        })),
        transition(Animate.hide + '<=>' + Animate.show, animate('500ms')),
        transition(Animate.show + '<=>' + Animate.hide, animate('300ms'))
    ])

export const ToggleSearchTrigger =
    trigger('toggleSearchTrigger', [
        state('hide', style({
            position: 'absolute',
            top: '-61px',
            opacity: '0'
        })),
        state('show', style({
            top: '0px',
            display: 'flex',
            opacity: '1'
        })),
        transition('hide => show', animate('300ms')),
        transition('show => hide', animate('300ms'))
    ])

export const ToggleSuggestionTrigger =
    trigger('toggleSuggestionTrigger', [
        state('hide', style({
            display: 'none',
            opacity: '0'
        })),
        state('show', style({
            display: 'flex',
            opacity: '1'
        })),
        transition('hide => show', animate('300ms')),
        transition('show => hide', animate('300ms'))
    ])


export const ExpandTab =
    trigger('expandTab', [
        state(Animate.in, style({
            width: '72px'
        })),
        state(Animate.out, style({
            width: '500px'
        })),
        transition(Animate.in + '=> ' + Animate.out, animate('500ms')),
        transition(Animate.out + ' =>' + Animate.in, animate('500ms'))
    ])

    export const TogleBtnTopRev =
    trigger('togleBtnTopRev', [
        state(Animate.out, style({
            opacity: '1',
            display: 'block'
        })),
        state(Animate.in, style({
            opacity: '0',
            display: 'none'
        })),
        transition(Animate.in + '=> ' + Animate.out, animate('200ms')),
        transition(Animate.out + ' =>' + Animate.in, animate('200ms'))
    ])


    

export const ExpandTrigger = 
trigger('expandTrigger',[
     state('small', style({
       width: '50px'
     })),
     state('large', style({
       width: '240px'
     })),
     transition(Animate.small + ' => ' + Animate.large, animate('200ms')),
     transition(Animate.large + ' => ' + Animate.small,animate('200ms')),
   ])

export const ShowlinksTrigger = 
trigger('showlinksTrigger',[
     state(Animate.hide, style({
       display: 'none',
       color: 'rgba(0,0,0,0.0)',
     })),
     state(Animate.show, style({
       display: 'block',

     })),
     transition(Animate.show + ' => ' + Animate.hide, animate('200ms')),
     transition(Animate.hide + ' => ' + Animate.show, animate('400ms'))
   ])

export const ShowOverlayTrigger = 
trigger('showOverlayTrigger',[
     state('hideOverLay', style({
       display: 'none',
       backgroundColor: 'rgba(0,0,0,0.0)'
     })),
     state('showOverLay', style({
       display: 'block',
       backgroundColor: 'rgba(0,0,0,0.5)'
     })),
     transition('hideOverLay => showOverLay', animate('300ms')),
     transition('showOverLay => hideOverLay', animate('100ms'))
   ])