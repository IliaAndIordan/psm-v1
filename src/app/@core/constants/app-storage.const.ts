import { environment } from 'src/environments/environment';

export const AppStore = {
    BarerToken: 'sx_barer_token_key',
    RefreshToken: 'sx_refresh_token_key',
    CurrentUser: 'sx_current_user_key',
    CurrentUserCompany: 'sx_current_user_company',
    CurrentUserPsmProduct: 'sx_current_user_psm_product',
    CurrentUserSxProduct: 'sx_current_user_sx_product',
    CurrentUserSxProducts: 'sx_current_user_company_sx_products',
    CurrentUserPsmProducts: 'sx_current_user_company_psm_products',
    UserLocation: 'sx_user_location',
    UserSettings: 'sx_user_settings_key',
    
    // ---PROJECT
    project: 'sx_project',
    project_tob_idx:'sx_project_tob_idx',
    psm_company_profile:'psm_company_profile'
};


export const ImgWide = 'image-wide';
export const ImgTall = 'image-tall';

export const MOBILE_MEDIAQUERY = 'screen and (max-width: 599px)';
export const TABLET_MEDIAQUERY = 'screen and (min-width: 600px) and (max-width: 959px)';
export const MONITOR_MEDIAQUERY = 'screen and (min-width: 960px)';

export const URL_COMMON = 'https://common.iordanov.info/';
export const URL_COMMON_IMAGES = URL_COMMON + 'images/';
export const URL_COMMON_IMAGES_COMPANY = URL_COMMON_IMAGES + 'company/';
export const URL_COMMON_IMAGE_SX = URL_COMMON_IMAGES + 'sx/';
export const URL_COMMON_IMAGE_PSM = URL_COMMON_IMAGES + 'psm/';
export const URL_COMMON_IMAGE_FLAGS = URL_COMMON_IMAGES + 'flags/';
export const URL_COMMON_IMAGE_COMMODITY = URL_COMMON_IMAGES + 'commodity/';

export const COMMON_IMG_LOGO_RED = URL_COMMON_IMAGE_SX + 'sx-logo-red-3d.png';
export const COMMON_IMG_LOGO_CERCLE = URL_COMMON_IMAGE_SX + 'sx-logo-red-3d.png';
export const COMMON_IMG_AVATAR = URL_COMMON_IMAGES + 'iziordanov_sd_128_bw.png';
export const COMMON_IMG_LOGO_IZIORDAN = URL_COMMON_IMAGES + 'iziordanov_logo.png';
export const COMMON_IMG_PSM_WHITE = URL_COMMON_IMAGE_PSM + 'psm_white.png';
export const COMMON_IMG_PSM_2COLORS = URL_COMMON_IMAGE_PSM + 'psm_2colors.jpg';
export const COMMON_IMG_HEROJURNEY = URL_COMMON_IMAGE_PSM +'Heroesjourney.png'
export const URL_COMMON_IMAGE_AIRCRAFT= URL_COMMON_IMAGES + 'aircraft/';

export const GMAP_API_KEY = 'AIzaSyDFFY0r9_fLApc2awmt-O1Q2URpkSFbQVc';

export const URL_NO_IMG = URL_COMMON_IMAGE_SX + 'common/noimage.png';
export const URL_NO_IMG_SQ =  URL_COMMON_IMAGE_SX +'common/noimage-sq.jpg';

export const HOUR_MS = (1 * 60 * 60 * 1000);
export const DAY_MS = (24 * HOUR_MS);

export const PAGE_SIZE_OPTIONS = [12, 25, 50, 100, 200];
