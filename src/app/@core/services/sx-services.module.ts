import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PsmClientsModule } from '../clients/clients.module';
import { AppService } from './app.service';
import { SxGuardsModule } from '../guards/sx-guards.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    //
    PsmClientsModule,
    SxGuardsModule,
  ],
  exports:[
    PsmClientsModule,
    SxGuardsModule,
  ],
  providers:[
  ]
})
export class SxServicesModule { }
