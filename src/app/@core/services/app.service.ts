import { Injectable } from '@angular/core';
import { default as projectInfo } from 'package.json';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor() { }

  //#region Application Info
  get title(): string {
    return projectInfo.name;
  }

  get version(): string {
    return projectInfo.version;
  }

  get env(): string {
    return environment.production?'PD':'DV';
  }
 
  //#endregion
}
