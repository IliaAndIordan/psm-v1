import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { PsmClientsModule } from "../clients/clients.module";
import { AuthGuard } from "./auth.guard.service";


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        PsmClientsModule,
    ],
    exports:[
    ],
    providers:[
    ]
  })
  export class SxGuardsModule { }