import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanLoad, Route, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CurrentUserService } from '../clients/current-user.service';
import { AppRoutes } from '../constants/app-routes.const';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(
    private cus: CurrentUserService,
    private router: Router) { }

  /**
   * used to prevent the application to load entire modules lazily if the user is not authorize to do so.
   * @param route 
   */
  canLoad(route: Route) {
    return true;
  }

  /**
   * used to prevent unauthorized users to access certain routes
   * @param next 
   * @param state 
   */
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    if (this.cus.isAuthenticated) {
      if (this.cus.user) {
        return true;
      } else {
        this.router.navigate([AppRoutes.Root, AppRoutes.public]);
        return false;
      }
    }

    this.router.navigate([AppRoutes.Root, AppRoutes.public], { queryParams: { redirectTo: state.url } });
    return false;
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    return this.canActivate(route, state);
  }
}
