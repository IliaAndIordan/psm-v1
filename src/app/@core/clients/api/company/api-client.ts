import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable, of, Subscription } from 'rxjs';
// Services
import { ApiBaseClient } from '../../api-base.client';
import { CurrentUserService } from '../../current-user.service';
// --- Models
import {
    CompanyModel, ResponseCompanySave, ResponseCompanySaveData,
    ResponseCompanyOpt, RespCompanyList,
    RespCompanyListToJoin,
    CompanyProduct
} from './dto';
import { environment } from 'src/environments/environment';
import { catchError, map, tap } from 'rxjs/operators';
import { CompanyStatus } from './enums';
import { RespCompanyUsers, ResponseGetUser, UserModel } from '../auth/dto';

// Models

// import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({ providedIn: 'root' })
export class SxApiCompanyClient extends ApiBaseClient {

    // refreshTockenSubscriver: Subscription;

    constructor(
        private client: HttpClient,
        private cus: CurrentUserService) {
        super();
    }

    private get baseUrl(): string {
        return environment.services.url.company;
    }

    //#region Company

    companyById(id: number): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'company',
            { company_id: id },
            { headers: hdrs }).pipe(
                map((response: ResponseCompanySave) => {
                    // console.log('companyById-> response=', response);
                    const res = Object.assign(new ResponseCompanySave(), response);
                    // console.log('companyById-> success=', res.success);
                    let rv: CompanyModel;
                    if (res && res.success) {
                        rv = CompanyModel.fromSaveModel(response.data);
                        //console.log('companyById-> rv=', rv);
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap Book save event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }

    companySave(value: CompanyModel): Observable<CompanyModel> {
        value.actorId = this.cus.user.userId;
        value.companyStatusId = value.companyId ? CompanyStatus.ChangedNotConfirmed : CompanyStatus.CreatedNotConfirmed;
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'save',
            { company: value },
            { headers: hdrs }).pipe(
                map((response: ResponseCompanySave) => {
                    // console.log('companySave-> response=', response);
                    const res = Object.assign(new ResponseCompanySave(), response);
                    // console.log('companySave-> success=', res.success);
                    let rv: CompanyModel;
                    if (res && res.success) {
                        // console.log('companySave-> res=', res);
                        if (res && res.data && res.data.company) {
                            rv = CompanyModel.fromJSON(res.data.company);
                        }
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap Book save event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }

    companySmProductAdd(companyId: number, smProductId: number): Observable<CompanyModel> {

        console.log('companySmProductAdd-> companyId=', companyId);
        console.log('companySmProductAdd-> smProductId=', smProductId);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'sm_product_add',
            {
                company_id: companyId,
                sm_product_id: smProductId
            },
            { headers: hdrs }).pipe(
                map((resp: ResponseCompanySave) => {
                    console.log('companySmProductAdd -> response=', resp);
                    const res = Object.assign(new ResponseCompanySave(), resp);
                    const company = CompanyModel.fromSaveModel(res.data);
                    return company;
                }),
                tap(event => {
                    this.log(`tap companySmProductAdd event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    companySmProductDelete(companyId: number, smCompanyProductId: number): Observable<CompanyModel> {

        console.log('companySmProductDelete-> companyId=', companyId);
        console.log('companySmProductDelete-> smCompanyProductId=', smCompanyProductId);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'sm_product_del',
            {
                company_id: companyId,
                sm_company_product_id: smCompanyProductId
            },
            { headers: hdrs }).pipe(
                map((resp: ResponseCompanySave) => {
                    console.log('companySmProductDelete -> response=', resp);
                    const res = Object.assign(new ResponseCompanySave(), resp);
                    const company = CompanyModel.fromSaveModel(res.data);
                    return company;
                }),
                tap(event => {
                    this.log(`tap companyProductAdd event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    liveryUpdate(id: number): Observable<any> {
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'company_livery_update',
            { company_id: id },
            { headers: hdrs }).pipe(
                map((response: ResponseCompanySave) => {
                    // console.log('liveryUpdate-> response=', response);
                    const res = Object.assign(new ResponseCompanySave(), response);
                    // console.log('liveryUpdate-> success=', res.success);
                    let rv: CompanyModel;
                    if (res && res.success) {
                        // console.log('liveryUpdate-> res=', res);
                        if (res && res.data && res.data.company) {
                            rv = CompanyModel.fromJSON(res.data.company);
                        }
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap liveryUpdate event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    companyOpt(id: number): Observable<Array<CompanyModel>> {
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'company_options',
            undefined,
            { headers: hdrs }).pipe(
                map((resp: ResponseCompanyOpt) => {
                    console.log('companyOpt-> resp=', resp);
                    const res = Object.assign(new ResponseCompanyOpt(), resp);
                    const rv: Array<CompanyModel> = new Array<CompanyModel>();
                    if (res && res.success) {

                        if (res && res.data &&
                            res.data.company_opt && res.data.company_opt.length > 0) {
                            res.data.company_opt.forEach(iu => {
                                const company = CompanyModel.fromJSONOpt(iu);
                                rv.push(company);
                            });
                        }
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap companyOpt event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }

    ListToJoin(): Observable<RespCompanyListToJoin> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.get(this.baseUrl + 'list_to_join', { headers: hdrs })
            .pipe(
                map((resp: RespCompanyListToJoin) => {
                    //console.log('ListToJoin -> response=', resp);
                    const res = Object.assign(new RespCompanyListToJoin(), resp);
                    return resp;
                }),
                tap(event => {
                    this.log(`tap ListToJoin event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    companyListTable(
        limit: number = 25,
        offset: number = 0,
        orderCol: string = 'user_name',
        isDesc: boolean = false,
        filter?: string): Observable<RespCompanyList> {

        // console.log('companyUsersTable-> offset=', offset);
        // console.log('companyUsersTable-> limit=', limit);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'company_list',
            {
                qry_offset: offset,
                qry_limit: limit,
                qry_orderCol: orderCol,
                qry_isDesc: isDesc,
                qry_filter: filter,
            },
            { headers: hdrs }).pipe(
                map((resp: RespCompanyList) => {
                    // console.log('companyListTable -> response=', resp);
                    const res = Object.assign(new RespCompanyList(), resp);
                    return resp;
                }),
                tap(event => {
                    this.log(`tap companyListTable event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }


    //#endregion

    //#region User
/*
    userSave(value: UserModel): Observable<ResponseGetUser> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'user_save',
            { user: value },
            { headers: hdrs }).pipe(
                map((response: ResponseGetUser) => {
                    console.log('userSave-> response=', response);
                    const res = Object.assign(new ResponseGetUser(), response);
                    // console.log('companySave-> success=', res.success);
                    return res;
                }),
                tap(event => {
                    this.log(`tap userSave event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }
*/
    //#endregion

    //#region  Company Users

    companyUsers(id: number): Observable<RespCompanyUsers> {
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'company_users',
            { company_id: id },
            { headers: hdrs }).pipe(
                map((resp: RespCompanyUsers) => {
                    // console.log('companyUsers-> response=', resp);
                    const res = Object.assign(new RespCompanyUsers(), resp);
                    return resp;
                }),
                tap(event => {
                    this.log(`tap liveryUpdate event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    companyUsersTable(
        com_id: number,
        limit: number = 25,
        offset: number = 0,
        orderCol: string = 'user_name',
        isDesc: boolean = false,
        filter?: string): Observable<RespCompanyUsers> {

        // console.log('companyUsersTable-> offset=', offset);
        // console.log('companyUsersTable-> limit=', limit);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'company_users_table',
            {
                company_id: com_id,
                qry_offset: offset,
                qry_limit: limit,
                qry_orderCol: orderCol,
                qry_isDesc: isDesc,
                qry_filter: filter,
            },
            { headers: hdrs }).pipe(
                map((resp: RespCompanyUsers) => {
                    // console.log('companyUsersTable-> response=', resp);
                    const res = Object.assign(new RespCompanyUsers(), resp);
                    return resp;
                }),
                tap(event => {
                    this.log(`tap liveryUpdate event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }


    companyUserList(id: number): Observable<Array<UserModel>> {
        return new Observable<Array<UserModel>>(subscriber => {
            this.companyUsers(id)
                .subscribe((resp: RespCompanyUsers) => {
                    const res = Object.assign(new RespCompanyUsers(), resp);
                    // console.log('companyUserList-> res=', res);
                    const rv: Array<UserModel> = new Array<UserModel>();
                    if (res && res.success) {

                        if (res && res.data &&
                            res.data.users && res.data.users.length > 0) {
                            res.data.users.forEach(iu => {
                                //const user = UserModel.fromTimestampJSON(iu);
                                //rv.push(user);
                            });
                        }
                    }
                    // console.log('companyUsers-> rv=', rv);
                    subscriber.next(rv);
                });
        });

    }

    //#endregion


}
