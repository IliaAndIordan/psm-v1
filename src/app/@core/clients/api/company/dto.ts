import { URL_NO_IMG, URL_COMMON_IMAGES_COMPANY } from "src/app/@core/constants/app-storage.const";
import { SmProductOpt, SmProductViewModel } from "src/app/@core/pipes/sx-product.pipe";
import { DateModel, IDateModel } from "../../models/date.model";
import { ResponseModel } from "../../models/responce.model";
import { IUserModel, IUserTimestampModel } from "../auth/dto";
import { CompanyStatus, CompanyType } from "./enums";


export class CompanyModel {

    public actorId: number;
    public adate: Date;
    public branch: string;
    public companyId: number;
    public companyStatusId: CompanyStatus;
    public countryId: number;
    public eanMfrCode: string;
    public logoUrl: string;
    public name: string;
    public notes: string;
    public parentCompanyId: number;
    public typeId: CompanyType;
    public udate: Date;
    public webUrl: string;
    public iso2: string;
    public countryName: string;

    public productsCount: number;
    public usersCount: number;
    public sxLiveryUrl: string;
    public sxLogoUrl: string;
    public pmsProductCount:number;
    public projectsCount:number;

    public sxProducts: CompanyProduct[];

    public static fromJSON(json: ICompanyModel): CompanyModel {
        const vs = Object.create(CompanyModel.prototype);
        return Object.assign(vs, json, {
            logoUrl: json.logoUrl ? json.logoUrl : URL_NO_IMG,
            //sxLiveryUrl: json.sx_livery_url ? json.sx_livery_url : URL_NO_IMG,
            webUrl: json.webUrl && json.webUrl.length > 2 ? json.webUrl : undefined,
            typeId: json.typeId ? json.typeId as CompanyType : CompanyType.Contractor,
            companyStatusId: json.companyStatusId ? json.companyStatusId as CompanyStatus : CompanyStatus.CreatedNotConfirmed,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
            sxLogoUrl: URL_COMMON_IMAGES_COMPANY + 'logo_' + json.companyId.toString() + '.png',
            sxLiveryUrl: URL_COMMON_IMAGES_COMPANY + 'livery_' + json.companyId.toString() + '.png',
        });
    }



    public static fromJSONOpt(json: ICompanyOptModel): CompanyModel {
        const vs = Object.create(CompanyModel.prototype);
        return Object.assign(vs, json, {
            logoUrl: json.logo_url && json.logo_url.length > 2 ? json.logo_url : undefined,
            sxLogoUrl: URL_COMMON_IMAGES_COMPANY + 'logo_' + json.company_id.toString() + '.png?' + new Date().getTime(),
            sxLiveryUrl: URL_COMMON_IMAGES_COMPANY + 'livery_' + json.company_id.toString() + '.png?' + new Date().getTime(),
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? CompanyModel.fromJSON(value) : value;
    }

    public toJSON(): ICompanyModel {
        const vs = Object.create(CompanyModel.prototype);
        return Object.assign(vs, this);
    }

    public static fromSaveModel(resp: IResponseCompanySaveData): CompanyModel {
        let rv: CompanyModel;
        if (resp) {
            rv = CompanyModel.fromJSON(resp.company);
            rv.sxProducts = new Array<CompanyProduct>();
            if (resp.sxProducts && resp.sxProducts.length > 0) {
                resp.sxProducts.forEach(cpi => {
                    const cp = CompanyProduct.fromJSON(cpi);
                    rv.sxProducts.push(cp);
                });
            }
           
        }
        return rv;
    }
}

export interface ICompanyModel {
    actorId: number;
    adate: string;
    branch: string;
    companyId: number;
    companyStatusId: number;
    countryId: number;
    eanMfrCode: string;
    logoUrl: string;
    name: string;
    notes: string;
    parentCompanyId: number;
    typeId: number;
    udate: string;
    webUrl: string;
    iso2: string;
    countryName: string;
    pmsProductCount:number;
    projectsCount:number;

}

export class CompanyProduct {
    public adate: Date;;
    public companyId: number;
    public cpId: number;
    public productId: number;
    public product: SmProductViewModel;

    public static fromJSON(json: ICompanyProduct): CompanyProduct {
        const vs = Object.create(CompanyProduct.prototype);
        return Object.assign(vs, json, {
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            product: (json && json.productId) ? SmProductOpt.find(x => x.id === json.productId) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? CompanyModel.fromJSON(value) : value;
    }

    public toJSON(): ICompanyModel {
        const vs = Object.create(CompanyModel.prototype);
        return Object.assign(vs, this);
    }
}

export interface ICompanyProduct {
    adate: string;
    companyId: number;
    cpId: number;
    productId: number;
}

export interface IResponseCompanySaveData {
    company: ICompanyModel;
    sxProducts: ICompanyProduct[];
}
export class ResponseCompanySaveData {
    company: ICompanyModel;
    sxProducts: ICompanyProduct[];
}


export class ResponseCompanySave extends ResponseModel {
    data: ResponseCompanySaveData;
}

export interface ICompanyOptModel {
    company_id: number;
    company_name: string;
    company_type_id: number;
    branch_code: string;
    logo_url: string;
    country_id: number;
}

export class ResponseCompanyOptData {
    company_opt: ICompanyOptModel[];

}

export class RespCompanyListToJoinData {
    companies: ICompanyModel[];
    countTotal: number;
}

export class RespCompanyListToJoin extends ResponseModel {
    data: RespCompanyListToJoinData;
}

export class ResponseCompanyOpt extends ResponseModel {
    data: ResponseCompanyOptData;
}



export class RespCompanyListData {
    company_list: ICompanyModel[];
    totals: {
        total_rows: number;
    };
}

export class RespCompanyList extends ResponseModel {
    data: RespCompanyListData;
}

