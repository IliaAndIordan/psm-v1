import { URL_COMMON_IMAGE_FLAGS } from "src/app/@core/constants/app-storage.const";
import { ResponseModel } from "../../models/responce.model";


export class CountryModel {
    id: number;
    name: string;
    iso2: string;
    region: string;
    subregion: string;
    regionId: number;
    subregionId: number;

    flagUrl: string;


    public static fromJSON(json: ICountryModel): CountryModel {
        const vs = Object.create(CountryModel.prototype);
        return Object.assign(vs, json, {
            flagUrl: URL_COMMON_IMAGE_FLAGS + json.iso2.toLowerCase() + '.png',
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? CountryModel.fromJSON(value) : value;
    }

    public toJSON(): ICountryModel {
        const vs = Object.create(CountryModel.prototype);
        return Object.assign(vs, this);
    }
}

export interface ICountryModel {
    Id: number;
    name: string;
    iso2: string;
    region: string;
    subregion: string;
    regionId: number;
    subregionId: number;
}

export class DataCountryListModel {
    countries: Array<ICountryModel>;
}

export class ResponseCountryListModel extends ResponseModel {
    data: DataCountryListModel;
}

