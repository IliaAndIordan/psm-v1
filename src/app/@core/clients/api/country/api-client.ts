import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Subject, Observable, of, BehaviorSubject } from 'rxjs';
// Services

// --- Models
import { CountryModel, ICountryModel, ResponseCountryListModel } from './dto';
import { ApiBaseClient } from '../../api-base.client';
import { CurrentUserService } from '../../current-user.service';
import { environment } from 'src/environments/environment';
import { catchError, map } from 'rxjs/operators';


// import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({ providedIn: 'root' })
export class SxApiCountryClient extends ApiBaseClient {

    protected countryListObs: Observable<any>;
    protected countryListData: Array<CountryModel>;
    protected countryListTime: Date;

    constructor(
        private client: HttpClient,
        private cus: CurrentUserService) {
        super();
    }

    private get baseUrl(): string {
        return environment.services.url.auth;
    }

    //#region Company



    //#endregion

    //#region  CASHE Country List

    get countryList(): Observable<Array<CountryModel>> {

        const data = this.countryListData;
        //console.log('countryList -> data:', data);
        if (data && data.length > 0) {
            return of(data);
        } else {

            return new Observable<Array<CountryModel>>(subscriber => {
                this.postCountryList()
                    .subscribe((resp: Array<CountryModel>) => {
                        this.countryListData = resp;
                        this.countryListTime = new Date();
                        subscriber.next(resp);
                    });
            });

        }

    }


    postCountryList(): Observable<any> {
        // const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
        const url = this.baseUrl + 'country_list';
        const req = { user_id: this.cus.user.userId };
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                observe: 'response',
            }),
        };
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
        return this.client.post<HttpResponse<any>>(url, req, { headers: hdrs, observe: 'response' })
            .pipe(
                map((response: HttpResponse<any>) => {
                    // console.log('loadCountryList-> response=', response);
                    let rv: Array<CountryModel>;
                    if (response.body) {
                        const res = Object.assign(new ResponseCountryListModel(), response.body);
                        // console.log('loadCountryList-> res=', res);
                        if (res && res.data && res.data.countries && res.data.countries.length > 0) {
                            this.countryListData = new Array<CountryModel>();
                            res.data.countries.forEach((element: ICountryModel) => {
                                const tmp: CountryModel = CountryModel.fromJSON(element);
                                this.countryListData.push(tmp);
                            });
                            // console.log('loadCountryList ->  countryListData', this.countryListData);
                            this.countryListTime = new Date();
                            rv = this.countryListData;
                        }

                    }
                    return rv;
                }),
                catchError(this.handleError)
            );
    }


    //#endregion
}
