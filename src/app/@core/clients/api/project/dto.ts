import { ResponseModel } from '../../models/responce.model';
import { RowsCountModel, TableCriteriaBase } from '../../models/table-criteria-base.model';
import { SxProjectStatus } from './enums';

//#region PsmProject

export class PsmProject {
    public projectId: number;
    public name: string;
    public statusId: SxProjectStatus;
    public companyId: number;
    public productId?: number;
    public accountKey: string;

    public managerId: number;
    public managerName?: string;
    public managerEmail?: string;

    public estimatorId: number;
    public estimatorName?: string;
    public estimatorEmail?: string;

    public notes: string;

    public adate: Date;
    public udate: Date;
    public dayStart: Date;
    public dayEnd: Date;


    public static fromJSON(json: IPsmProject): PsmProject {
        const vs = Object.create(PsmProject.prototype);
        return Object.assign(vs, json, {
            statusId: json.statusId ? json.statusId as SxProjectStatus : SxProjectStatus.Backlog,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
            dayStart: (json && json.dayStart) ? new Date(json.dayStart) : undefined,
            dayEnd: (json && json.dayEnd) ? new Date(json.dayEnd) : undefined
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? PsmProject.fromJSON(value) : value;
    }

    public toJSON(): IPsmProject {
        const vs = Object.create(PsmProject.prototype);
        return Object.assign(vs, this);
    }

    public static fromSaveModel(resp: ResponsePsmProjectSaveData): PsmProject {
        let rv: PsmProject;
        if (resp) {
            rv = PsmProject.fromJSON(resp.project);
        }
        return rv;
    }
}

export interface IPsmProject {
    projectId: number;
    name: string;
    statusId: number;
    companyId: number;
    productId: number;
    accountKey: string;

    managerId: number;
    managerName: string;
    managerEmail: string;

    estimatorId: number;
    estimatorName: string;
    estimatorEmail: string;


    notes: string;

    adate: string;
    udate: string;
    dayStart: string;
    dayEnd: string;
}

export class IResponsePsmProjectData {
    project: IPsmProject;

}


export class ResponsePsmProject extends ResponseModel {
    data: IResponsePsmProjectData;
}

export class ResponsePsmProjectSaveData {
    project: IPsmProject;

}

export class ResponsePsmProjectSave extends ResponseModel {
    data: ResponsePsmProjectSaveData;
}


export class PsmProjectTableCriteria extends TableCriteriaBase {
    productId?: number;
    managerId?: number;
    statusId?: number;
    companyId?: number;
}

export class PsmProjectTableData {
    projects: IPsmProject[];
    rowsCount: RowsCountModel;
}

export class ResponsePsmProjectTable extends ResponseModel {
    data: PsmProjectTableData;
}

//#endregion

//#region SxProjectModel

export class SxUseCaseModel {
    public usecase_id: number;
    public project_id: number;
    public project_name: string;
    public project_account_id: string;

    public usecase_name: string;
    public usecase_order: number;
    public pstatus_id: SxProjectStatus;

    public usecase_notes: string;
    public usecase_img_url: string;

    public adate: Date;
    public user_id: number;
    public user_name: string;
    public e_mail: string;
    public udate: Date;


    public static fromJSON(json: ISxUseCaseModel): SxUseCaseModel {
        const vs = Object.create(SxUseCaseModel.prototype);
        return Object.assign(vs, json, {
            pstatus_id: json.pstatus_id ? json.pstatus_id as SxProjectStatus : SxProjectStatus.Backlog,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? SxUseCaseModel.fromJSON(value) : value;
    }

    public toJSON(): ISxUseCaseModel {
        const vs = Object.create(SxUseCaseModel.prototype);
        return Object.assign(vs, this);
    }
}

export interface ISxUseCaseModel {
    usecase_id: number;
    project_id: number;
    project_name: string;
    project_account_id: string;

    usecase_name: string;
    usecase_order: number;
    pstatus_id: number;

    usecase_notes: string;
    usecase_img_url: string;

    adate: Date;
    user_id: number;
    user_name: string;
    e_mail: string;
    udate: Date;
}

export class ResponseSxUseCasetData {
    usecase: ISxUseCaseModel;

}


export class ResponseSxUseCase extends ResponseModel {
    data: ResponseSxUseCasetData;
}

export class ResponseSxUseCaseSaveData {
    usecase: ISxUseCaseModel[];

}

export class ResponseSxUseCasetSave extends ResponseModel {
    data: ResponseSxUseCaseSaveData;
}


export class ResponseSxUseCaseTableData {
    usecase_table: ISxUseCaseModel[];
    totals: {
        total_rows: number;
    };
}

export class ResponseSxUseCaseTable extends ResponseModel {
    data: ResponseSxUseCaseTableData;
}

//#endregion
