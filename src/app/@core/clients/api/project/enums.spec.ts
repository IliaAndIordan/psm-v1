import { EnumViewModel } from "../../models/enum-view.model";
import { SxProjectStatus, SxProjectStatusDisplayPipe, SxProjectStatusOpt } from "./enums";

describe('SxProjectStatusDisplayPipe', () => {
    // This pipe is a pure, stateless function so no need for BeforeEach
    const pipe = new SxProjectStatusDisplayPipe();
  
    it('transforms "2" to "Active"', () => {
        const data: EnumViewModel = SxProjectStatusOpt.find(x => x.id === SxProjectStatus.Active);
      expect(pipe.transform(SxProjectStatus.Active)).toBe(data.name);
    });
  
  
    // ... more tests ...
  });