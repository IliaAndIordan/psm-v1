import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable, of, Subscription } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
// Services
import { ApiBaseClient } from '../../api-base.client';
import { CurrentUserService } from '../../current-user.service';
// --- Models
import {
    ResponsePsmProject, ResponsePsmProjectSave, ResponsePsmProjectTable,
    ResponseSxUseCaseTable, ResponseSxUseCasetSave, PsmProject,
    SxUseCaseModel,
    PsmProjectTableCriteria
} from './dto';
import { environment } from 'src/environments/environment';


// Models

// import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({ providedIn: 'root' })
export class ApiPsmProjectClient extends ApiBaseClient {

    constructor(
        private client: HttpClient,
        private cus: CurrentUserService) {
        super();
    }

    private get projectUrl(): string {
        return environment.services.url.project;
    }

    private get baseUrl(): string {
        return environment.services.url.company;
    }

    //#region PsmProject

    psmProjectById(id: number): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'psm_project_get',
            { projectId: id },
            { headers: hdrs }).pipe(
                map((response: ResponsePsmProjectSave) => {
                    // console.log('companyById-> response=', response);
                    const res = Object.assign(new ResponsePsmProjectSave(), response);
                    // console.log('companyById-> success=', res.success);
                    let rv: PsmProject;
                    if (res && res.success) {
                        rv = PsmProject.fromSaveModel(response.data);
                        //console.log('companyById-> rv=', rv);
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }

    psmProjectSave(value: PsmProject): Observable<PsmProject> {
        //value.actorId = this.cus.user.userId;
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'psm_project_save',
            { project: value },
            { headers: hdrs }).pipe(
                map((response: ResponsePsmProjectSave) => {
                    // console.log('companySave-> response=', response);
                    const res = Object.assign(new ResponsePsmProjectSave(), response);
                    // console.log('companySave-> success=', res.success);
                    let rv: PsmProject;
                    if (res && res.success) {
                        // console.log('companySave-> res=', res);
                        if (res && res.data && res.data.project) {
                            rv = PsmProject.fromSaveModel(res.data);
                        }
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap Book save event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }

    psmProjectTable(criteria: PsmProjectTableCriteria): Observable<ResponsePsmProjectTable> {

        //console.log('macTable-> criteria=', criteria);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'psm_project_table',
            {

                qry_offset: criteria.offset,
                qry_limit: criteria.limit,
                qry_orderCol: criteria.sortCol,
                qry_isDesc: criteria.sortDesc,
                qry_filter: criteria.filter,
                companyId: criteria.companyId,
                productId: criteria.productId,
                managerId: criteria.managerId,
                statusId: criteria.statusId,
            },
            { headers: hdrs }).pipe(
                map((resp: ResponsePsmProjectTable) => {
                    //console.log('psmProductTable -> resp=', resp);
                    const res = Object.assign(new ResponsePsmProjectTable(), resp);
                    //console.log('aircraftTable -> response=', res);
                    return res;
                }),
                tap(event => {
                    this.log(`tap psmProductTable event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    //#endregion

    //#endregion

    //#region UseCase

    usecaseSave(value: SxUseCaseModel): Observable<SxUseCaseModel> {


        // value.actor_id = this.cus.user.user_id;
        console.log('usecaseSave-> value=', value);
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.projectUrl + 'usecase_save',
            { usecase: value },
            { headers: hdrs }).pipe(
                map((response: ResponseSxUseCasetSave) => {
                    console.log('usecaseSave-> response=', response);
                    const res = Object.assign(new ResponseSxUseCasetSave(), response);
                    // console.log('companySave-> success=', res.success);
                    let rv: SxUseCaseModel;
                    if (res && res.success) {
                        // console.log('companySave-> res=', res);
                        if (res && res.data && res.data.usecase) {
                            rv = SxUseCaseModel.fromJSON(res.data.usecase[0]);
                        }
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap Project save event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }


    usecaseTable(
        limit: number = 25,
        offset: number = 0,
        orderCol: string = 'usecase_order',
        isDesc: boolean = false,
        projectId?: number,
        filter?: string): Observable<ResponseSxUseCaseTable> {

        console.log('usecaseTable-> offset=', offset);
        console.log('usecaseTable-> limit=', limit);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.projectUrl + 'usecase_table',
            {
                project_id: projectId,
                qry_offset: offset,
                qry_limit: limit,
                qry_orderCol: orderCol,
                qry_isDesc: isDesc,
                qry_filter: filter,
            },
            { headers: hdrs }).pipe(
                map((resp: ResponseSxUseCaseTable) => {
                    const res = Object.assign(new ResponseSxUseCaseTable(), resp);
                    console.log('usecaseTable -> response=', res);
                    return res;
                }),
                tap(event => {
                    this.log(`tap usecaseTable event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    //#endregion


}
