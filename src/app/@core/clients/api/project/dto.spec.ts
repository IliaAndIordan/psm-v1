import { TestBed } from '@angular/core/testing';
import { IPsmProject, ISxUseCaseModel, PsmProject, SxUseCaseModel } from './dto';
import { SxProjectStatus } from './enums';


describe('SxProjectModel', () => {
  let dto: PsmProject;
  let dateJson: IPsmProject = {
    projectId: 1,
    name: 'test',
    statusId: SxProjectStatus.Active,
    companyId: 25,
    productId:undefined,
    accountKey: undefined,

    estimatorId:undefined,
    estimatorName: undefined,
    estimatorEmail: undefined,

    managerId: undefined,
    managerName: undefined,
    managerEmail: undefined,

    notes: undefined,

    adate: undefined,
    udate: undefined,
    dayStart: undefined,
    dayEnd: undefined
  };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    dto = Object.assign(new PsmProject(), dateJson);
  });

  it('should be created', () => {
    expect(dto).toBeTruthy();
  });

  it('#fromJSON should return real value', () => {
    dto = PsmProject.fromJSON(dateJson);
    expect(dto).toBeTruthy();
    dateJson.adate = '2020-12-23 22:19:35';
    dateJson.udate = '2020-12-23 22:19:35';
    dateJson.dayStart = '2020-12-23 22:19:35';
    dateJson.dayEnd = '2020-12-23 22:19:35';
    dateJson.statusId = undefined;
    dto = PsmProject.fromJSON(dateJson);
    expect(dto).toBeTruthy();
  });

  it('#toJSON should ve called', () => {
    dto = PsmProject.fromJSON(dateJson);
    expect(dto).toBeTruthy();
    const dtoJson = dto.toJSON();
    expect(dtoJson).toBeTruthy();
  });
});


describe('SxUseCaseModel ', () => {
  let dto: SxUseCaseModel ;
  let dateJson:ISxUseCaseModel  = {
    usecase_id: 2,
    project_id: 1,
    project_name: 'test',
    project_account_id: undefined,

    usecase_name: 'usecase_name',
    usecase_order: 1,
    pstatus_id: undefined,

    usecase_notes: undefined,
    usecase_img_url: undefined,

    adate: undefined,
    user_id: 1,
    user_name: undefined,
    e_mail: undefined,
    udate: undefined,
  };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    dto = Object.assign(new SxUseCaseModel(), dateJson);
  });

  it('should be created', () => {
    expect(dto).toBeTruthy();
  });

  it('#fromJSON should return real value', () => {
    dto = SxUseCaseModel.fromJSON(dateJson);
    expect(dto).toBeTruthy();
  });

  it('#toJSON should ve called', () => {
    dto = SxUseCaseModel.fromJSON(dateJson);
    expect(dto).toBeTruthy();
    const dtoJson = dto.toJSON();
    expect(dtoJson).toBeTruthy();
  });
});
