import { ResponseModel } from "../../models/responce.model";
import { RowsCountModel, TableCriteriaBase } from "../../models/table-criteria-base.model";
import { PsmModuleType } from "./enums";

//#region PsmProduct

export class PsmUseCase {
    public ucId: number;
    public name: string;
    public ucOrder: number;
    public notes: string;
    public imageUrl: string;
    public statusId: number;
    public psmProductId: number;
    public parentUcId: number;
    public actorId: number;
    public adate: Date;
    public udate: Date;
    public companyId?: number;
    public productName?: string;
    public productKey?: string;
    public actorName?: string;
    public actorEmail?: string;
    

    

    public static fromJSON(json: IPsmUseCase): PsmUseCase {
        const vs = Object.create(PsmUseCase.prototype);
        return Object.assign(vs, json, {
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? PsmUseCase.fromJSON(value) : value;
    }

    public toJSON(): IPsmUseCase {
        const vs = Object.create(PsmUseCase.prototype);
        return Object.assign(vs, this);
    }

    public static fromSaveModel(resp: IResponsePsmUseCaseSaveData): PsmUseCase {
        let rv: PsmUseCase;
        if (resp) {
            rv = PsmUseCase.fromJSON(resp.usecase);
        }
        return rv;
    }
}

export interface IPsmUseCase {
    ucId: number;
    name: string;
    ucOrder: number;
    notes: string;
    imageUrl: string;
    statusId: number;
    psmProductId: number;
    parentUcId: number;
    actorId: number;
    adate: Date;
    udate: Date;
    companyId?: number;
    productName?: string;
    productKey?: string;
    actorName?: string;
    actorEmail?: string;
}


export interface IResponsePsmUseCaseSaveData {
    usecase: IPsmUseCase;
}
export class ResponsePsmUseCaseSaveData {
    usecase: IPsmUseCase;
}


export class ResponsePsmUseCaseSave extends ResponseModel {
    data: ResponsePsmUseCaseSaveData;
}


export class PsmUseCaseTableCriteria extends TableCriteriaBase {
    companyId?: number;
    actorId?: number;
    statusId?: number;
    psmProductId?: number;
    parentUcId?: number;
}

export class PsmProductTableData {
    usecases: IPsmUseCase[];
    rowsCount: RowsCountModel;
    
}

export class ResponsePsmProductTable extends ResponseModel {
    data: PsmProductTableData;
}

//#endregion


