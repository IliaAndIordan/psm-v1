import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { catchError, map, tap } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { ApiBaseClient } from "../../api-base.client";
import { CurrentUserService } from "../../current-user.service";
import { PsmModule, PsmModuleTableCriteria, PsmProduct, PsmProductTableCriteria, PsmProductTableData, ResponsePsmModuleSave, ResponsePsmModuleTable, ResponsePsmProductSave, ResponsePsmProductTable } from "./dto";

@Injectable({ providedIn: 'root' })
export class ApiPsmProductClient extends ApiBaseClient {

    constructor(
        private client: HttpClient,
        private cus: CurrentUserService) {
        super();
    }

    private get baseUrl(): string {
        return environment.services.url.company;
    }

    //#region PsmProduct

    psmProductById(id: number): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'psm_product_get',
            { psmProduct: id },
            { headers: hdrs }).pipe(
                map((response: ResponsePsmProductSave) => {
                    // console.log('companyById-> response=', response);
                    const res = Object.assign(new ResponsePsmProductSave(), response);
                    // console.log('companyById-> success=', res.success);
                    let rv: PsmProduct;
                    if (res && res.success) {
                        rv = PsmProduct.fromSaveModel(response.data);
                        //console.log('companyById-> rv=', rv);
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }

    psmProductSave(value: PsmProduct): Observable<PsmProduct> {
        value.actorId = this.cus.user.userId;
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'psm_product_save',
            { psmProduct: value },
            { headers: hdrs }).pipe(
                map((response: ResponsePsmProductSave) => {
                    // console.log('companySave-> response=', response);
                    const res = Object.assign(new ResponsePsmProductSave(), response);
                    // console.log('companySave-> success=', res.success);
                    let rv: PsmProduct;
                    if (res && res.success) {
                        // console.log('companySave-> res=', res);
                        if (res && res.data && res.data.psmProduct) {
                            rv = PsmProduct.fromJSON(res.data.psmProduct);
                        }
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap Book save event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }

    psmProductTable(criteria: PsmProductTableCriteria): Observable<ResponsePsmProductTable> {

        //console.log('macTable-> criteria=', criteria);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'psm_product_table',
            {

                qry_offset: criteria.offset,
                qry_limit: criteria.limit,
                qry_orderCol: criteria.sortCol,
                qry_isDesc: criteria.sortDesc,
                qry_filter: criteria.filter,
                companyId: criteria.companyId,
                actorId: criteria.actorId,
            },
            { headers: hdrs }).pipe(
                map((resp: ResponsePsmProductTable) => {
                    //console.log('psmProductTable -> resp=', resp);
                    const res = Object.assign(new ResponsePsmProductTable(), resp);
                    //console.log('aircraftTable -> response=', res);
                    return res;
                }),
                tap(event => {
                    this.log(`tap psmProductTable event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    //#endregion

    //#region PsmModule

    psmModuleById(id: number): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'psm_module_get',
            { moduleId: id },
            { headers: hdrs }).pipe(
                map((response: ResponsePsmModuleSave) => {
                    // console.log('companyById-> response=', response);
                    const res = Object.assign(new ResponsePsmModuleSave(), response);
                    // console.log('companyById-> success=', res.success);
                    let rv: PsmModule;
                    if (res && res.success) {
                        rv = PsmModule.fromSaveModel(response.data);
                        //console.log('companyById-> rv=', rv);
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }

    psmModuleSave(value: PsmModule): Observable<PsmModule> {
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'psm_module_save',
            { psmModule: value },
            { headers: hdrs }).pipe(
                map((response: ResponsePsmModuleSave) => {
                    // console.log('companySave-> response=', response);
                    const res = Object.assign(new ResponsePsmModuleSave(), response);
                    // console.log('companySave-> success=', res.success);
                    let rv: PsmModule;
                    if (res && res.success) {
                        // console.log('companySave-> res=', res);
                        if (res && res.data && res.data.psmModule) {
                            rv = PsmModule.fromJSON(res.data.psmModule);
                        }
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap Book save event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }

    psmModuleTable(criteria: PsmModuleTableCriteria): Observable<ResponsePsmModuleTable> {

        //console.log('macTable-> criteria=', criteria);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'psm_module_table',
            {

                qry_offset: criteria.offset,
                qry_limit: criteria.limit,
                qry_orderCol: criteria.sortCol,
                qry_isDesc: criteria.sortDesc,
                qry_filter: criteria.filter,
                psmProductId: criteria.psmProductId,
                companyId: criteria.companyId,
                moduleTypeId: criteria.moduleTypeId,
            },
            { headers: hdrs }).pipe(
                map((resp: ResponsePsmModuleTable) => {
                    //console.log('psmProductTable -> resp=', resp);
                    const res = Object.assign(new ResponsePsmModuleTable(), resp);
                    //console.log('aircraftTable -> response=', res);
                    return res;
                }),
                tap(event => {
                    this.log(`tap psmProductTable event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    //#endregion

    //#region PsmProductModule

    psmConnectedModuleById(id: number): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'psm_connected_module_get',
            { moduleId: id },
            { headers: hdrs }).pipe(
                map((response: ResponsePsmModuleSave) => {
                    // console.log('companyById-> response=', response);
                    const res = Object.assign(new ResponsePsmModuleSave(), response);
                    // console.log('companyById-> success=', res.success);
                    let rv: PsmModule;
                    if (res && res.success) {
                        rv = PsmModule.fromSaveModel(response.data);
                        //console.log('companyById-> rv=', rv);
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }

    psmConnectedModuleSave(value: PsmModule): Observable<PsmModule> {
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'psm_connected_module_save',
            { psmModule: value },
            { headers: hdrs }).pipe(
                map((response: ResponsePsmModuleSave) => {
                    // console.log('companySave-> response=', response);
                    const res = Object.assign(new ResponsePsmModuleSave(), response);
                    // console.log('companySave-> success=', res.success);
                    let rv: PsmModule;
                    if (res && res.success) {
                        // console.log('companySave-> res=', res);
                        if (res && res.data && res.data.psmModule) {
                            rv = PsmModule.fromJSON(res.data.psmModule);
                        }
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap Book save event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }

    psmConnectedModuleTable(criteria: PsmModuleTableCriteria): Observable<ResponsePsmModuleTable> {

        //console.log('macTable-> criteria=', criteria);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.baseUrl + 'psm_connected_module_table',
            {

                qry_offset: criteria.offset,
                qry_limit: criteria.limit,
                qry_orderCol: criteria.sortCol,
                qry_isDesc: criteria.sortDesc,
                qry_filter: criteria.filter,
                psmProductId: criteria.psmProductId,
                companyId: criteria.companyId,
                moduleTypeId: criteria.moduleTypeId,
            },
            { headers: hdrs }).pipe(
                map((resp: ResponsePsmModuleTable) => {
                    //console.log('psmProductTable -> resp=', resp);
                    const res = Object.assign(new ResponsePsmModuleTable(), resp);
                    //console.log('aircraftTable -> response=', res);
                    return res;
                }),
                tap(event => {
                    this.log(`tap psmProductTable event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    //#endregion
}
