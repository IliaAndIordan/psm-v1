import { ResponseModel } from "../../models/responce.model";
import { RowsCountModel, TableCriteriaBase } from "../../models/table-criteria-base.model";
import { IUserModel } from "../auth/dto";


export class UserTableCriteria extends TableCriteriaBase {
    companyId?: number;
}

export class RespUserTableData {
    users: IUserModel[];
    rowsCount: RowsCountModel;
}

export class ResponseUserTable extends ResponseModel {
    data: RespUserTableData;
}
