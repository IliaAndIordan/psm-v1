import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Subject, Observable, of, BehaviorSubject } from 'rxjs';
// Services
// --- Models
import { ApiBaseClient } from '../../api-base.client';
import { CurrentUserService } from '../../current-user.service';
import { environment } from 'src/environments/environment';
import { ResponseGetUser, UserModel } from '../auth/dto';
import { catchError, map, tap } from 'rxjs/operators';
import { ResponseUserTable, UserTableCriteria } from './dto';


// import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({ providedIn: 'root' })
export class PsmUserClient extends ApiBaseClient {


  constructor(
    private client: HttpClient,
    private cus: CurrentUserService) {
    super();
  }

  private get sertviceUrl(): string {
    return environment.services.url.auth;
  }

  //#region User

  userSave(value: UserModel): Observable<ResponseGetUser> {
    console.log('userSave-< value', value);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'user_save',
      { user: value },
      { headers: hdrs }).pipe(
        map((res: ResponseGetUser) => {
          console.log('userSave-> res=', res);
          const resp = Object.assign(new ResponseGetUser(), res);
          return resp;
        }),
        catchError(this.handleError)
      );

  }
  
  userTable(criteria: UserTableCriteria): Observable<ResponseUserTable> {

    //console.log('macTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'users_table',
      {
        offset: criteria.offset,
        limit: criteria.limit,
        sortCol: criteria.sortCol,
        sortDesc: criteria.sortDesc,
        filter: criteria.filter,
        companyId: criteria.companyId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseUserTable) => {
          //console.log('userTable -> resp=', resp);
          const res = Object.assign(new ResponseUserTable(), resp);
          //console.log('aircraftTable -> response=', res);
          return res;
        }),
        tap(event => {
          this.log(`tap aircraftTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }


  //#endregion
}
