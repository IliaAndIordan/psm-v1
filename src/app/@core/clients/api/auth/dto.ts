import { ILocalUserSettings } from "../../models/local-user-settings";
import { ResponseModel } from "../../models/responce.model";
import { CompanyModel, ICompanyModel, ICompanyProduct, IResponseCompanySaveData, ResponseCompanySave, ResponseCompanySaveData } from "../company/dto";
import { PsmUserRole } from "./enums";

export class UserModel {

    public adate: Date;
    public companyId: number;
    public email: string;
    public ipAddress: string;
    public isReceiveEmails: boolean;
    public name: string;
    public password: string;
    public role: PsmUserRole;
    public udate: Date;
    public userId: number;

    public companyName?: string;
    public branchCode?: string;
    public companyTypeId?: number;
    public countryId?: number;

    public static fromJSON(json: IUserModel): UserModel {
        const vs = Object.create(UserModel.prototype);
        return Object.assign(vs, json, {
            role: json.role ? json.role as PsmUserRole : PsmUserRole.Guest,
            name:json.name?json.name:undefined,
            isReceiveEmails: json.isReceiveEmails && json.isReceiveEmails > 0 ? true : false,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? UserModel.fromJSON(value) : value;
    }
    
}

export interface IUserModel {

    adate: string;
    companyId: number;
    email: string;
    ipAddress: string;
    isReceiveEmails: number;
    name: string;
    password: string;
    role: number;
    udate: string;
    userId: number;
}

export class RefreshTokenModel {
    client_id: number;
    exp: number;
    iss: string;
    guid: string;
}

export interface ResponseAuthenticateUser extends ResponseGetUserData {
    settings: ILocalUserSettings;
}

export class ResponseAuthenticate extends ResponseModel {
    data: ResponseAuthenticateUser;
}

export class LatLng {
    constructor(latitude: number, longitude: number, altitude?: number) {
        this.lat = latitude;
        this.lng = longitude;
        this.alt = altitude;
    }
    lat: number;
    lng: number;
    alt?: number;
}


export class ResponseGetUserData extends ResponseCompanySaveData {
    public user: IUserModel;
}


export class ResponseGetUser extends ResponseModel {
    data: ResponseGetUserData;
}


export interface IUserTimestampModel {

    user_id: number;
    user_name: string;
    e_mail: string;
    u_password: string;
    company_id: number;
    user_role: number;
    is_receive_emails: number;
    ip_address: string;
    adate: number;
    udate: number;
    userName:string;

    company_name?: string;
    branch_code?: string;
    company_type_id?: number;
    country_id?: number;
}

export class RespCompanyUsersData {
    users: IUserTimestampModel[];
    totals: {
        total_rows: number;
    };
}

export class RespCompanyUsers extends ResponseModel {
    data: RespCompanyUsersData;
}

