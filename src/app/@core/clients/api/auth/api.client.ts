import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, of, Subscription, throwError} from 'rxjs';


import { UserModel, ResponseAuthenticate } from './dto';
import { environment } from 'src/environments/environment';
import { ApiBaseClient } from '../../api-base.client';
import { CompanyModel, CompanyProduct } from '../company/dto';
import { TokenService } from '../../token.service';
import { CurrentUserService } from '../../current-user.service';
import { catchError, map } from 'rxjs/operators';
import { LocalUserSettings } from '../../models/local-user-settings';
import { AppRoutes } from 'src/app/@core/constants/app-routes.const';


@Injectable({ providedIn: 'root' })
export class SxApiAuthClient extends ApiBaseClient {

  constructor(
    private router: Router,
    private client: HttpClient,
    private tokenService: TokenService,
    private cus: CurrentUserService) {
    super();
    
  }

  private get baseUrl(): string {
    return environment.services.url.auth;
  }

  //#region JWT Tocken

  autenticate(name: string, pwd: string): Observable<any> {
    // console.log('user: ' + name + ', ped: ' + pwd);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.baseUrl + 'login',
      { username: name, password: pwd },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((responce: HttpResponse<any>) => this.convertAutenticateToUser(responce)),
        catchError(this.handleError)
      );
  }

  convertAutenticateToUser(response: HttpResponse<any>): ResponseAuthenticate {
    let userObj;
    let userSettings: LocalUserSettings;
    let company: CompanyModel;
    let token;
    let rv: ResponseAuthenticate;
    let products = new Array<CompanyProduct>();
    //console.log('response:', response);
    if (response) {
      if (response.headers) {
        token = response.headers.get('X-Authorization');
      }

      if (response.body) {
        rv = Object.assign(new ResponseAuthenticate(), response.body);
        //console.log('convertAutenticateToUser ResponseAuthenticate:', rv);
        if (rv && rv.success) {

          if (rv.data && rv.data.user) {
            userObj = UserModel.fromJSON(rv.data.user);
            //console.log('convertAutenticateToUser -> userObj:', userObj);
          }
          
          if (rv.data && rv.data.company) {
            company = CompanyModel.fromSaveModel(rv.data);
            products = company.sxProducts;
            //console.log('convertAutenticateToUser -> company:', company);
          }


          if (rv.data.settings) {
            userSettings = LocalUserSettings.fromJSON(rv.data.settings);
            //console.log(' User Settings: ', userSettings);
          }

        }
      }


      if (token) {
        this.tokenService.barerToken = token;

        //console.log('decodeToken:', this.tokenService.decodeToken());
        this.cus.user = userObj;
        this.cus.localSettings = userSettings;
        this.cus.company = company;
        this.cus.sxProducts = products;
        this.cus.user = userObj;

      }

    }

    // console.dir(retvalue);
    return rv;
  }

  refreshToken(): Observable<any> {

    //console.log('refreshToken -> ');
    //console.log('refreshToken -> barerToken:', this.tokenService.barerToken);
    //console.log('refreshToken -> refreshToken:', this.tokenService.refreshToken);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.baseUrl + 'jwtrefresh',
      { refresh: this.tokenService.refreshToken },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((response: HttpResponse<any>) => {
          const ra: ResponseAuthenticate = this.convertAutenticateToUser(response);
          //console.log('refreshToken -> ResponseAuthenticate:', ra);
          //console.log('refreshToken -> barerToken:', this.tokenService.barerToken);
          return this.tokenService.barerToken;
        }),
        catchError(this.handleError)
      );
  }

  logout(): Observable<any> {
    console.log('logout -> ');
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.baseUrl + '/authenticate',
      { provider: 'logout' },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((response: HttpResponse<any>) => {
          console.log('logout -> responce=', response);
          this.tokenService.clearToken();
          this.cus.user = undefined;
          this.cus.clearLocalStorage();
          this.cus.clearSession();
          this.router.navigate(['/', AppRoutes.public]);
          return of(response);
        }),
        /*
        tap((res: HttpResponse<any>) => {
          console.log(res.headers.keys);
        }),*/
        catchError(this.handleError)
      );
  }

  register(mail: string, pwd: string): Observable<any> {
    //console.log('e-mail: ' + mail + ', psw: ' + pwd);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.baseUrl + '/register',
      { email: mail, password: pwd },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((responce: HttpResponse<any>) => this.convertAutenticateToUser(responce)),
        /*
        tap((res: HttpResponse<any>) => {
          console.log(res.headers.keys);
        }),*/
        catchError(this.handleError)
      );
    /*
      .map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      });
      */
  }

  //#endregion


}
