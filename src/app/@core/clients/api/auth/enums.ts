import { Pipe, PipeTransform } from "@angular/core";
import { EnumViewModel } from "../../models/enum-view.model";

//#region PsmUserRole

export enum PsmUserRole {
    Admin = 1,
    CompanyAdmin = 2,
    User = 3,
    Guest = 4,
    RequestPermission = 5
}


export const PsmUserRoleOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Admin'
    },
    {
        id: 2,
        name: 'CompanyAdmin'
    },
    {
        id: 3,
        name: 'User'
    },
    {
        id: 4,
        name: 'Guest'
    },
    {
        id: 5,
        name: 'Request Permission'
    },
];

@Pipe({ name: 'psmuserrole' })
export class PsmUserRoleDisplayPipe implements PipeTransform {
    transform(value: number, args: string[] = undefined): string {
        let rv: string = PsmUserRole[value];
        const data: EnumViewModel = PsmUserRoleOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

//#endregion