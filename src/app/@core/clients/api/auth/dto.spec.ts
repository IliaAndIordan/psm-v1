import { TestBed } from '@angular/core/testing';
import { IUserModel, UserModel } from './dto';


describe('UserModel', () => {
  let dto: UserModel;
  let dateJson: IUserModel = {
    adate: "2020-06-07 12:55:37",
    companyId: 25,
    email: "iziordanov@gmail.com",
    ipAddress: "95.43.220.58",
    isReceiveEmails: 1,
    name: "Iordan G Mail",
    password: "password",
    role: 1,
    udate: "2021-09-01 23:37:21",
    userId: 1,
  };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    dto = Object.assign(new UserModel(), dateJson);
  });

  it('should be created', () => {
    expect(dto).toBeTruthy();
  });

  it('#fromJSON should return real value', () => {
    dto = UserModel.fromJSON(dateJson);
    expect(dto).toBeTruthy();
    dateJson.adate = '2020-12-23 22:19:35';
    dateJson.udate = '2020-12-23 22:19:35';
    dateJson.companyId = undefined;
    dto = UserModel.fromJSON(dateJson);
    expect(dto).toBeTruthy();
  });

});

