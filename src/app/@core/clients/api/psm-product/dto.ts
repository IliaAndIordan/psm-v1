import { ResponseModel } from "../../models/responce.model";
import { RowsCountModel, TableCriteriaBase } from "../../models/table-criteria-base.model";
import { PsmModuleType } from "./enums";

//#region PsmProduct

export class PsmProduct {
    public psmProductId: number;
    public psmProductKey: string;
    public name: string;
    public logoUrl: string;
    public webUrl: string;
    public notes: string;
    public companyId: number;
    public actorId: number;
    public adate: Date;
    public udate: Date;

    actorName?: string;

    public static fromJSON(json: IPsmProduct): PsmProduct {
        const vs = Object.create(PsmProduct.prototype);
        return Object.assign(vs, json, {
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? PsmProduct.fromJSON(value) : value;
    }

    public toJSON(): IPsmProduct {
        const vs = Object.create(PsmProduct.prototype);
        return Object.assign(vs, this);
    }

    public static fromSaveModel(resp: IResponsePsmProductSaveData): PsmProduct {
        let rv: PsmProduct;
        if (resp) {
            rv = PsmProduct.fromJSON(resp.psmProduct);
        }
        return rv;
    }
}

export interface IPsmProduct {
    psmProductId: number;
    psmProductKey: string;
    name: string;
    logoUrl: string;
    webUrl: string;
    notes: string;
    companyId: number;
    actorId: number;
    adate: string;
    udate: string;

    actorName?: string;

}


export interface IResponsePsmProductSaveData {
    psmProduct: IPsmProduct;
}
export class ResponsePsmProductSaveData {
    psmProduct: IPsmProduct;
}


export class ResponsePsmProductSave extends ResponseModel {
    data: ResponsePsmProductSaveData;
}


export class PsmProductTableCriteria extends TableCriteriaBase {
    companyId?: number;
    actorId?: number;
}

export class PsmProductTableData {
    psmProducts: IPsmProduct[];
    rowsCount: RowsCountModel;
}

export class ResponsePsmProductTable extends ResponseModel {
    data: PsmProductTableData;
}

//#endregion

//#region PsmModule

export class PsmModule {
    public moduleId: number;
    public name: string;
    public moduleTypeId: PsmModuleType;
    public psmProductId: number;
    public notes: string;
    public pmId?: number;
   
    public psmProductKey?: string;
    public psmProductName?: string;
    public companyId?: string;

    public static fromJSON(json: IPsmModule): PsmModule {
        const vs = Object.create(PsmModule.prototype);
        return Object.assign(vs, json, {
            moduleTypeId: (json && json.moduleTypeId) ? json.moduleTypeId as PsmModuleType: undefined,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? PsmModule.fromJSON(value) : value;
    }

    public toJSON(): IPsmModule {
        const vs = Object.create(PsmModule.prototype);
        return Object.assign(vs, this);
    }

    public static fromSaveModel(resp: IResponsePsmModuleSaveData): PsmModule {
        let rv: PsmModule;
        if (resp) {
            rv = PsmModule.fromJSON(resp.psmModule);
        }
        return rv;
    }
}

export interface IPsmModule {
    moduleId: number;
    name: string;
    moduleTypeId: number;
    psmProductId: number;
    notes: string;
    pmId?: number;
   
    psmProductKey?: string;
    psmProductName?: string;
    companyId?: string;
}

export interface IResponsePsmModuleSaveData {
    psmModule: IPsmModule;
}
export class ResponsePsmModuleSaveData {
    psmModule: IPsmModule;
}

export class ResponsePsmModuleSave extends ResponseModel {
    data: ResponsePsmModuleSaveData;
}

export class PsmModuleTableCriteria extends TableCriteriaBase {
    psmProductId?: number;
    companyId?: number;
    moduleTypeId?: number;
}

export class PsmModuleTableData {
    psmModules: IPsmModule[];
    rowsCount: RowsCountModel;
}

export class ResponsePsmModuleTable extends ResponseModel {
    data: PsmModuleTableData;
}

//#endregion

