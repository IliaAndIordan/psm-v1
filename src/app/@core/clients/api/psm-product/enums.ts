import { Pipe, PipeTransform } from "@angular/core";
import { EnumViewModel } from "../../models/enum-view.model";

//#region CompanyType
export enum PsmModuleType {
    StorageDB = 1,
    StorageFiles = 2,
    WebService = 3,
    MicroService = 4,
    ApplicationService = 5,
    ApplicationWeb = 6,
    ApplicationGUI = 7,
}



export const PsmModuleTypeOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Storage DB'
    },
    {
        id: 2,
        name: 'Storage Files'
    },
    {
        id: 3,
        name: 'Web Service'
    },
    {
        id: 4,
        name: 'Micro Service'
    },
    {
        id: 5,
        name: 'Application Service'
    },
    {
        id: 6,
        name: 'Application Web'
    },
    {
        id: 7,
        name: 'Application GUI'
    },
];


@Pipe({ name: 'psmmoduletype' })
export class PsmModuleTypeTypeDisplayPipe implements PipeTransform {
    transform(value: number, args?: string[]): string {
        let rv: string = PsmModuleType[value];
        const data: EnumViewModel = PsmModuleTypeOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

//#endregion

