import { TestBed } from "@angular/core/testing";
import { JwtHelperService, JwtModule } from "@auth0/angular-jwt";
import { TokenService } from "./token.service";

describe('TokenService', () => {
    let service: TokenService;
    let jwtHelper: JwtHelperService;
  
    beforeEach(() => {
      TestBed.configureTestingModule({
          imports:[
            JwtModule.forRoot({
                config: {
                  tokenGetter: () => {
                    return '';
                  }
                }
              }),
          ],
          providers:[
            TokenService
          ]
      });
      jwtHelper = TestBed.inject(JwtHelperService);
      service = TestBed.inject(TokenService);
      
    });
  
    it('should be created', () => {
      expect(service).toBeTruthy();
    });
  
    it('#isTokenExpired should return true', () => {
        const spy = jasmine.createSpyObj('JwtHelperService', ['isTokenExpired']);
        spy.isTokenExpired.and.returnValue(true);
        //const spyRefreshToken = spyOnProperty(TokenService.prototype , 'refreshToken', 'get').and.returnValue(undefined);
      expect(service.isTokenExpired()).toBe(true);
      //expect(spyRefreshToken).toHaveBeenCalled();
      expect(service.isAccessTokenExpired()).toBe(true);
      expect(service.isRefreshTokenExpired()).toBe(true);
      //expect(service.clearToken).toHaveBeenCalled();
    });

    it('#getTokenExpirationDate should return stubbed value from a spy', () => {
        //const spy = jasmine.createSpy(JwtHelperService.prototype, 'getTokenExpirationDate');
        const stubValue = new Date();
        const spy = spyOn(JwtHelperService.prototype , 'getTokenExpirationDate').and.returnValue(stubValue);
        
        expect(service.getTokenExpirationDate())
        .toBe(stubValue);
    });

    it('#getTokenExpirationDate should return stubbed value from a spy', () => {
        //const spy = jasmine.createSpy(JwtHelperService.prototype, 'getTokenExpirationDate');
        const stubValue = 'test decode tocken';
        const spy = spyOn(JwtHelperService.prototype , 'decodeToken').and.returnValue(stubValue);
        //const spyBarerToken = spyOnProperty(TokenService.prototype , 'barerToken', 'get').and.returnValue(stubValue);
        expect(service.decodeToken()).toBe(stubValue);
        
        //expect(spyBarerToken).toHaveBeenCalled();
        //expect(service.clearToken).toHaveBeenCalled();
    });
  });