
export class ResponseModel {
    public status: string;
    public message: string;

    public get success(): boolean {
        return this && this.status ? this.status === 'success' : false;
    }
}
