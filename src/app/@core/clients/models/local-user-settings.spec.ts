import { TestBed } from '@angular/core/testing';
import { ILocalUserSettings, LocalUserSettings } from './local-user-settings';


describe('LocalUserSettings', () => {
  let dto: LocalUserSettings;
  const dateJson:ILocalUserSettings = {userId: 1, snavLeftWidth: 3, displayDistanceMl: 1, displayWeightLb:1 };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    dto = Object.assign(new LocalUserSettings(), dateJson);
  });

  it('should be created', () => {
    expect(dto).toBeTruthy();
  });

  it('#fromJSON should return real value', () => {
    dto = LocalUserSettings.fromJSON(dateJson);
    expect(dto).toBeTruthy();
  });

  it('#toJSON should ve called', () => {
    dto = LocalUserSettings.fromJSON(dateJson);
    expect(dto).toBeTruthy();
    const dtoJson = dto.toJSON();
    expect(dtoJson).toBeTruthy();
  });
});
