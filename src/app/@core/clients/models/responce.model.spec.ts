import { TestBed } from '@angular/core/testing';
import { ResponseModel } from './responce.model';

describe('LocalUserSettings', () => {
  let dto: ResponseModel;
  const dtoJson = {status:'success', message:'test message' };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    dto = Object.assign(new ResponseModel(), {status:'success', message:'test message' });
  });

  it('should be created', () => {
    expect(dto).toBeTruthy();
  });

  it('#sucess test', () => {
    expect(dto).toBeTruthy();
    expect(dto.success).toBe(true);
    dto.status = 'error';
    expect(dto.success).toBe(false);
  });

});
