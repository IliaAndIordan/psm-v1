import { TestBed } from '@angular/core/testing';
import { DateModel, IDateModel } from './date.model';


describe('DateModel', () => {
  let dto: DateModel;
  const dateJson:IDateModel = {date: "2021-08-31 23:38:55.000000", timezone_type: 3, timezone: "Europe/Helsinki"};

  beforeEach(() => {
    TestBed.configureTestingModule({});
    dto = Object.assign(new DateModel(), dateJson);
  });

  it('should be created', () => {
    expect(dto).toBeTruthy();
  });

  it('#fromJSON should return real value', () => {
    dto = DateModel.fromJSON(dateJson);
    expect(dto).toBeTruthy();
  });

  it('#toJSON should ve called', () => {
    dto = DateModel.fromJSON(dateJson);
    expect(dto).toBeTruthy();
    const dtoJson = dto.toJSON();
    expect(dtoJson).toBeTruthy();
  });
});
