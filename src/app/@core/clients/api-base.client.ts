import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { AppModule } from 'src/app/app.module';
import { ToastrService } from 'ngx-toastr';
import { ResponseModel } from './models/responce.model';

@Injectable({ providedIn: 'root' })
export class ApiBaseClient {

    constructor() { }

    //#region Base

    protected handleError(error: HttpErrorResponse) {
        console.log('handleError-> error:', error);
        if (error.error instanceof ErrorEvent) {
            // Aclient-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            const toastr = AppModule.injector.get(ToastrService);
            let msg =  `Backend returned code ${error.status} but error out during data processing. `;
            let title = 'Syntax Error';
           switch(error.status){
               case 200:
                   msg =  `Backend returned code ${error.status} but error out during data processing. `;
                   title = 'Syntax Error';
                   break;
                case 401:
                    const err:ResponseModel = Object.assign(new ResponseModel(),  error.error);
                    msg =  err?err.message: `Backend returned code ${error.status} but error out during data processing. `;
                    title = 'Unauthorized';
                    break;
           }
           toastr.error(msg, title);
           /*
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error ? error.error.message : error.message}`);
            if (error.error && error.error.statusCode === 401) {
                console.error('handleError -> Go to Logout:');
                return throwError(error);
            }*/
        }

        // Let the app keep running by returning an empty result.
        // return of(result as T);
        return throwError(error);

    }

    /** Log a Service message  */
    protected log(message: string) {
        // console.log('Airport Runway Service', message);
        // this.toastr.info(message, 'WAD Service');
    }

    //#endregion
}
