import { Injectable, Inject } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AppStore } from '../constants/app-storage.const';
import { RefreshTokenModel } from './api/auth/dto';
import { TokenFactory } from './token-factory';

@Injectable({ providedIn: 'root' })
export class TokenService extends TokenFactory {


  constructor(private jwtHelper: JwtHelperService) {
    super();
  }

  //#region JwtHelperService methods

  public isTokenExpired(): boolean {
    const retVal = this.jwtHelper.isTokenExpired();
    if (retVal) {
      // console.log('isTokenExpired: ' + retVal);
    }
    return retVal;
  }

  public isAccessTokenExpired(): boolean {
    return this.isTokenExpired();
  }

  public isRefreshTokenExpired(): boolean {
    let rv = true;
    if (this.refreshToken) {
      const refToken: RefreshTokenModel = Object.assign(new RefreshTokenModel(), this.decode(this.refreshToken));
      // console.log('isRefreshTokenExpired-> refToken:', refToken);
      if (refToken) {
        const curDate: Date = new Date();
        // console.log('isRefreshTokenExpired-> curDate ms:', curDate.getTime());
        const diff_ms = ((refToken.exp * 1000) - (curDate.getTime() + 500));
        // console.log('isRefreshTokenExpired-> diff_ms:', diff_ms.toFixed());
        rv = diff_ms > 0 ? false : true;
      }
    }


    return rv;
  }

  public getTokenExpirationDate(): Date {
    //const rv = this.jwtHelper.getTokenExpirationDate() as Date;
    return this.jwtHelper.getTokenExpirationDate() as Date;
  }

  public decodeToken(): string {
    return this.jwtHelper.decodeToken();
  }

  public decode(token: string): string {
    return this.jwtHelper.decodeToken(token);
  }

  public get expIntervalMs(): number {
    let rv = 0;
    const curDate: Date = new Date();
    const jwtExp: Date = this.getTokenExpirationDate();
    rv = jwtExp.getTime() - curDate.getTime();
    // console.log('ExpIntervalMs-> rv=', rv);
    return rv;
  }

  //#endregion

  //#region Token

  

  public get barerToken(): string {
    return sessionStorage.getItem(AppStore.BarerToken) as string;
  }

  public set barerToken(value: string) {
    if (value) {
      sessionStorage.setItem(AppStore.BarerToken, JSON.stringify(value));
    } else {
      sessionStorage.removeItem(AppStore.BarerToken);
    }
  }

  public get refreshToken(): string {
    return sessionStorage.getItem(AppStore.RefreshToken) as string;
  }

  public set refreshToken(value: string) {
    if (value) {
      sessionStorage.setItem(AppStore.RefreshToken, JSON.stringify(value));
    } else {
      sessionStorage.removeItem(AppStore.RefreshToken);
    }
  }


  public clearToken() {
    sessionStorage.removeItem(AppStore.RefreshToken);
    sessionStorage.removeItem(AppStore.BarerToken);
  }


  //#endregion
}
