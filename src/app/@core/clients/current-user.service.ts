import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject, of, Subject } from 'rxjs';
// --- Services
import { GravatarService } from '@infinitycube/gravatar';
// Models
import { TokenService } from './token.service';
import { LatLng, UserModel } from './api/auth/dto';
import { AppStore, URL_NO_IMG_SQ } from '../constants/app-storage.const';
import { CompanyType } from './api/company/enums';
import { CompanyModel, CompanyProduct } from './api/company/dto';
import { PsmProject } from './api/project/dto';
import { LocalUserSettings } from './models/local-user-settings';
import { PsmUserRole } from './api/auth/enums';

@Injectable({ providedIn: 'root' })
export class CurrentUserService {
    
    public userChanged = new BehaviorSubject<UserModel>(undefined);
    
    redirectTo:string;

    constructor(
        private tockenService: TokenService,
        private gravatarService: GravatarService) {
    }


    //#region Location Geo

    getLocation(): Observable<LatLng> {
        return new Observable(obs => {
            const str = sessionStorage.getItem(AppStore.UserLocation);
            if (!str) {
                navigator.geolocation.getCurrentPosition(
                    pos => {
                        const rv = new LatLng(pos.coords.latitude, pos.coords.longitude);
                        sessionStorage.setItem(AppStore.UserLocation, JSON.stringify(rv));
                        obs.next(rv);
                        obs.complete();
                    },
                    error => {
                        console.log('error', error);
                        const rv = new LatLng(42.704198, 23.2840454);
                        obs.next(rv);
                        obs.complete();
                    }
                );
            } else {
                const rv = JSON.parse(str) as LatLng;
                obs.next(rv);
                obs.complete();
            }

        });
    }

    clearLocation() {

        sessionStorage.removeItem(AppStore.UserLocation);
    }

    //#endregion

    //#region User

    get user(): UserModel {
        const userStr = sessionStorage.getItem(AppStore.CurrentUser);
        let userObj;
        // console.log('userStr: ', userStr);
        if (userStr) {
            userObj = Object.assign(new UserModel(), JSON.parse(userStr));
        }
        // console.log('userObj: ', userObj);
        return userObj;
    }

    get userName(): string {
        let rv = undefined;
        const userObj = this.user;
        if (userObj) {
            const emailIdx = userObj.email.indexOf('@');
            rv = userObj.name && userObj.name.length>0?userObj.name:userObj.email.substr(0,emailIdx);
        }
        return rv;
    }

    set user(userObj: UserModel) {
        if (userObj) {
            sessionStorage.setItem(AppStore.CurrentUser, JSON.stringify(userObj));
        } else {
            sessionStorage.removeItem(AppStore.CurrentUser);
        }
        this.userChanged.next(userObj);
    }

    get isLogged(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        retValue = (userObj && !this.tockenService.isTokenExpired()) ? true : false;
        return retValue;
    }

    get isAdmin(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        retValue = (userObj && userObj.role === PsmUserRole.Admin);
        return retValue;
    }

    get isCompanyAdmin(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        if (userObj && userObj.companyId) {
            retValue = (userObj.role === PsmUserRole.CompanyAdmin);
        }
        // console.log('isCompanyAdmin: ' + retValue);
        return retValue;
    }

    get isAuthenticated(): boolean {
        // console.log('isAuthenticated: ');
        return this.isLogged;
    }

    get avatarUrl(): string {
        let retValue = URL_NO_IMG_SQ;
        const userObj: UserModel = this.user;
        // console.log('avatarUrl: userObj', userObj);
        // console.log('avatarUrl: email',  this.user.e_mail);
        if (this.user && this.user.email) {

            const gravatarImgUrl = this.gravatarService.url(userObj.email, 128, 'identicon');
            // console.log('gravatarImgUrl', gravatarImgUrl);
            if (gravatarImgUrl) {
                retValue = gravatarImgUrl;
            }
        }

        return retValue;
    }

    getAvatarUrl(e_mail: string): string {
        let retValue = URL_NO_IMG_SQ;
        const userObj: UserModel = this.user;
        // console.log('avatarUrl: userObj', userObj);
        // console.log('avatarUrl: email',  this.user.e_mail);
        if (e_mail) {

            const gravatarImgUrl = this.gravatarService.url(e_mail, 128, 'identicon');
            // console.log('gravatarImgUrl', gravatarImgUrl);
            if (gravatarImgUrl) {
                retValue = gravatarImgUrl;
            }
        }

        return retValue;
    }

    clearLocalStorage() {
        //console.log('clearLocalStorage ->');
        Object.keys(AppStore).forEach((key: string) => {
            if (AppStore.hasOwnProperty(key)) {
                const val = (AppStore as any)[key];
                // console.log('clearLocalStorage key= ' + key + ', val= ' + val);
                localStorage.removeItem(val);
            }
        });
        // console.log('clearLocalStorage <-');
    }

    clearSession() {
        //console.log('clearSession ->');
        Object.keys(AppStore).forEach((key: string) => {
            if (AppStore.hasOwnProperty(key)) {
                const val = (AppStore as any)[key];
                // console.log('clearLocalStorage key= ' + key + ', val= ' + val);
                sessionStorage.removeItem(val);
            }
        });
        // console.log('clearLocalStorage <-');
    }

    logout(): Observable<boolean> {

        return new Observable<boolean>(subscriber => {
            this.tockenService.clearToken();
            this.clearLocalStorage();
            this.clearSession();
            this.user = undefined;
            subscriber.next(true);
            subscriber.complete();
        });

    }
    //#endregion

    //#region CompanyModel

    // tslint:disable-next-line: member-ordering
    companyChanged = new Subject<CompanyModel>();

    get company(): CompanyModel {
        const objStr = sessionStorage.getItem(AppStore.CurrentUserCompany);
        let obj: CompanyModel;
        if (objStr) {
            obj = Object.assign(new CompanyModel(), JSON.parse(objStr));
        }
        // console.log('company-> obj', obj);
        return obj;
    }

    set company(value: CompanyModel) {
        if (value) {
            sessionStorage.setItem(AppStore.CurrentUserCompany, JSON.stringify(value));
        } else {
            sessionStorage.removeItem(AppStore.CurrentUserCompany);
        }
        this.companyChanged.next(value);
    }

    get companyType(): CompanyType {
        return this.company ? this.company.typeId : undefined;
    }

    //#endregion

    //#region Company Sx Products

    sxProductsChanged = new Subject<Array<CompanyProduct>>();

    get sxProducts(): Array<CompanyProduct> {
        const objStr = sessionStorage.getItem(AppStore.CurrentUserSxProducts);
        let obj: Array<CompanyProduct>;
        if (objStr) {
            obj = Object.assign(new Array<CompanyProduct>(), JSON.parse(objStr));
        }
        return obj;
    }

    set sxProducts(value: Array<CompanyProduct>) {
        const oldValue = this.sxProducts;
        if (value) {
            sessionStorage.setItem(AppStore.CurrentUserSxProducts, JSON.stringify(value));
        } else {
            sessionStorage.removeItem(AppStore.CurrentUserSxProducts);
        }

        this.sxProductsChanged.next(value);
    }

    //#endregion

    //#region Project

    // tslint:disable-next-line: member-ordering
    selProjectChanged = new Subject<PsmProject>();

    get selProject(): PsmProject {
        const onjStr = localStorage.getItem(AppStore.project);
        let obj: PsmProject;
        if (onjStr) {
            obj = Object.assign(new PsmProject(), JSON.parse(onjStr));
        }
        return obj;
    }

    set selProject(value: PsmProject) {
        const oldValue = this.selProject;
        if (value) {
            localStorage.setItem(AppStore.project, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.project);
        }

        this.selProjectChanged.next(value);
    }

    //#endregion

    //#region Local User Settings

    // tslint:disable-next-line:member-ordering
    public LocalSettingsChanged = new Subject<LocalUserSettings>();

    get localSettings(): LocalUserSettings {
        const userStr = sessionStorage.getItem(AppStore.UserSettings);
        let userObj = null;
        if (userStr) {
            userObj = Object.assign(new LocalUserSettings(), JSON.parse(userStr));
        } else {
            userObj = new LocalUserSettings();
            userObj.snavLeftWidth = 4;
            sessionStorage.setItem(AppStore.UserSettings, JSON.stringify(userObj));
        }
        return userObj;
    }

    public set localSettings(userObj: LocalUserSettings) {
        if (userObj) {
            sessionStorage.setItem(AppStore.UserSettings, JSON.stringify(userObj));
        } else {
            sessionStorage.removeItem(AppStore.UserSettings);
        }
        this.LocalSettingsChanged.next(userObj);
    }

    //#endregion

    //#region Airports



    //#endregion
}
