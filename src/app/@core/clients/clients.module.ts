import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { GravatarModule } from '@infinitycube/gravatar';
import { SxApiCompanyClient } from './api/company/api-client';
import { ApiBaseClient } from './api-base.client';
import { SxApiAuthClient } from './api/auth/api.client';
import { JwtInterceptor } from '@auth0/angular-jwt';
import { TokenInterceptor } from './token.interseptor';
import { PsmUserClient } from './api/user/api-client';



@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    
    GravatarModule,
  ],
  declarations: [
    // Pipes
  ],
  exports:[
    GravatarModule,

    // Pipes
    
  ],
  providers:[
    ApiBaseClient,
    SxApiAuthClient,
    SxApiCompanyClient,
    PsmUserClient,
    JwtInterceptor, // Providing JwtInterceptor allow to inject JwtInterceptor manually into RefreshTokenInterceptor
    {
      provide: HTTP_INTERCEPTORS,
      useExisting: JwtInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
  ]
})

export class PsmClientsModule { }
