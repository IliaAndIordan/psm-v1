import { Injectable, Inject } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AppStore } from '../constants/app-storage.const';

@Injectable({
    providedIn: 'root'
  })
export class TokenFactory {

    // public headerName: string =  'Authorization';
    // public authScheme: string = 'Bearer ';
    // tslint:disable-next-line:no-inferrable-types
    public skipWhenExpired: boolean = false;
    public  throwNoTokenError = false;
    public allowedDomains : string[] = [
        'sx.ws.iordanov.info',
        'sx.iordanov.info',
        'common.ams.iord',
        'localhost:4306',
        'maps.googleapis.com',
        'psm.ws.iordanov.info',
    ];

    constructor() { }
    public tokenGetter = () => {
        const token = sessionStorage.getItem(AppStore.BarerToken);
        // console.log('TokenFactory.tokenGetter token:' + token);
        return token;
    }
}
