import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { SxSpinnerComponent } from './@share/sx-common/sx-spinner/sx-spinner.component';
import { SxSpinnerService } from './@share/sx-common/sx-spinner/sx-spinner.service';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { JwtModule } from '@auth0/angular-jwt';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { PsmCommonModule } from './@share/sx-common/psm-common.module';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let spinnerService: SxSpinnerService;
  let toastr: ToastrService;
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        NoopAnimationsModule,
        RouterModule,
        ToastrModule.forRoot(),
        JwtModule.forRoot({
          config: {
            tokenGetter: () => {
              return '';
            }
          }
        }),
        PsmCommonModule,
      ],
      declarations: [
        AppComponent
      ],
      providers: [SxSpinnerService]
    }).compileComponents();
  });

  beforeEach(() => {
    spinnerService = TestBed.inject(SxSpinnerService);
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it(`should have as title 'psm-v1'`, () => {
    expect(component.title).toEqual('psm-v1');
  });

  /*
  it('should render title', () => {
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content span')?.textContent).toContain('psm-v1 app');
  });
*/
  it('should call toggleSpinner', () => {
    component.showToastr();
    component.toggleSpinner();
    spinnerService.showLoaderSuject.subscribe(res => {
      expect(res).toBe(true);
    });
  });

});
