import { ApplicationRef, Injector, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PsmShareModule } from './@share/share.module';
import { JwtInterceptor, JwtModule, JWT_OPTIONS } from '@auth0/angular-jwt';
import { TokenFactory } from './@core/clients/token-factory';
import { CurrentUserService } from './@core/clients/current-user.service';
import { ApiBaseClient } from './@core/clients/api-base.client';
import { SxApiAuthClient } from './@core/clients/api/auth/api.client';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './@core/clients/token.interseptor';
import { TokenService } from './@core/clients/token.service';
import { AuthGuard } from './@core/guards/auth.guard.service';
import { AppService } from './@core/services/app.service';
import { SxSpinnerService } from './@share/sx-common/sx-spinner/sx-spinner.service';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
 
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    AppRoutingModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useClass: TokenFactory
      }
    }),
    //App Modules
    PsmShareModule,
    //
    
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    TokenService,
    CurrentUserService,
    SxSpinnerService,

    AuthGuard,

    AppService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
  static injector: Injector;
  
  constructor(injector: Injector,
    applicationRef: ApplicationRef) {
    AppModule.injector = injector;
    }
}
