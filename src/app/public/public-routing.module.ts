import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from '../@core/constants/app-routes.const';
import { AuthGuard } from '../@core/guards/auth.guard.service';
import { CompanySetComponent } from './company/company-set.component';
import { HomeComponent } from './home.component';
import { SxPublicComponent } from './sx-public.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  {
    path: AppRoutes.Root, component: SxPublicComponent,
    children: [
      { path: AppRoutes.Root, component: HomeComponent,
        children: [
          { path: AppRoutes.Root,   pathMatch: 'full', component: WelcomeComponent   },
          { path: AppRoutes.companyset, component: CompanySetComponent, canActivate:[AuthGuard] },
          // { path: AppRoutes.Dashboard, component: DashboardComponent },
        ]
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }

export const routedComponents = [
  SxPublicComponent, 
  HomeComponent, 
  WelcomeComponent,
  CompanySetComponent,
];
