import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule, routedComponents } from './public-routing.module';
import { PsmShareModule } from '../@share/share.module';
import { WelcomeComponent } from './welcome/welcome.component';


@NgModule({
  imports: [
    CommonModule,

    //App Modules
    PsmShareModule,

    //Current module
    PublicRoutingModule,
  ],
  declarations: [
    routedComponents,
    WelcomeComponent,
  ],
 
})
export class PublicModule { }
