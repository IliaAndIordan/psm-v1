import { Component, OnInit } from '@angular/core';
import { COMMON_IMG_HEROJURNEY } from 'src/app/@core/constants/app-storage.const';

@Component({
  selector: 'sx-public-welcome',
  templateUrl: './welcome.component.html',
})
export class WelcomeComponent implements OnInit {

  herojurneyUrl = COMMON_IMG_HEROJURNEY;
  
  constructor() { }

  ngOnInit(): void {
  }

}
