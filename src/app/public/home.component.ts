import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscribable, Subscription } from 'rxjs';
import { UserModel } from '../@core/clients/api/auth/dto';
import { CurrentUserService } from '../@core/clients/current-user.service';
import { AppRoutes } from '../@core/constants/app-routes.const';
import { COMMON_IMG_PSM_WHITE } from '../@core/constants/app-storage.const';
import { UserRegisterModal } from '../@share/sx-common/modals/user/register/user-register.dialog';
import { SxSpinnerService } from '../@share/sx-common/sx-spinner/sx-spinner.service';

@Component({
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit, OnDestroy {

  sxLogo = COMMON_IMG_PSM_WHITE;
  userChanged: Subscription;
  isLoggedIn: boolean;
  user: UserModel;
  userName: string;
  avatarUrl: string;

  constructor(
    private cdr: ChangeDetectorRef,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private spinnerService: SxSpinnerService,
    public dialogService: MatDialog,
    private cus: CurrentUserService) {
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }


  ngOnInit(): void {

    this.route.queryParams
      .subscribe(params => this.cus.redirectTo = params['redirectTo'] || AppRoutes.dashboard);

    this.cus.userChanged.subscribe((user => {
      this.initFields();
    }));

    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.userChanged) { this.userChanged.unsubscribe(); }
  }

  initFields(): void {
    this.isLoggedIn = this.cus.isLogged;
    this.user = this.cus.user;
    this.avatarUrl = this.cus.avatarUrl;
    this.userName = this.cus.userName;
  }

  public userRegisterModalShow(): void {

    const dialogRef = this.dialogService.open(UserRegisterModal, {
      width: '500px',
      height: '340px',
      data: { email: null, password: null }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
      // this.password = result.password;
    });

  }

}
