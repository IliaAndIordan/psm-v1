import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { ResponseGetUser, UserModel } from 'src/app/@core/clients/api/auth/dto';
import { PsmUserRole } from 'src/app/@core/clients/api/auth/enums';
import { SxApiCompanyClient } from 'src/app/@core/clients/api/company/api-client';
import { CompanyModel, CompanyProduct, RespCompanyListToJoin } from 'src/app/@core/clients/api/company/dto';
import { PsmUserClient } from 'src/app/@core/clients/api/user/api-client';
import { CurrentUserService } from 'src/app/@core/clients/current-user.service';
import { AppRoutes } from 'src/app/@core/constants/app-routes.const';
import { COMMON_IMG_HEROJURNEY } from 'src/app/@core/constants/app-storage.const';
import { SxSpinnerService } from 'src/app/@share/sx-common/sx-spinner/sx-spinner.service';

@Component({
  selector: 'sx-public-company-set',
  templateUrl: './company-set.component.html',
})
export class CompanySetComponent implements OnInit {

  herojurneyUrl = COMMON_IMG_HEROJURNEY;
  userChanged: Subscription;
  user: UserModel;

  companies: CompanyModel[];
  countTotal: number;

  constructor(private cus: CurrentUserService,
    private companyClient: SxApiCompanyClient,
    private uClient: PsmUserClient,
    private router: Router,
    private spinnerService: SxSpinnerService,
    private toastr: ToastrService) { 
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    }

  ngOnInit(): void {
    this.userChanged = this.cus.userChanged.subscribe((user => {
      this.initFields();
    }));

    this.loadCompanies();
  }

  ngOnDestroy(): void {
    if (this.userChanged) { this.userChanged.unsubscribe(); }
  }

  initFields(): void {
    this.user = this.cus.user;
  }

  registerCompanyClick() {

  }

  joinCompany(company: CompanyModel): void {
    if (company && this.user) {
      this.user.companyId = company.companyId;
      this.user.role = PsmUserRole.RequestPermission;
      this.spinnerService.show();
      this.uClient.userSave(this.user)
        .subscribe((resp: ResponseGetUser) => {
          this.spinnerService.hide();
          this.toastr.success('User updated.', 'Join Company');
          this.cus.user = UserModel.fromJSON(resp.data.user);
          this.cus.company = CompanyModel.fromJSON(resp.data.company);
          const products = new Array<CompanyProduct>();
          if (resp.data.sxProducts && resp.data.sxProducts.length > 0) {
            resp.data.sxProducts.forEach(element => {
              products.push(CompanyProduct.fromJSON(element));
            });
          }
          this.cus.sxProducts = products;

          if (this.cus.user && this.cus.user.companyId) {
            this.router.navigate([AppRoutes.Root, AppRoutes.dashboard]);
          }
        });
    }
  }

  loadCompanies(): void {
    this.spinnerService.show();
    this.companyClient.ListToJoin()
      .subscribe((res: RespCompanyListToJoin) => {

        this.companies = new Array<CompanyModel>();
        this.spinnerService.hide();
        if (res) {
          this.countTotal = res.data.countTotal;
          if (res.data &&
            res.data.companies &&
            res.data.companies.length > 0) {

            res.data.companies.forEach(el => {
              const obj = CompanyModel.fromJSON(el);
              this.companies.push(obj);
            });
            console.log('ListToJoin -> companies=', this.companies);
          }


        }
      },
        err => {
          this.spinnerService.hide();
          //this.toastr.error('Load company list failed', 'Load Data');
        });
  }
}
