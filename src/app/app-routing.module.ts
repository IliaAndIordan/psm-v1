import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from './@core/constants/app-routes.const';
import { AuthGuard } from './@core/guards/auth.guard.service';

const routes: Routes = [
  { path: AppRoutes.Root, pathMatch: 'full', redirectTo: AppRoutes.public },
  { path: AppRoutes.public,
    loadChildren: () => import('./public/public.module').then(m => m.PublicModule), data: { preload: false }
  },
  { path: AppRoutes.dashboard,
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule), 
    data: { preload: false }, canActivate: [AuthGuard]
  },
  { path: AppRoutes.company,
    loadChildren: () => import('./company/company.module').then(m => m.PsmCompanyModule), 
    data: { preload: false }, canActivate: [AuthGuard]
  },
  { path: AppRoutes.product,
    loadChildren: () => import('./psm-product/psm-products.module').then(m => m.PmsProductsModule), 
    data: { preload: false }, canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
