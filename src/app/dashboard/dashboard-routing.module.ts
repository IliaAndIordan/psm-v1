import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppRoutes } from "../@core/constants/app-routes.const";
import { DashboardComponent } from "./dashboard.component";
import { DashboardHomeComponent } from "./home.component";

const routes: Routes = [
    {
      path: AppRoutes.Root, component: DashboardComponent,
      children: [
        {
          path: AppRoutes.Root, component: DashboardHomeComponent,
         /*
          children: [
            { path: AppRoutes.Root, pathMatch: 'full', component: SxDistributorDashboardComponent},
  
            { path: AppRoutes.items, component: SxDistributorItemsHomeComponent, children: [
              {path: AppRoutes.Root,   pathMatch: 'full', component: SxDistributorCItemsHomeTabsComponent},
            ]},
            /*
            { path: AppRoutes.file_select, component: SxBomFileImportHomeComponent, children: [
              {path: AppRoutes.Root,   pathMatch: 'full', component: SxBomFileImportSelectFileComponent},
              { path: AppRoutes.map_fields, component: SxBomFileImportMapFieldsComponent },
            ]},
            /*
            { path: AppRoutes.manufacturers, component: AmsAdminManufacturersComponent },
            { path: AppRoutes.manufacturer, component: AmsAdminManufacturerHomeComponent, children: [
              {path: AppRoutes.Root,   pathMatch: 'full', component: AmsAdminManufacturerHomeTabsComponent},
            ]},
            { path: AppRoutes.mac, component: AmsAdminMacHomeComponent , children: [
              {path: AppRoutes.Root,   pathMatch: 'full', component: AmsAdminMacHomeTabsComponent},
            ]},
            { path: AppRoutes.user, component: AmsAdminUserHomeComponent },
            
          ]
          */
        },
        // { path: 'copper-pricing', component: CopperPricingComponent }
      ]
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class DashboardRoutingModule { }
  
  export const routedComponents = [
    DashboardComponent,
    DashboardHomeComponent,
  ];
  