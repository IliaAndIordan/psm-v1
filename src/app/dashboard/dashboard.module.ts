import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule, routedComponents } from './dashboard-routing.module';
import { PsmShareModule } from '../@share/share.module';
import { RouterModule } from '@angular/router';



@NgModule({
  
  imports: [
    CommonModule,
    RouterModule,
    PsmShareModule,
    DashboardRoutingModule,
  ],
  declarations: [
    routedComponents,
  ],
})
export class DashboardModule { }
