import { Component, OnInit, OnDestroy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// --- Services
// --- Animations
import { Animate } from '../@core/constants/animation.const';
// --- Models
import { ExpandTab, ExpandTrigger, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, ShowlinksTrigger, ShowOverlayTrigger, SideNavChange, SpinExpandIconTrigger } from '../@core/constants/animations-triggers';
import { MatDialog } from '@angular/material/dialog';
import { AppStore, COMMON_IMG_LOGO_RED } from '../@core/constants/app-storage.const';
import { AppService } from '../@core/services/app.service';
import { CurrentUserService } from '../@core/clients/current-user.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatDrawerMode, MatSidenav } from '@angular/material/sidenav';
import { UserModel } from '../@core/clients/api/auth/dto';
import { CompanyModel } from '../@core/clients/api/company/dto';
import { AppRoutes } from '../@core/constants/app-routes.const';

//ShowlinksTrigger, ShowOverlayTrigger, SideNavChange
@Component({
  templateUrl: './home.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, ExpandTrigger]
})

export class DashboardHomeComponent implements OnInit, OnDestroy {

  @ViewChild('snav') snav: MatSidenav;

  sxLogo = COMMON_IMG_LOGO_RED;
  sidebarClass = 'sidebar-closed';
  // --- Side Tab
  expandTabVar: string = Animate.out;
  showTabContentsVar: string = Animate.hide;
  snavmode: MatDrawerMode = 'side';
  sideNavState: string = Animate.open;

  mobileQuery: MediaQueryList;
  fillerNav = Array.from({ length: 5 }, (_, i) => `Nav Item ${i + 1}`);

  showText: string = 'hide';
  snavWidth: string = 'small';
  showOverlay: string = 'hideOverLay';

  roots = AppRoutes;
  userName:string;
  user:UserModel;
  company:CompanyModel;
  avatarUrl:string;

  // public dialogService: MatDialog
  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private appService: AppService,
    private cus: CurrentUserService,
    public dialogService: MatDialog) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  private _mobileQueryListener: () => void;

  ngOnInit(): void {
    this.initFields();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  initFields() {
    //this.snavmode = this.cus.localSettings.snavLeftWidth > 0 ? 'side' : 'over';
    this.showText = Animate.hide;
    this.userName = this.cus.userName;
    this.user = this.cus.user;
    this.avatarUrl = this.cus.avatarUrl;
    this.company = this.cus.company;
  }

  snavToggleClick() {
    if(this.snav){
      //this.snav.toggle();
    }
  }
  exapandTabClick() {
    this.expandTabVar = this.expandTabVar === Animate.in ? Animate.out : Animate.in;
    this.showTabContentsVar = this.showTabContentsVar === Animate.show ? Animate.hide : Animate.show;
  }

  gotoCompanyProfile(user:UserModel){
    if(user){
      localStorage.setItem(AppStore.psm_company_profile, JSON.stringify(user));
      this.router.navigate([AppRoutes.Root, AppRoutes.company, AppRoutes.profile]);
    }
  }

  public loginDialogShow(): void {
    /*
    const dialogRef = this.dialogService.open(LoginModal, {
      width: '500px',
      height: '340px',
      data: { email: null, password: null }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
      // this.password = result.password;
    });
    */
  }

  // sidebar-closed
  sidebarToggle() {
    this.sideNavState = this.expandTabVar === Animate.open ? Animate.close : Animate.open;
    this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
  }

  // Expand Menu
  expandMenu(): void {
    this.snavWidth = Animate.large;
    this.sideNavState =  Animate.open;
    this.showOverlay = 'showOverLay';
    this.showText = Animate.show;
   
  }

  // Close Menu
  closeMenu(): void {
    this.snavWidth = Animate.small;
    this.sideNavState =  Animate.close;
    this.showOverlay = 'hideOverLay';
    this.showText = Animate.hide;
  }

}
