export const environment = {
  production: true,
  services: {
    url: {

      auth: 'https://psm.ws.iordanov.info/',
      company: 'https://sx.ws.iordanov.info/company/',
      usecase: 'https://psm.ws.iordanov.info/usecase/',
      pricefile: 'https://sx.ws.iordanov.info/pricefile/',
      project: 'https://sx.ws.iordanov.info/project/',
      commodity: 'https://sx.ws.iordanov.info/mfr/',
    },
    domains: {
      iordanovInfo: 'psm.ws.iordanov.info',
    },
  }
};
