// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  services: {
    url: {
      auth: 'https://psm.ws.iordanov.info/',
      company: 'https://psm.ws.iordanov.info/company/',
      usecase: 'https://psm.ws.iordanov.info/usecase/',
      pricefile: 'https://sx.ws.iordanov.info/pricefile/',
      project: 'https://sx.ws.iordanov.info/project/',
      commodity: 'https://sx.ws.iordanov.info/mfr/',
    },
    domains: {
      iordanovInfo: 'psm.ws.iordanov.info',
    },
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
